<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

switch (ENVIRONMENT) {
	case 'development':
		$config['sso_address'] = 'http://127.0.0.1/berkatcorp-master/sso/server';
		$config['master_address'] = 'http://127.0.0.1/berkatcorp-master';
		$config['pegawai_address'] = 'http://127.0.0.1/berkatcorp-pegawai';
		break;
	case 'production':
		$config['sso_address'] = 'http://master.berkatcorp.com/sso/server';
		$config['master_address'] = 'http://master.berkatcorp.com';
		$config['pegawai_address'] = 'http://pegawai.berkatcorp.com';
		break;
}