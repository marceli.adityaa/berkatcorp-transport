<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	protected $auth_type = 'internal';
	
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	protected function render($view, $data = array())
	{
		$this->blade->render('modules/' . $view, $data);
	}

	protected function go($link = '', $is_link = FALSE, $method = 'auto')
	{
		$url = $is_link ? $link : site_url($link);
		redirect($url, $method);
	}

	protected function message($message, $message_type = 'info')
	{
		$this->session->set_flashdata('message', array($message, $message_type));
	}

	public function validateToken($token = '')
    {
        if ($this->auth_type === 'external') {
			# Cek token from sso server
			$url = urlencode(site_url());
			$par[CURLOPT_URL] = $this->config->item('sso_address') . "/token_auth?token=" . $token . "&source=" . $url;
			$res = xcurl($par);
			# Decode json response
			$res['response'] = json_decode($res['response']);
			# return
			if ($res['response']->status !== 'success') {
				return false;
			}
		} else {
			# Cek token from local session
			if ($token !== $this->session->auth['token']) {
				return false;
			}
        }
        
        return true;
    }
}