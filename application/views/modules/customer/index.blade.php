@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item">Customer</li>
    </ol>
</nav>
@end

@section('content')
<div class="card">
    <div class="card-header">Data Customer</div>
    <div class="card-body">
        <div id="toolbar">
            <button class="btn btn-primary" data-toggle="modal" data-target="#mForm"><i class="fa fa-user-plus"></i> Tambah customer</button>
        </div>
        <table class="table table-js table-striped" data-toolbar="#toolbar" data-search="true" data-search-on-enter-key="true" data-pagination="true" data-side-pagination="server" data-show-refresh="true" data-url="{{site_url('api/internal/customer/get_many/'.$this->session->auth['token'].'?callback=ctable')}}">
            <thead>
                <tr>
                    <th class="text-center fit" data-formatter="formatNomor">Nomor</th>
                    <th class="text-center fit" data-field="aksi">Aksi</th>
                    <th class="text-center" data-field="nama">Nama/Instansi</th>
                    <th class="text-left" data-field="alamat">Alamat</th>
                    <th class="text-center" data-field="telpon">Nomor Telpon</th>
                    <th class="text-center" data-field="insert_time">Detail Entri</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="mForm" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Formulir</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="">
                    <input type="hidden" name="id" value="">
                    <div class="form-group">
                        <label for="">Nama/Instansi</label>
                        <input type="text" name="nama" class="form-control" placeholder="Tulis nama">
                    </div>
                    <div class="form-group">
                        <label for="">Alamat</label>
                        <textarea name="alamat" cols="30" rows="3" class="form-control" placeholder="Tulis alamat"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="">Telpon</label>
                        <input type="text" name="telpon" class="form-control" placeholder="Tulis telpon">
                    </div>
                    
                    <div class="form-group">
                        <button class="btn btn-primary" type="button" onclick="save()"><i class="fa fa-save"></i> Simpan</button>
                        <button class="btn btn-warning" type="button" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script>
    // VAR
    var url = "{{site_url('api/internal/customer')}}";
    var token = "{{$this->session->auth['token']}}";
    // FUNCTIONS
    function save() {
        // Validate
        var valid = true;
        $('[required]').each(function() {
            if (!$(this).val() || $(this).val() === null) {
                $(this).addClass('is-invalid').parent().find('.form-text').text('Data ini harus diisi.').focus();
                valid = false;
            } else {
                $(this).removeClass('is-invalid').parent().find('.form-text').text('');
            }
        });

        if (!valid) {
            return;
        }
        //Save
        var data = $('#mForm form').serializeArray();
        $.ajax({
            url: url + '/save/' + token,
            method: 'POST',
            data: data,
            dataType: 'json',
            success: function(result, status, xhr) {
                if (result.status === 'success') {
                    Toast.fire('Sukses!', result.message, 'success');
                    $('.modal').modal('hide');
                    $('.table-js').bootstrapTable('refresh');
                } else {
                    Toast.fire('Error!', result.error, 'error');
                }
            },
            error: function(xhr, status, error) {
                Toast.fire('Error!', 'Terjadi kesalahan pada sistem!', 'error');
            }
        });
    }

    function edit(id) {
        $.getJSON(url + '/get/' + token + '?id=' + id, function(result) {
            if (result) {
                $('[name=id]').val(result.id);
                $('[name=nama]').val(result.nama);
                $('[name=telpon]').val(result.telpon);
                $('[name=alamat]').val(result.alamat);
                $('#mForm').modal('show');
            }
        });
    }

    function disable(id, $isDisabled) {
        $.getJSON(url + '/disable/' + token + '?id=' + id + '&par=' + !$isDisabled, function(result) {
            if (result.status == 'success') {
                Toast.fire('Sukses!', result.message, 'success');
                $('.table-js').bootstrapTable('refresh');
            }
        });
    }

    function formatStatus(v) {
        if(v == '1') {
            return '<i class="fa fa-check text-success"></i>';
        } else {
            return '<i class="fa fa-times text-danger"></i>';
        }
    }
</script>
@end