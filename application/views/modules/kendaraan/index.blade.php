@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active">Kendaraan</li>
    </ol>
</nav>
@end

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('kendaraan/index?')?>" id="form-filter">
                    <?php $get = $this->input->get()?>
                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>

                        <label class="col-sm-3 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Status</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control select2">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$status as $key => $row){
                                        if(!empty($get['status']) && $get['status'] == $key){
                                            echo '<option value="'.$key.'" selected>'.ucwords($row).'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.ucwords($row).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label"></label>
                        <div class="col-sm-9 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                            <!-- <button type="button" class="btn btn-success" onclick="export_data(this)"><i class="fa fa-file-excel-o mg-r-10"></i>Export (dalam perbaikan)</button> -->
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card mt-4">
    <div class="card-header card-header-default">DATA KENDARAAN</div>
    <div class="card-body">
        <div id="toolbar">
            <button class="btn btn-primary" data-toggle="modal" data-target="#mForm">+ Tambah Data</button>
        </div>
        <div class="table-responsive">
            <table class="table table-white table-stripped" id="table-data">
                <thead>
                    <tr>
                        <th class="text-center fit" data-formatter="formatNomor">No.</th>
                        <th class="text-center fit" data-field="aksi">Aksi</th>
                        <th data-field="merk">Status</th>
                        <th data-field="merk">Merk Kendaraan</th>
                        <th data-field="tipe">Tipe Kendaraan</th>
                        <th data-field="warna">Warna</th>
                        <th data-field="tahun">Tahun</th>
                        <th data-field="no_pol">No Polisi</th>
                        <th data-field="no_mesin">No Mesin</th>
                        <th data-field="stnk_terbayar">STNK Terbayar</th>
                        <th data-field="stnk_terbayar">Tgl Bayar STNK</th>
                        <th data-field="pajak_terbayar">Pajak Terbayar</th>
                        <th data-field="pajak_terbayar">Tgl Bayar Pajak</th>
                        <th data-field="stnk_terbayar">BPKB Terbayar</th>
                        <th data-field="stnk_terbayar">Tgl Bayar BPKB</th>
                        <th data-field="pajak_terbayar">KIR Terbayar</th>
                        <th data-field="pajak_terbayar">Tgl Bayar KIR</th>
                    </tr>
                </thead>
                <tbody>
                <?php $no = 1;?>
                    @if (!empty($kendaraan))
                        @foreach ($kendaraan as $row)
                            <tr>
                                <td>{{$no++}}</td>
                                <td class="text-nowrap">
                                    <button class='btn btn-warning btn-sm' title='Sunting' onclick='edit("{{$row["id"]}}")'><i class='icon-pencil'></i></button>
                                    @if($row['is_disabled'] == 1) 
                                    <button class='btn btn-success btn-sm' title='Aktifkan' onclick='aktif({{$row["id"]}})'><i class='fa fa-check')></i></button>
                                    @else 
                                    <button class='btn btn-secondary btn-sm' title='Nonaktifkan' onclick='nonaktif({{$row["id"]}})'><i class='fa fa-times')></i></button>
                                    @endif
                                </td>
                                <td>
                                    @if($row['is_disabled'] == 0)
                                    <span class="badge badge-success">aktif</span>
                                    @else
                                    <span class="badge badge-secondary">nonaktif</span>
                                    @endif
                                </td>
                                <td>
                                    <label class="badge badge-light">{{ucwords($row['merk'])}}</label>
                                </td>
                                <td>
                                    <label class="badge badge-light">{{ucwords($row['tipe'])}}</label>
                                </td>
                                <td>
                                    <label class="badge badge-light">{{ucwords($row['warna'])}}</label>
                                </td>
                                <td>
                                    <label class="badge badge-light">{{$row['tahun']}}</label>
                                </td>
                                <td>
                                    <label class="badge badge-light">{{($row['no_pol'])}}</label>
                                </td>
                                <td>
                                    <label class="badge badge-light">{{ucwords($row['no_mesin'])}}</label>
                                </td>
                                <td>
                                    <label class="badge badge-light">{{($row['stnk_terbayar'])}}</label>
                                </td>
                                <?php 
                                    $now = date_create();
                                    $stnk_reminder = (strtotime($row['stnk_jatuh_tempo']) - strtotime(date_format($now, "Y-m-d"))) / (60 * 60 * 24);
                                    $pajak_reminder = (strtotime($row['pajak_jatuh_tempo']) - strtotime(date_format($now, "Y-m-d"))) / (60 * 60 * 24);
                                    $bpkb_reminder = (strtotime($row['bpkb_jatuh_tempo']) - strtotime(date_format($now, "Y-m-d"))) / (60 * 60 * 24);
                                    $kir_reminder = (strtotime($row['kir_jatuh_tempo']) - strtotime(date_format($now, "Y-m-d"))) / (60 * 60 * 24);
                                    // dump($pajak_reminder);
                                ?>
                                <td>
                                    <label class="badge badge-light">{{$row['stnk_jatuh_tempo']}}</label><br>
                                    @if($row['stnk_jatuh_tempo'] != '0000-00-00')
                                        @if($stnk_reminder > 0 && $stnk_reminder <= 7)
                                        <label class="badge badge-warning">kurang {{$stnk_reminder}} hari</label>
                                        @elseif($stnk_reminder > 7)
                                        <!-- <label class="badge badge-secondary">kurang {{$stnk_reminder}} hari</label> -->
                                        @else 
                                        <label class="badge badge-danger">lewat {{abs($stnk_reminder)}} hari</label>
                                        @endif
                                    @endif
                                </td>
                                <td>
                                    <label class="badge badge-light">{{($row['pajak_terbayar'])}}</label>
                                </td>
                                <td>
                                    <label class="badge badge-light">{{$row['pajak_jatuh_tempo']}}</label><br>
                                    @if($row['pajak_jatuh_tempo'] != '0000-00-00')
                                        @if($pajak_reminder > 0 && $pajak_reminder <= 7)
                                        <label class="badge badge-warning">kurang {{$pajak_reminder}} hari</label>
                                        @elseif($pajak_reminder > 7)
                                        <!-- <label class="badge badge-secondary">kurang {{$pajak_reminder}} hari</label> -->
                                        @else 
                                        <label class="badge badge-danger">lewat {{abs($pajak_reminder)}} hari</label>
                                        @endif
                                    @endif
                                </td>
                                <td>
                                    <label class="badge badge-light">{{($row['bpkb_terbayar'])}}</label>
                                </td>
                                <td>
                                    <label class="badge badge-light">{{$row['bpkb_jatuh_tempo']}}</label><br>
                                    @if($row['bpkb_jatuh_tempo'] != '0000-00-00')
                                        @if($bpkb_reminder > 0 && $bpkb_reminder <= 7)
                                        <label class="badge badge-warning">kurang {{$bpkb_reminder}} hari</label>
                                        @elseif($bpkb_reminder > 7)
                                        <!-- <label class="badge badge-secondary">kurang {{$pajak_reminder}} hari</label> -->
                                        @else 
                                        <label class="badge badge-danger">lewat {{abs($bpkb_reminder)}} hari</label>
                                        @endif
                                    @endif
                                </td>
                                <td>
                                    <label class="badge badge-light">{{($row['kir_terbayar'])}}</label>
                                </td>
                                <td>
                                    <label class="badge badge-light">{{$row['kir_jatuh_tempo']}}</label><br>
                                    @if($row['kir_jatuh_tempo'] != '0000-00-00')
                                        @if($kir_reminder > 0 && $kir_reminder <= 7)
                                        <label class="badge badge-warning">kurang {{$kir_reminder}} hari</label>
                                        @elseif($kir_reminder > 7)
                                        <!-- <label class="badge badge-secondary">kurang {{$kir_reminder}} hari</label> -->
                                        @else 
                                        <label class="badge badge-danger">lewat {{abs($kir_reminder)}} hari</label>
                                        @endif
                                    @endif
                                </td>
                                
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="mForm" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Formulir Data Kendaraan</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="">
                    <div class="form-layout form-layout-4">
                        <input type="hidden" name="id" value="">

                        <div class="row">
                            <label class="col-sm-4 form-control-label">Merk Kendaraan <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10">
                                <input type="text" class="form-control" name="merk" placeholder="Masukkan merk kendaraan" required>
                            </div>
                        </div>

                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Tipe Kendaraan <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10">
                                <input type="text" class="form-control" name="tipe" placeholder="Masukkan tipe kendaraan" required>
                            </div>
                        </div>

                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Warna <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10">
                                <input type="text" class="form-control" name="warna" placeholder="Masukkan warna kendaraan" required>
                            </div>
                        </div>

                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Tahun <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10">
                                <input type="text" class="form-control" name="tahun" placeholder="Masukkan tahun kendaraan" required>
                            </div>
                        </div>

                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Nomor Polisi <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10">
                                <input type="text" class="form-control" name="no_pol" placeholder="Masukkan nomor polisi" autocomplete="off" required>
                            </div>
                        </div>

                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Nomor Mesin <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10">
                                <input type="text" class="form-control" name="no_mesin" placeholder="Masukkan nomor mesin" autocomplete="off">
                            </div>
                        </div>

                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">STNK Terbayar <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10">
                                <input type="date" class="form-control" name="stnk_terbayar" placeholder="Masukkan tanggal bayar STNK terakhir" autocomplete="off">
                            </div>
                        </div>

                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">STNK Jatuh Tempo <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10">
                                <input type="date" class="form-control" name="stnk_jatuh_tempo" placeholder="Masukkan tanggal jatuh tempo STNK" autocomplete="off">
                            </div>
                        </div>

                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Pajak Terbayar <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10">
                                <input type="date" class="form-control" name="pajak_terbayar" placeholder="Masukkan tanggal bayar pajak terakhir" autocomplete="off">
                            </div>
                        </div>

                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">Pajak Jatuh Tempo <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10">
                                <input type="date" class="form-control" name="pajak_jatuh_tempo" placeholder="Masukkan tanggal jatuh tempo pajak" autocomplete="off">
                            </div>
                        </div>

                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">BPKB Terbayar <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10">
                                <input type="date" class="form-control" name="bpkb_terbayar" placeholder="Masukkan tanggal bayar pajak terakhir" autocomplete="off">
                            </div>
                        </div>

                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">BPKB Jatuh Tempo <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10">
                                <input type="date" class="form-control" name="bpkb_jatuh_tempo" placeholder="Masukkan tanggal jatuh tempo pajak" autocomplete="off">
                            </div>
                        </div>

                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">KIR Terbayar <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10">
                                <input type="date" class="form-control" name="kir_terbayar" placeholder="Masukkan tanggal bayar pajak terakhir" autocomplete="off">
                            </div>
                        </div>

                        <div class="row mg-t-10">
                            <label class="col-sm-4 form-control-label">KIR Jatuh Tempo <span class="tx-danger"></span></label>
                            <div class="col-sm-8 mg-t-10">
                                <input type="date" class="form-control" name="kir_jatuh_tempo" placeholder="Masukkan tanggal jatuh tempo pajak" autocomplete="off">
                            </div>
                        </div>

                        <div class="row mg-t-10">
                            <div class="col-12 text-center">
                                <button class="btn btn-primary" type="button" onclick="save()"><i class="fa fa-save"></i> Simpan</button>
                                <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script>
    // VAR
    var url = "{{site_url('api/internal/kendaraan')}}";
    var token = "{{$this->session->auth['token']}}";

    // FUNCTIONS
    function save() {
        // Validate
        var valid = true;
        $('[required]').each(function() {
            if (!$(this).val() || $(this).val() === null) {
                $(this).addClass('is-invalid').parent().find('.form-text').text('Data ini harus diisi.').focus();
                valid = false;
            } else {
                $(this).removeClass('is-invalid').parent().find('.form-text').text('');
            }
        });

        if (!valid) {
            return;
        }
        //Save
        var data = $('#mForm form').serializeArray();
        $.ajax({
            url: url + '/save/' + token,
            method: 'POST',
            data: data,
            dataType: 'json',
            success: function(result, status, xhr) {
                reload_page();
            },
            error: function(xhr, status, error) {
                Toast.fire('Error!', 'Terjadi kesalahan pada sistem!', 'error');
            }
        });
    }

    function edit(id) {
        $.getJSON(url+'/get/'+token+'?id='+id, function(result) {
            if (result) {
                $('[name=id]').val(result.id);
                $('[name=merk]').val(result.merk);
                $('[name=tipe]').val(result.tipe);
                $('[name=warna]').val(result.warna);
                $('[name=tahun]').val(result.tahun);
                $('[name=no_pol]').val(result.no_pol);
                $('[name=no_mesin]').val(result.no_mesin);
                $('[name=stnk_terbayar]').val(result.stnk_terbayar);
                $('[name=stnk_jatuh_tempo]').val(result.stnk_jatuh_tempo);
                $('[name=pajak_terbayar]').val(result.pajak_terbayar);
                $('[name=pajak_jatuh_tempo]').val(result.pajak_jatuh_tempo);
                $('[name=bpkb_terbayar]').val(result.bpkb_terbayar);
                $('[name=bpkb_jatuh_tempo]').val(result.bpkb_jatuh_tempo);
                $('[name=kir_terbayar]').val(result.kir_terbayar);
                $('[name=kir_jatuh_tempo]').val(result.kir_jatuh_tempo);
                $('#mForm').modal('show');
            }
        });
    }

    function aktif(id, $isDisabled) {
		$.ajax({
            url: url + '/aktif/' + token,
            method: 'POST',
            data: {
                id: id
            },
            dataType: 'json',
            success: function(result, status, xhr) {
                reload_page();
            },
            error: function(xhr, status, error) {
                Toast.fire('Error!', 'Terjadi kesalahan pada sistem!', 'error');
            }
        });
	}
    function nonaktif(id) {
        $.ajax({
            url: url + '/nonaktif/' + token,
            method: 'POST',
            data: {
                id: id
            },
            dataType: 'json',
            success: function(result, status, xhr) {
                reload_page();
            },
            error: function(xhr, status, error) {
                Toast.fire('Error!', 'Terjadi kesalahan pada sistem!', 'error');
            }
        });
    }
</script>
@end