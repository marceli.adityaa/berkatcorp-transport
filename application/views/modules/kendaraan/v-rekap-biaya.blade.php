@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('kendaraan/rekap_biaya?')?>" id="form-filter">
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Kendaraan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="kendaraan" class="form-control select2">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$kendaraan as $row){
                                            if(!empty($this->input->get('kendaraan')) && $this->input->get('kendaraan') == $row['id']){
                                                echo '<option value="'.$row['id'].'" selected>'.ucwords($row['no_pol'].' | '.$row['merk'].' | '.$row['tipe']).'</option>';
                                            }else{
                                                echo '<option value="'.$row['id'].'">'.ucwords($row['no_pol'].' | '.$row['merk'].' | '.$row['tipe']).'</option>';
                                            }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Kategori Biaya</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="kategori" class="form-control select2">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$kategori as $row){
                                        // if($row['is_primary'] == 0){
                                            if(!empty($this->input->get('kategori')) && $this->input->get('kategori') == $row['id']){
                                                echo '<option value="'.$row['id'].'" selected>'.ucwords($row['nama']).'</option>';
                                            }else{
                                                echo '<option value="'.$row['id'].'">'.ucwords($row['nama']).'</option>';
                                            }
                                        // }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                            @if(!empty($this->input->get()))
                            <a href="{{base_url('kendaraan/rekap_biaya')}}"><button type="button" class="btn btn-warning"><i class="fa fa-refresh mg-r-10"></i>Reset</button></a>
                            <button type="button" class="btn btn-success" onclick="export_data(this)"><i class="fa fa-file-excel-o mg-r-10"></i>Export to Excel</button>
                            @endif
                        </div>
                    </div>
                    
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Rekap Biaya Kendaraan </h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
        </div>
        @if(!empty($this->input->get()))
        <table class="table table-striped mg-t-10 table-white" id="tabel_beban">
            <thead>
                <tr>
                    <th data-formatter="reformat_number" class="text-center">No.</th>
                    <th data-sortable="true">Tanggal</th>
                    <th data-sortable="true">Kategori</th>
                    <th data-sortable="true">Kendaraan</th>
                    <th data-sortable="true">Catatan</th>
                    <th data-sortable="true">Nominal</th>
                    <th data-sortable="true">Sumber Kas</th>
                    <th data-sortable="true">Input by</th>
                    <th data-sortable="true">Verifikasi</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                
                    $no = 1;
                    $subtotal = 0;
                    if(!empty($biaya)){
                        foreach($biaya as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td>".$row['tanggal']."</td>";
                            echo "<td><label class='badge badge-info'>".ucwords($row['kategori'])."</label></td>";
                            echo "<td><label class='badge badge-light'>".ucwords($row['kendaraan'])."</label></td>";
                            echo "<td>".$row['catatan']."</td>";
                            echo "<td>".monefy($row['nominal'], false)."</td>";
                            if($row['id_kas'] != 0){
                                if($row['kas_is_rekening'] == 1){
                                    echo "<td><label class='badge badge-info'>".$row['kas_bank']." | ".$row['kas_no_rek']."</label></td>";
                                }else{
                                    echo "<td><label class='badge badge-primary'>".$row['kas_label']."</label></td>";
                                }
                            }else{
                                echo "<td>-</td>";
                            }
                            echo "<td><label class='badge badge-light'>".$row['submit_by']."</label><br><label class='badge badge-light'>".$row['timestamp_input']."</label></td>";
                            if($row['is_verifikasi'] != 1){
                                echo "<td>-</td>";
                            }else{
                                echo "<td><label class='badge badge-light'>".$row['acc_by']."</label><br><label class='badge badge-light'>".$row['timestamp_verifikasi']."</label></td>";
                            }
                            echo "</tr>";
                            $subtotal += $row['nominal'];
                        }
                    }
				?>
            </tbody>
        </table>
        <table class="table table-white" style="font-weight:800;font-size:18px">
            <tr>
                <td class="text-right">Subtotal : </td>
                <td class="text-left"><?= monefy($subtotal, false)?></td>
            </tr>

        </table>
        @else
        <p class="text-center">Klik filter untuk menampilkan data transaksi</p>
        @endif
    </div>
</div>
@end

@section('modal')

@end

@section('js')
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_beban').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2();

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    })

    $('.autonumeric').autoNumeric('init', {
        'mDec': 0
    });
    cek_metode_pembayaran();
});

function export_data(el){
    let start = $("#form-filter [name=start]").val();
    let end = $("#form-filter [name=end]").val();
    let kategori = $("#form-filter [name=kategori]").val();
    let kendaraan = $("#form-filter [name=kendaraan]").val();
    // if (start != '' && end != '') {
    var url = "<?= base_url('kendaraan/export_biaya?')?>"+'start='+start+'&end='+end+'&kategori='+kategori+'&kendaraan='+kendaraan;
    window.open(url, '_blank');
    // } else {
    //     Swal.fire('Perhatian', 'Rentang tanggal harus diisi', 'warning');
    // }
}
</script>
@end