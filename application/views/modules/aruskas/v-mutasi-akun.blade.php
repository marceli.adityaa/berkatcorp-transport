@layout('commons/index')

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('aruskas/mutasi?')?>" id="form-filter">
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Sumber Kas</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="kas" class="form-control select2">
                                <option value="all">All</option>
                                <optgroup label="Kas">
                                @foreach ($kas as $row)
                                    @if (!empty($this->input->get('kas')) && $this->input->get('kas') == $row->id)
                                        <option value="{{$row->id}}" selected>{{ucwords($row->label)}}</option>
                                    @else
                                        <option value="{{$row->id}}">{{ucwords($row->label)}}</option>
                                    @endif
                                @endforeach
                                </optgroup>
                                <optgroup label="Rekening">
                                @foreach ($rekening as $row)
                                    @if (!empty($this->input->get('kas')) && $this->input->get('kas') == $row->id)
                                        <option value="{{$row->id}}" selected>{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                                    @else
                                        <option value="{{$row->id}}">{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                                    @endif
                                @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>                       
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>
                   
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                            <button type="button" class="btn btn-success" onclick="export_data(this)"><i class="fa fa-file-excel-o mg-r-10"></i>Export to Excel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card">
    <div class="card-header card-header-default bg-brown">
        <h6 class="mg-b-0 color-brown">Data Mutasi Akun</h6>
    </div>
    <div class="card-body">
        <div id="toolbar" class="mg-b-10">
            <button class="btn btn-primary" id="tambah">+ Tambah Transaksi</button>
        </div>
        <table class="table table-striped mg-t-10 table-white" id="tabel_pembelian">
        <thead>
                <tr>
                    <th class="text-center">No.</th>
                    <th data-searchable="false">Aksi</th>
                    <th data-sortable="true">Tanggal</th>
                    <th data-sortable="true">Kas Sumber</th>
                    <th data-sortable="true">Kas Tujuan</th>
                    <th data-sortable="true">Nominal</th>
                    <th data-sortable="true">Keterangan</th>
                    <th data-sortable="true">Input by</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    if(!empty($transaksi)){
                        foreach($transaksi as $row){
                            echo "<tr>";
                            echo "<td class='text-center'>".$no++."</td>";
                            echo "<td class='text-nowrap'>";
                                echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus' onclick='hapus(this)' data-id='".$row->id."'><i class='fa fa-trash'></i></button> ";
                            
                            echo "</td>";
                            
                            echo "<td>".date('Y-m-d', strtotime($row->tanggal))."</td>";
                            echo "<td><label class='badge badge-light'>".$row->kas_from_label."</label></td>";
                            echo "<td><label class='badge badge-light'>".$row->kas_to_label."</label></td>";
                            echo "<td>".monefy($row->nominal, false)."</td>";
                            echo "<td>".$row->keterangan."</td>";

                            echo "<td><label class='badge badge-light'>".$row->username_input."</label><br><label class='badge badge-light'>".$row->timestamp_input."</label></td>";
                            echo "</tr>";
                        }
                    }
				?>
            </tbody>
        </table>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Mutasi Akun</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="tab-pane fade show active" id="tab-timbangan" role="tabpanel" aria-labelledby="pills-home-tab">
                        <form action="{{base_url('aruskas/submit_mutasi_akun')}}" id="form-transaksi" method="post">
                            <div class="form-layout form-layout-4">
                                <!-- <input type="hidden" name="id" value=""> -->
                                <input type="hidden" name="url" value="{{$_SERVER['QUERY_STRING']}}">
                                <div class="row">
                                    <label class="col-sm-4 form-control-label">Tanggal <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <input type="date" class="form-control" name="tanggal" required=""> 
                                    </div>
                                </div>
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Sumber Kas <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <select name="kas_from" class="form-control select2-modal" required="">
                                            <option value="">- Pilih Kas -</option>
                                            <optgroup label="Kas">
                                            @foreach ($kas as $row)
                                                @if (!empty($this->input->get('kas')) && $this->input->get('kas') == $row->id)
                                                    <option value="{{$row->id}}" selected>{{ucwords($row->label)}}</option>
                                                @else
                                                    <option value="{{$row->id}}">{{ucwords($row->label)}}</option>
                                                @endif
                                            @endforeach
                                            </optgroup>
                                            <optgroup label="Rekening">
                                            @foreach ($rekening as $row)
                                                @if (!empty($this->input->get('kas')) && $this->input->get('kas') == $row->id)
                                                    <option value="{{$row->id}}" selected>{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                                                @else
                                                    <option value="{{$row->id}}">{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                                                @endif
                                            @endforeach
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Target Kas <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <select name="kas_to" class="form-control select2-modal" required="">
                                            <option value="">- Pilih Kas -</option>
                                            <optgroup label="Kas">
                                            @foreach ($kas as $row)
                                                @if (!empty($this->input->get('kas')) && $this->input->get('kas') == $row->id)
                                                    <option value="{{$row->id}}" selected>{{ucwords($row->label)}}</option>
                                                @else
                                                    <option value="{{$row->id}}">{{ucwords($row->label)}}</option>
                                                @endif
                                            @endforeach
                                            </optgroup>
                                            <optgroup label="Rekening">
                                            @foreach ($rekening as $row)
                                                @if (!empty($this->input->get('kas')) && $this->input->get('kas') == $row->id)
                                                    <option value="{{$row->id}}" selected>{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                                                @else
                                                    <option value="{{$row->id}}">{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                                                @endif
                                            @endforeach
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-4 form-control-label">Nominal<span class="tx-danger">*</span></label>
                                    <div class="col-8 mg-t-10 mg-sm-t-0">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">Rp</span>
                                            </div>
                                            <input type="text" class="form-control autonumeric" name="nominal" required="" autocomplete="off"> 
                                        </div>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-4 form-control-label">Keterangan<span class="tx-danger">*</span></label>
                                    <div class="col-8 mg-t-10 mg-sm-t-0">
                                        <input type="text" class="form-control" autocomplete="off" name="keterangan" required>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label"></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tabel_pembelian').bootstrapTable({
        pagination: true,
        search: true,
        toolbar: '#toolbar'
    });
    $('.select2').select2({
    });

    $('.select2-modal').select2({
        dropdownParent: $('#modal_form')
    });

    $('.autonumeric').autoNumeric('init', {
        'mDec': 0
    });
    
});

$('#m-form').on('shown.bs.modal', function(e) {
    $("[name=nama]").focus();
});

$('#tambah').click(function() {
    $('#form-transaksi').trigger('reset');
    $("#modal_form").modal('show');
});

function hapus(e) {
    Swal.fire({
        title: 'Apakah anda yakin?',
        text: "Data transaksi di arus kas juga akan dihapus, lanjutkan?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "<?= base_url('aruskas/delete_transaksi_mutasi')?>",
                type: "POST",
                dataType: "json",
                data: {
                    'id': $(e).data().id
                },
                success: function(result) {
                    reload_page();
                },
                error: function(e) {
                    console.log(e);
                },
                complete: function(e) {}
            });
        }
    })
}

function export_data(el){
    let start = $("#form-filter [name=start]").val();
    let end = $("#form-filter [name=end]").val();
    let kas = $("#form-filter [name=kas]").val();
    var url = "<?= base_url('aruskas/export_mutasi?')?>"+'start='+start+'&end='+end+'&kas='+kas;
    window.open(url, '_blank');
}
</script>
@end