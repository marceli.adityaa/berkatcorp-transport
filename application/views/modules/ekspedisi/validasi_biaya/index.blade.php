@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{site_url('ekspedisi')}}">Ekspedisi</a></li>
        <li class="breadcrumb-item active">Validasi Biaya</li>
    </ol>
</nav>
@end

@section('content')

<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('validasi_biaya/index?')?>" id="form-filter">
                    <?php $get = $this->input->get()?>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Status</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control select2">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$status as $key => $row){
                                        if(!empty($get['status']) && $get['status'] == $key){
                                            echo '<option value="'.$key.'" selected>'.ucwords($row).'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.ucwords($row).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card mt-4">
    <div class="card-header card-header-default">DATA EKSPEDISI</div>
    <div class="card-body">
        <div id="toolbar">
        </div>
        <table class="table table-white table-bordered" id="table-data">
            <thead>
                <tr>
                    <th class="text-left">AKSI</th>
                    <th class="text-center" data-sortable="true">STATUS</th>
                    <th class="text-center" data-sortable="true">NO RESI</th>
                    <th class="text-center" data-sortable="true">KENDARAAN</th>
                    <th class="text-center" data-sortable="true">TGL BERANGKAT</th>
                    <th class="text-center" data-sortable="true">TGL KEMBALI</th>
                    <th class="text-left" data-sortable="true">SUPIR</th>
                    <th class="text-center" data-sortable="true">TUJUAN</th>
                    <th class="text-center">BERAT MUATAN</th>
                    <th class="text-center">SOLAR PERTAMA</th>
                    <th class="text-center">UANG SAKU</th>
                    <th class="text-right">ONGKOS BERANGKAT</th>
                    <th class="text-right">BIAYA EKSPEDISI</th>
                    <th class="text-right">PENDAPATAN BALEN</th>
                    <th class="text-right">LABA / RUGI</th>
                    <th class="text-right">SISA UANG SAKU</th>
                    <th class="text-right">UANG KEMBALI</th>
                </tr>
            </thead>
            <tbody>
                @if (!empty($ekspedisi))
                    @foreach ($ekspedisi as $row)
                        <tr>
                            <td class="nowrap">
                                <a href="{{site_url('validasi_biaya/detail/'.$row['id'])}}" class='btn btn-info btn-sm' title='Detail Biaya' target="_blank"><i class='icon-list'></i></a>
                                @if($row['is_done'] == 0) 
                                    <button class='btn btn-success btn-sm' title='Setujui ekspedisi' onclick="setuju({{$row['id']}})"><i class='fa fa-check'></i></button>
                                    <button class='btn btn-danger btn-sm' title='Batalkan ekspedisi' onclick="tolak({{$row['id']}})"><i class='fa fa-times'></i></button>
                                @elseif($row['is_done'] == 2)
                                    <button class='btn btn-success btn-sm' title='Setujui ekspedisi' onclick="setuju({{$row['id']}})"><i class='fa fa-check'></i></button>
                                @endif
                            </td>
                            <td>
                                @if($row['is_done'] == 0)
                                <span class="badge badge-secondary">menunggu</span>
                                @elseif ($row['is_done'] == 2)
                                <span class="badge badge-danger">ditolak</span>
                                @elseif ($row['is_done'] == 1)
                                <span class="badge badge-success">disetujui</span>
                                @endif
                            </td>
                            <td>
                                <label class="badge badge-light">{{$row['no_resi']}}</label>
                            </td>
                            <td>
                                <label class="badge badge-light">{{$row['no_pol']}}</label>
                            </td>
                            <td>
                                <label class="badge badge-light">{{$row['tanggal_jalan']}}</label>
                            </td>
                            <td>
                                @if($row['tanggal_kembali'] == '0000-00-00')
                                -
                                @else 
                                    <label class="badge badge-light">{{$row['tanggal_kembali']}}</label>
                                @endif
                            </td>
                            <td>
                                <label class="badge badge-light">{{ucwords($row['supir'])}}</label>
                            </td>
                            <td>
                                @if($row['is_sby'])
                                    <label class="badge badge-warning">{{ucwords($row['kota'])}}</label>
                                @else 
                                    <label class="badge badge-light">{{ucwords($row['kota'])}}</label>
                                @endif
                            </td>
                           
                            <td>
                                <label class="badge badge-light">{{($row['berat_muatan'])}}</label>
                            </td>
                            <td>
                                {{monefy($row['solar_pertama'], false)}}
                            </td>
                            <td>
                                {{monefy($row['uang_saku'], false)}}<br>
                                @if($row['id_kas'] != 0)
                                    <label class="badge badge-light">{{($row['kas_label'])}}</label>
                                @endif
                            </td>
                            <td>
                                {{monefy($row['biaya'], false)}}
                            </td>
                            <td>
                                {{monefy($row['total_biaya'], false)}}
                            </td>
                            <td>
                                {{monefy($row['total_balen'], false)}}
                            </td>
                            <?php 
                                $laba = ($row['biaya'] + $row['total_balen'] - $row['solar_pertama'] - $row['total_biaya']);
                                $kembalian = ($row['uang_saku'] + $row['total_balen_cash'] - $row['total_biaya']);
                                $sisa_uangsaku = ($row['uang_saku'] - $row['total_biaya']);
                            ?>
                            <td>
                                @if($laba < 0)
                                <span class="tx-danger">{{monefy($laba, false)}}</span>
                                @else 
                                <span class="tx-success">+{{monefy($laba, false)}}</span>
                                @endif
                            </td>
                            <td>
                                @if($sisa_uangsaku < 0)
                                <span class="tx-danger">{{monefy($sisa_uangsaku, false)}}</span>
                                @else 
                                {{monefy($sisa_uangsaku, false)}}
                                @endif
                            </td>
                            <td>
                                @if($kembalian < 0)
                                <span class="tx-danger">{{monefy($kembalian, false)}}</span>
                                @else 
                                {{monefy($kembalian, false)}}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <form method="post" action="<?= base_url('validasi_biaya/setuju_pengajuan')?>" id="form-ekspedisi">
                <div class="modal-header bg-midnightblack">
                    <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Persetujuan Ekspedisi | <label id="kode" class="tx-white"></label></h6>
                    <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="">
                    <input type="hidden" name="url" value="{{$_SERVER['QUERY_STRING']}}">
                    <div class="form-layout form-layout-4">
                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Berangkat</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="berangkat" class="form-control" value="" required="">
                            </div>
                        </div>

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Pulang</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="pulang" class="form-control" value="" required="">
                            </div>
                        </div>

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Tujuan</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="id_area" class="form-control select2" required="">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($area as $row)
                                        <option value="{{$row->id}}">{{$row->kota}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Kendaraan</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="kendaraan_id" class="form-control select2-modal" required="">
                                    <option value="">- Pilih Salah Satu</option>
                                    @foreach($kendaraan as $row)
                                        <option value="{{$row['id']}}">{{ucwords($row['no_pol'])}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Supir</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="pegawai_id" class="form-control select2-modal" required="">
                                    <option value="">- Pilih Salah Satu</option>
                                    @foreach($supir as $row)
                                        <option value="{{$row->id}}">{{ucwords($row->nama)}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>     

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Berat Muatan</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="berat_muatan" class="form-control autonumeric" value="" autocomplete="off" placeholder="">
                            </div>
                        </div>               

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Solar Pertama</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="solar_pertama" class="form-control autonumeric" value="" autocomplete="off" placeholder="Masukkan jumlah solar pertama">
                            </div>
                        </div>    

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Uang Saku</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="uang_saku" class="form-control autonumeric" value="" autocomplete="off" placeholder="Masukkan jumlah uang saku">
                            </div>
                        </div>
                        
                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Ongkos Berangkat</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="ongkos_berangkat" class="form-control autonumeric" value="" autocomplete="off" placeholder="Masukkan jumlah ongkos berangkat">
                            </div>
                        </div>

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Biaya Ekspedisi</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="biaya_ekspedisi" class="form-control autonumeric" value="" autocomplete="off" placeholder="Masukkan jumlah uang saku">
                            </div>
                        </div>

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Pendapatan Balen</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="balen" class="form-control autonumeric" value="" autocomplete="off" placeholder="Masukkan jumlah pendapatan balen">
                            </div>
                        </div>

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Sisa Uang Saku</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="sisa_uangsaku" class="form-control" value="" autocomplete="off">
                            </div>
                        </div>

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Uang Kembali</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="uang_kembali" class="form-control" value="" autocomplete="off" placeholder="Masukkan jumlah uang kembali">
                            </div>
                        </div>

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Laba / Rugi</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="laba_rugi" class="form-control" value="" autocomplete="off" placeholder="Masukkan jumlah laba rugi">
                            </div>
                        </div>

                        
                        
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save mr-2"></i>Setujui</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
@end

@section('js')
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script>
    // VAR
    var url = "{{site_url('api/internal/biaya')}}";
    var token = "{{$this->session->auth['token']}}";
    // FUNCTIONS

    $(document).ready(function() {
        $('.select2-modal').select2({
            dropdownParent: $('#modal_form')
        });
        cek_metode_pembayaran()
    });

    $('#table-data').bootstrapTable({
        pagination: true,
        search:true,
        showToggle: true,
        showColumns: true,
        pageSize:10,
        striped: true,
        showFilter: true,
        toolbar: '#toolbar'
    });

    function cek_metode_pembayaran(){
        let jenis_pembayaran = $('[name=jenis_pembayaran]:checked').val();
        if(jenis_pembayaran == 'cash'){
            $('.cash').fadeIn();
            $('.transfer').hide();
        }else if(jenis_pembayaran == 'transfer'){
            $('.cash').hide();
            $('.transfer').fadeIn();
        }else{
            $('.cash').hide();
            $('.transfer').hide();
        }
    }

    $('[name=jenis_pembayaran]').change(function(){
        cek_metode_pembayaran();
    });

    function setuju(id) {
        $.ajax({
            url: url + '/get_detail_validasi/' + token,
            type: "POST",
            dataType: "json",
            data: {
                'id' : id,
            },
            success: function(result) {
                $('#form-ekspedisi input[type=text], #form-ekspedisi select').attr('disabled', 'true');
                $('#kode').empty().html(result.no_resi);
                $('[name=berangkat]').val(result.tanggal_jalan);
                $('[name=pulang]').val(result.tanggal_kembali);
                $('[name=id]').val(result.id);
                $('#form-ekspedisi [name=kendaraan_id]').val(result.kendaraan_id).change();
                $('#form-ekspedisi [name=pegawai_id]').val(result.pegawai_id).change();
                $('#form-ekspedisi [name=id_area]').val(result.id_area).change();
                $('[name=solar_pertama]').val(result.solar_pertama);
                $('[name=uang_saku]').val(result.uang_saku);
                $('[name=berat_muatan]').val(result.berat_muatan);
                $('[name=ongkos_berangkat]').val(result.biaya);
                $('[name=biaya_ekspedisi]').val(result.total_biaya);
                $('[name=balen]').val(result.total_balen);
                let laba = parseInt(result.biaya) + parseInt(result.total_balen) - parseInt(result.solar_pertama) - parseInt(result.total_biaya);
                let kembalian = parseInt(result.uang_saku) + parseInt(result.total_balen_cash) - parseInt(result.total_biaya);
                let sisa_uangsaku = parseInt(result.uang_saku) - parseInt(result.total_biaya);
                $('[name=laba_rugi]').val(laba.toLocaleString());
                $('[name=uang_kembali]').val(kembalian.toLocaleString());
                $('[name=sisa_uangsaku]').val(sisa_uangsaku.toLocaleString());
                $("#modal_form").modal('show');
                $('.autonumeric').autoNumeric('destroy');
                $('.autonumeric').autoNumeric('init', {
                    'mDec': 0
                });
                cek_metode_pembayaran();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {
            }
        });
        
    }

    function tolak(id) {
        Swal.fire({
            title: 'Konfirmasi?',
            text: "Tolak pengajuan data ekspedisi?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url + '/tolak_pengajuan/' + token,
                    method: 'POST',
                    data: {
                        id: id
                    },
                    dataType: 'json',
                    success: function(result, status, xhr) {
                            reload_page();
                    },
                    error: function(xhr, status, error) {
                        console.log(error);
                    }
                });
            }
        });
    }

    
</script>
@end