@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{site_url('ekspedisi')}}">Ekspedisi</a></li>
        <li class="breadcrumb-item"><a href="{{site_url('biaya')}}">Biaya</a></li>
        <li class="breadcrumb-item"><a href="{{site_url('validasi_biaya')}}">Validasi Biaya</a></li>
        <li class="breadcrumb-item active">Formulir Isian</li>
    </ol>
</nav>
@end

@section('content')
<div class="card">
    <div class="card-header">Formulir Input Biaya</div>
    <div class="card-body">
        <form action="{{site_url('biaya/save/'.$this->session->auth['token'].'?isvalidasi=true')}}" method="POST">
            <input type="hidden" name="id" value="{{$biaya?$biaya->id:''}}">
            <input type="hidden" name="ekspedisi_id" value="{{$ekspedisi->id}}">
            <div class="form-row">
                <div class="form-group col-12 col-md-4">
                    <label>Nomor Resi</label>
                    <input type="text" class="form-control" value="{{$ekspedisi->no_resi}}" readonly>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label>Nomor Polisi</label>
                    <input type="text" class="form-control" value="{{$ekspedisi->no_pol}}" readonly>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label>Tujuan</label>
                    <input type="text" class="form-control" value="{{$ekspedisi->tujuan}}" readonly>
                </div>
            </div>
            <hr>
            <div class="form-row">
                <div class="form-group col-12 col-md-4">
                    <label>Solar</label>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                        <input class="form-control" type="number" name="solar" placeholder="Tulis biaya solar" value="{{$biaya?$biaya->solar:''}}" required>
                    </div>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label>Timbangan</label>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                        <input class="form-control" type="number" name="timbangan" placeholder="Tulis biaya timbangan" value="{{$biaya?$biaya->timbangan:''}}" required>
                    </div>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label>Makan</label>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                        <input class="form-control" type="number" name="makan" placeholder="Tulis biaya makan" value="{{$biaya?$biaya->makan:''}}" required>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-12 col-md-4">
                    <label>Kuli</label>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                        <input class="form-control" type="number" name="kuli" placeholder="Tulis upah kuli" value="{{$biaya?$biaya->kuli:''}}" required>
                    </div>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label>Lain-Lain</label>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                        <input class="form-control" type="number" name="lain" placeholder="Tulis biaya lain-lain" value="{{$biaya?$biaya->lain:''}}" required>
                    </div>
                </div>
            </div>
            @if(strtolower($ekspedisi->tujuan) == 'surabaya')
            <hr>
            <h5>KHUSUS EKSPEDISI SURABAYA</h5>
            <div class="form-row mt-4">
                <div class="form-group col-12 col-md-4">
                    <label for="">Biaya Tol</label>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                        <input class="form-control" type="number" name="tol" placeholder="Tulis biaya tol" value="{{$biaya?$biaya->tol:''}}">
                    </div>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label for="">Ongkos Polisi</label>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                        <input class="form-control" type="number" name="polisi" placeholder="Tulis ongkos polisi" value="{{$biaya?$biaya->polisi:''}}">
                    </div>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label for="">Biaya Parkir</label>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                        <input class="form-control" type="number" name="parkir" placeholder="Tulis biaya parkir" value="{{$biaya?$biaya->parkir:''}}">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-12 col-md-4">
                    <label for="">Masuk Ruko</label>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                        <input class="form-control" type="number" name="masuk_ruko" placeholder="Tulis biaya masuk ruko" value="{{$biaya?$biaya->masuk_ruko:''}}">
                    </div>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label for="">Pengurus Pelabuhan</label>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                        <input class="form-control" type="number" name="pengurus_pelabuhan" placeholder="Tulis biaya pengurus pelabuhan" value="{{$biaya?$biaya->pengurus_pelabuhan:''}}">
                    </div>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label for="">Pak Serap</label>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                        <input class="form-control" type="number" name="pak_serap" placeholder="Tulis biaya pak serap" value="{{$biaya?$biaya->pak_serap:''}}">
                    </div>
                </div>
            </div>
            @endif
            <hr>
            <h5>VALIDASI BIAYA</h5>
            <div class="form-row mt-4">
                <div class="form-group col-12 col-md-4">
                    <label for="">Uang Saku</label>
                    <div class="input-group">
                        <span class="input-group-prepend"><span class="input-group-text">Rp</span></span>
                        <input class="form-control" type="number" name="uang_saku" placeholder="Tulis biaya pak serap" value="{{$biaya?$biaya->uang_saku:''}}" required>
                    </div>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label for="">Solar Pertama</label>
                    <div class="input-group">
                        <span class="input-group-prepend"><span class="input-group-text">Rp</span></span>
                        <input class="form-control" type="number" name="solar_pertama" placeholder="Tulis biaya pak serap" value="{{$biaya?$biaya->solar_pertama:''}}" required>
                    </div>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label for="">Upah Sopir</label>
                    <div class="input-group">
                        <span class="input-group-prepend"><span class="input-group-text">Rp</span></span>
                        <input class="form-control" type="number" name="upah" placeholder="Tulis biaya pak serap" value="{{$biaya?$biaya->upah:''}}" required>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header bg-dark tx-white d-flex align-items-center">
                    <input type="checkbox" name="hasbalen" value="1" {{$balen ? 'checked' : ''}}>
                    <i class="fa fa-cart-arrow-down mr-2 ml-2"></i>
                    <span>Input Balen</span>
                </div>
                <div class="card-body bg-secondary" id="balenForm">
                    <div class="form-group">
                        <label class="tx-white">Jenis Barang</label>
                        <input type="text" name="balen[jenis_barang]" class="form-control" value="{{$balen?$balen->jenis_barang:''}}" placeholder="Tulis jenis barang balen">
                    </div>
                    <div class="form-row">
                        <div class="form-group col-12 col-md-3">
                            <label class="tx-white">Area</label>
                            <select name="balen[kota]" class="form-control select2" data-placeholder="pilih kota">
                                <option></option>
                                @foreach($area AS $a)
                                <option value="{{$a->kota}}" data-rit="{{$a->per_rit}}" data-tonase="{{$a->per_ton}}" {{$balen && $balen->kota==$a->kota?'selected':''}}>{{$a->kota}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-12 col-md-3">
                            <label class="tx-white">Jenis Perhitungan</label>
                            <select name="balen[jenis_perhitungan_berat]" class="form-control">
                                <option value="rit" {{$balen && $balen->jenis_perhitungan_berat=='rit'?'selected':''}}>Rit</option>
                                <option value="tonase" {{$balen && $balen->jenis_perhitungan_berat=='tonase'?'selected':''}}>Tonase</option>
                            </select>
                        </div>
                        <div class="form-group col-12 col-md-3">
                            <label class="tx-white">Berat Muatan</label>
                            <div class="input-group">
                                <input type="number" step=".01" name="balen[berat]" value="{{$balen?$balen->berat:''}}" class="form-control">
                                <div class="input-group-append"><span class="input-group-text">Kg</span></div>
                            </div>
                        </div>
                        <div class="form-group col-12 col-md-3">
                            <label class="tx-white">Total</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                                <input type="number" name="balen[biaya]" value="{{$balen?$balen->biaya:''}}" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <a href="{{site_url('ekspedisi/biaya')}}" class="btn btn-warning" onclick="return confirm('Apakah anda yakin ingin kembali?')"><i class="fa fa-arrow-left mr-2"></i>Kembali</a>
                <button class="btn btn-primary"><i class="fa fa-save mr-2"></i>Simpan</button>
            </div>
        </form>
    </div>
</div>
@end

@section('js')
<script>
    // INIT
    init();
    // FUNCTIONS
    function init() {
        if ($("[name=hasbalen]").is(":checked")) {
            $("#balenForm").slideDown();
        } else {
            $("#balenForm").slideUp();
        }
    }
    // EVENTS
    $("[name='balen[kota]']").on('change', function(e) {
        var metode = $("[name='balen[jenis_perhitungan_berat]']").val();
        var biaya = $("[name='balen[kota]'] option:selected").data(metode);
        if (metode == 'tonase') {
            var berat = $("[name='balen[berat]']").val();
            biaya *= berat;
        }
        $("[name='balen[biaya]']").val(biaya);
    });

    $("[name='balen[jenis_perhitungan_berat]']").on('change', function() {
        $("[name='balen[kota]']").trigger("change");
    });

    $("[name='balen[berat]']").on('blur', function() {
        $("[name='balen[kota]']").trigger("change");
    });

    $("[name=hasbalen]").on("change", function(e) {
        if ($(e.currentTarget).is(":checked")) {
            $("#balenForm").slideDown();
        } else {
            $("#balenForm").slideUp();
        }
    });
</script>
@end