@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{site_url('ekspedisi')}}">Ekspedisi</a></li>
        <li class="breadcrumb-item active">Detail Transaksi</li>
    </ol>
</nav>
@end

@section('content')
<div class="card">
    <div class="card-header bg-dark tx-white"><i class="fa fa-clock-o"></i> TIMELINE</div>
    <div class="card-body px-0 py-0">
        <table class="table table-bordered table-sm mb-0">
            <tr>
                <td class="text-center tx-bold">ENTRI DATA</td>
                <td class="text-center tx-bold">INSEPEKSI KEBERANGKATAN</td>
                <td class="text-center tx-bold">INSEPEKSI PULANG</td>
                <td class="text-center tx-bold">ENTRI BIAYA</td>
                <td class="text-center tx-bold">VALIDASI BIAYA</td>
                <td class="text-center tx-bold">VERIFIKASI AKHIR</td>
            </tr>
            <tr>
                <td class="text-center">
                    <h5>{{$ekspedisi->tanggal_entri?datify($ekspedisi->tanggal_entri, 'd/m/y, H:i'):'<i class="fa fa-ban"></i>'}}</h5>
                </td>
                <td class="text-center">
                    <h5>{{$ekspedisi->tanggal_cek_berangkat?datify($ekspedisi->tanggal_cek_berangkat, 'd/m/y, H:i'):'<i class="fa fa-ban"></i>'}}</h5>
                </td>
                <td class="text-center">
                    <h5>{{$ekspedisi->tanggal_cek_pulang?datify($ekspedisi->tanggal_cek_pulang, 'd/m/y, H:i'):'<i class="fa fa-ban"></i>'}}</h5>
                </td>
                <td class="text-center">
                    <h5>{{$ekspedisi->tanggal_input_biaya?datify($ekspedisi->tanggal_input_biaya, 'd/m/y, H:i'):'<i class="fa fa-ban"></i>'}}</h5>
                </td>
                <td class="text-center">
                    <h5>{{$ekspedisi->tanggal_validasi_biaya?datify($ekspedisi->tanggal_validasi_biaya, 'd/m/y, H:i'):'<i class="fa fa-ban"></i>'}}</h5>
                </td>
                <td class="text-center">
                    <h5>{{$ekspedisi->tanggal_verifikasi_akhir?datify($ekspedisi->tanggal_verifikasi_akhir, 'd/m/y, H:i'):'<i class="fa fa-ban"></i>'}}</h5>
                </td>
            </tr>
        </table>
    </div>
</div>
<!-- batas -->
<div class="card mt-5">
    <div class="card-header bg-dark tx-white"><i class="fa fa-info"></i> INFORMASI EKSPEDISI</div>
    <div class="card-body">
        <span class="card-title">Informasi Umum</span>
        <div class="row">
            <div class="col-12 col-md-6">
                <table class="table table-striped table-bordered table-sm">
                    <tr>
                        <td>No. Resi</td>
                        <td>{{$ekspedisi->no_resi}}</td>
                    </tr>
                    <tr>
                        <td>No. Polisi</td>
                        <td>{{$ekspedisi->no_pol}}</td>
                    </tr>
                    <tr>
                        <td>Sopir</td>
                        <td>{{$ekspedisi->pegawai_nama}}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Jalan</td>
                        <td>{{datify($ekspedisi->tanggal_jalan, 'd/m/Y')}}</td>
                    </tr>
                </table>
            </div>
            <div class="col-12 col-md-6">
                <table class="table table-striped table-bordered table-sm">
                    <tr>
                        <td>Tujuan</td>
                        <td>{{$ekspedisi->tujuan}}</td>
                    </tr>
                    <tr>
                        <td>Jenis Barang</td>
                        <td>{{$ekspedisi->jenis_barang}}</td>
                    </tr>
                    <tr>
                        <td>Berat Barang</td>
                        <td>{{$ekspedisi->berat}}</td>
                    </tr>
                    <tr>
                        <td>Jenis Perhitungan</td>
                        <td>{{$ekspedisi->jenis_perhitungan_berat}}</td>
                    </tr>
                </table>
            </div>
        </div>
        <span class="card-title">Informasi Customer</span>
        <div class="row">
            <div class="col-12 col-md-6">
                <table class="table table-striped table-bordered table-sm">
                    <tr><td class="tx-bold" colspan="2">Pengirim</td></tr>
                    <tr><td>Nama Instansi</td><td>{{$ekspedisi->sender_nama}}</td></tr>
                    <tr><td>Alamat</td><td>{{$ekspedisi->sender_alamat}}</td></tr>
                    <tr><td>Telpon</td><td>{{$ekspedisi->sender_telpon}}</td></tr>
                </table>
            </div>
            <div class="col-12 col-md-6">
                <table class="table table-striped table-bordered table-sm">
                    <tr><td class="tx-bold" colspan="2">Penerima</td></tr>
                    <tr><td>Nama Instansi</td><td>{{$ekspedisi->reciver_nama}}</td></tr>
                    <tr><td>Alamat</td><td>{{$ekspedisi->reciver_alamat}}</td></tr>
                    <tr><td>Telpon</td><td>{{$ekspedisi->reciver_telpon}}</td></tr>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- batas -->
<div class="card mt-5">
    <div class="card-header bg-dark tx-white"><i class="fa fa-check"></i> PENGECEKAN KELENGKAPAN</div>
    <div class="card-body">
        <div class="row">
            <div class="col-12 col-md-6">
                <b>Berangkat</b>
                <table class="table table-striped table-bordered table-sm">
                    @if(!empty($cek_berangkat))
                    <tr><td>KM</td><td>{{$cek_berangkat->km}}</td></tr>
                    <tr><td>Pemeriksa</td><td>{{$cek_berangkat->pemeriksa}}</td></tr>
                    <tr><td>Pemadam</td><td>{{$cek_berangkat->pemadam==1?'ada':'tidak ada'}}</td></tr>
                    <tr><td>Dongkrak</td><td>{{$cek_berangkat->dongkrak==1?'ada':'tidak ada'}}</td></tr>
                    <tr><td>Kunci Roda</td><td>{{$cek_berangkat->kunci_roda==1?'ada':'tidak ada'}}</td></tr>
                    <tr><td>Stang Pendek</td><td>{{$cek_berangkat->pemadam==1?'ada':'tidak ada'}}</td></tr>
                    <tr><td>Stang Panjang</td><td>{{$cek_berangkat->stang_panjang==1?'ada':'tidak ada'}}</td></tr>
                    <tr><td>Terpal</td><td>{{$cek_berangkat->terpal==1?'ada':'tidak ada'}}</td></tr>
                    <tr><td>Tampar</td><td>{{$cek_berangkat->tampar==1?'ada':'tidak ada'}}</td></tr>
                    <tr><td>P3K</td><td>{{$cek_berangkat->p3k==1?'ada':'tidak ada'}}</td></tr>
                    <tr><td>Ban Serep</td><td>{{$cek_berangkat->ban_serep==1?'ada':'tidak ada'}}</td></tr>
                    <tr><td>STNK</td><td>{{$cek_berangkat->stnk==1?'ada':'tidak ada'}}</td></tr>
                    <tr><td>Surat Jalan</td><td>{{$cek_berangkat->surat_jalan==1?'ada':'tidak ada'}}</td></tr>
                    <tr><td>Buku Kir</td><td>{{$cek_berangkat->buku_kir==1?'ada':'tidak ada'}}</td></tr>
                    <tr><td>Cek Fisik</td><td>{{$cek_berangkat->cek_fisik==1?'ada':'tidak ada'}}</td></tr>
                    @endif
                </table>
            </div>
            <div class="col-12 col-md-6">
                <b>Pulang</b>
                <table class="table table-striped table-bordered table-sm">
                    @if(!empty($cek_pulang))
                    <tr><td>KM</td><td>{{$cek_pulang->km}}</td></tr>
                    <tr><td>Pemeriksa</td><td>{{$cek_pulang->pemeriksa}}</td></tr>
                    <tr><td>Pemadam</td><td>{{$cek_pulang->pemadam==1?'ada':'tidak ada'}}</td></tr>
                    <tr><td>Dongkrak</td><td>{{$cek_pulang->dongkrak==1?'ada':'tidak ada'}}</td></tr>
                    <tr><td>Kunci Roda</td><td>{{$cek_pulang->kunci_roda==1?'ada':'tidak ada'}}</td></tr>
                    <tr><td>Stang Pendek</td><td>{{$cek_pulang->pemadam==1?'ada':'tidak ada'}}</td></tr>
                    <tr><td>Stang Panjang</td><td>{{$cek_pulang->stang_panjang==1?'ada':'tidak ada'}}</td></tr>
                    <tr><td>Terpal</td><td>{{$cek_pulang->terpal==1?'ada':'tidak ada'}}</td></tr>
                    <tr><td>Tampar</td><td>{{$cek_pulang->tampar==1?'ada':'tidak ada'}}</td></tr>
                    <tr><td>P3K</td><td>{{$cek_pulang->p3k==1?'ada':'tidak ada'}}</td></tr>
                    <tr><td>Ban Serep</td><td>{{$cek_pulang->ban_serep==1?'ada':'tidak ada'}}</td></tr>
                    <tr><td>STNK</td><td>{{$cek_pulang->stnk==1?'ada':'tidak ada'}}</td></tr>
                    <tr><td>Surat Jalan</td><td>{{$cek_pulang->surat_jalan==1?'ada':'tidak ada'}}</td></tr>
                    <tr><td>Buku Kir</td><td>{{$cek_pulang->buku_kir==1?'ada':'tidak ada'}}</td></tr>
                    <tr><td>Cek Fisik</td><td>{{$cek_pulang->cek_fisik==1?'ada':'tidak ada'}}</td></tr>
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>
<!-- batas -->
<div class="card mt-5">
    <div class="card-header bg-dark tx-white"><i class="fa fa-calculator"></i> BIAYA</div>
    <div class="card-body">
        @if(!empty($biaya))
        <div class="row">
            <div class="col-12 col-md-6">
                <h5>Berangkat</h5>
                <table class="table table-striped table-bordered table-sm">
                    <tr><td>Uang Saku</td><td>Rp {{monefy($biaya->uang_saku, false)}}</td></tr>
                    <tr><td>Solar Pertama</td><td>Rp {{monefy($biaya->solar_pertama, false)}}</td></tr>
                </table>
                <h5>Pulang</h5>
                <table class="table table-striped table-bordered table-sm">
                    <tr><td>Solar</td><td>Rp {{monefy($biaya->solar, false)}}</td></tr>
                    <tr><td>Timbangan</td><td>Rp {{monefy($biaya->timbangan, false)}}</td></tr>
                    <tr><td>Makan</td><td>Rp {{monefy($biaya->timbangan, false)}}</td></tr>
                    <tr><td>Kuli</td><td>Rp {{monefy($biaya->kuli, false)}}</td></tr>
                    <tr><td>Lain-Lain</td><td>Rp {{monefy($biaya->lain, false)}}</td></tr>
                </table>
                @if(strtolower($ekspedisi->tujuan) == 'surabaya')
                <h5>Khusus Ekspedisi Surabaya</h5>
                <table class="table table-striped table-bordered table-sm">
                    <tr><td>Tol</td><td>Rp {{monefy($biaya->tol, false)}}</td></tr>
                    <tr><td>Polisi</td><td>Rp {{monefy($biaya->polisi, false)}}</td></tr>
                    <tr><td>Parkir</td><td>Rp {{monefy($biaya->parkir, false)}}</td></tr>
                    <tr><td>Masuk Ruko</td><td>Rp {{monefy($biaya->masuk_ruko, false)}}</td></tr>
                    <tr><td>Pengurus Pelabuhan</td><td>Rp {{monefy($biaya->pengurus_pelabuhan, false)}}</td></tr>
                    <tr><td>Pak Serap</td><td>Rp {{monefy($biaya->pak_serap, false)}}</td></tr>
                </table>
                @endif
            </div>
            <div class="col-12 col-md-6">
                <h5>Total & Laba/Rugi</h5>
                <table class="table table-striped table-bordered table-sm">
                    <?php $total = $biaya->solar + $biaya->timbangan + $biaya->tol + $biaya->polisi + $biaya->parkir + $biaya->masuk_ruko + $biaya->kuli + $biaya->makan + $biaya->pengurus_pelabuhan + $biaya->pak_serap + $biaya->lain + $biaya->upah; ?>
                    <tr><td>Upah Sopir</td><td>Rp {{monefy($biaya->upah, false)}}</td></tr>
                    <tr><td>Total Pengeluaran</td><td>Rp {{monefy($total, false)}}</td></tr>
                    <tr><td>Ongkos Ekspedisi</td><td>Rp {{monefy($ekspedisi->biaya, false)}}</td></tr>
                    <tr><td>Ongkos Balen</td><td>Rp {{monefy($biaya->balen, false)}}</td></tr>
                    <tr><td>Laba/Rugi</td><td>Rp {{monefy(($ekspedisi->biaya + $biaya->balen) - $total, false)}}</td></tr>
                </table>
                @if(!empty($balen))
                <h5>Balen</h5>
                <table class="table table-striped table-bordered table-sm">
                    <tr><td>Kota</td><td>{{$balen->kota}}</td></tr>
                    <tr><td>Jenis Barang</td><td>{{$balen->jenis_barang}}</td></tr>
                    <tr><td>Jenis Perhitungan Berat</td><td>{{$balen->jenis_perhitungan_berat}}</td></tr>
                    <tr><td>Berat</td><td>{{$balen->berat}}</td></tr>
                </table>
                @endif
                @if(empty($ekspedisi->tanggal_verifikasi_akhir) && !empty($ekspedisi->tanggal_validasi_biaya))
                <a href="{{site_url('ekspedisi/finish/'.$ekspedisi->id)}}" class="btn btn-block btn-primary btn-lg" onclick="return confirm('Apakah anda yakin?')"><i class="fa fa-check mr-2"></i>SELESAIKAN TRANSAKSI</a>
                @elseif(!empty($ekspedisi->tanggal_verifikasi_akhir) && !empty($ekspedisi->tanggal_validasi_biaya))
                <button class="btn btn-block btn-success btn-lg" disabled><i class="fa fa-check mr-2"></i>EKSPEDISI TELAH SELESAI</button>
                @endif
            </div>
        </div>
        @endif
    </div>
</div>
@end