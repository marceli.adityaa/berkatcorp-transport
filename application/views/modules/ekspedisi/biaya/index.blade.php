@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{site_url('ekspedisi')}}">Ekspedisi</a></li>
        <li class="breadcrumb-item active">Biaya</li>
    </ol>
</nav>
@end

@section('content')
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('biaya/index?')?>" id="form-filter">
                    <?php $get = $this->input->get()?>
                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>

                        <label class="col-sm-3 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Kendaraan</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="kendaraan" class="form-control select2">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$kendaraan as $row){
                                        if(isset($get['kendaraan']) && $get['kendaraan'] == $row['id']){
                                            echo '<option value="'.$row['id'].'" selected>'.strtoupper($row['no_pol']).'</option>';
                                        }else{
                                            echo '<option value="'.$row['id'].'">'.strtoupper($row['no_pol']).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>

                        <label class="col-sm-3 form-control-label">Supir</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="supir" class="form-control select2">
                                <option value="all">All</option>
                                <?php
                                    foreach($supir as $row){
                                        if(isset($get['supir']) && $get['supir'] == $row->id){
                                            echo '<option value="'.$row->id.'" selected>'.ucwords($row->nama).'</option>';
                                        }else{
                                            echo '<option value="'.$row->id.'">'.ucwords($row->nama).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Customer</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="customer" class="form-control select2">
                                <option value="all">All</option>
                                <?php
                                    foreach($customer as $row){
                                        if(isset($get['customer']) && $get['customer'] == $row['id']){
                                            echo '<option value="'.$row['id'].'" selected>'.ucwords($row['nama']).'</option>';
                                        }else{
                                            echo '<option value="'.$row['id'].'">'.ucwords($row['nama']).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                        <label class="col-sm-3 form-control-label">Status</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control select2">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$status as $key => $row){
                                        if(!empty($get['status']) && $get['status'] == $key){
                                            echo '<option value="'.$key.'" selected>'.ucwords($row).'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.ucwords($row).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label"></label>
                        <div class="col-sm-9 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<!-- batas -->
<div class="card">
    <div class="card-header card-header-default">DATA EKSPEDISI</div>
    <div class="card-body">
    <table class="table table-white table-bordered" id="table-data">
            <thead>
                <tr>
                    <th class="text-left" nowrap>AKSI</th>
                    <th class="text-center">STATUS</th>
                    <th class="text-center" data-sortable="true">NO RESI</th>
                    <th class="text-center" data-sortable="true">TGL JALAN</th>
                    <th class="text-center" data-sortable="true">TGL KEMBALI</th>
                    <th class="text-center" data-sortable="true">CUSTOMER</th>
                    <th class="text-center" data-sortable="true">KENDARAAN</th>
                    <th class="text-left" data-sortable="true">SUPIR</th>
                    <th class="text-center" data-sortable="true">TUJUAN</th>
                    <th class="text-center">BERAT MUATAN</th>
                    <th class="text-center">SOLAR PERTAMA</th>
                    <th class="text-center">UANG SAKU</th>
                    <th class="text-right">ONGKOS BERANGKAT</th>
                    <th class="text-right">BIAYA EKSPEDISI</th>
                    <th class="text-right">PENDAPATAN BALEN</th>
                    <th class="text-right">LABA / RUGI</th>
                    <th class="text-right">SISA UANG SAKU</th>
                    <th class="text-right">UANG KEMBALI</th>
                </tr>
            </thead>
            <tbody>
                @if (!empty($ekspedisi))
                    @foreach ($ekspedisi as $row)
                        <tr>
                            <td>
                                <a href="{{site_url('biaya/form/'.$row['id'])}}" class='btn btn-info btn-sm' title='Detail Biaya'><i class='icon-list'></i></a>
                            </td>
                            <td>
                                @if($row['status_biaya'] == 0 && $row['is_done'] == 0)
                                <span class="badge badge-secondary">menunggu input</span>
                                @elseif ($row['status_biaya'] == 1 && $row['is_done'] == 0)
                                <span class="badge badge-warning">menunggu persetujuan</span>
                                @elseif ($row['status_biaya'] == 1 && $row['is_done'] == 1)
                                <span class="badge badge-success">disetujui</span>
                                @elseif ($row['status_biaya'] == 1 && $row['is_done'] == 2)
                                <span class="badge badge-danger">dibatalkan</span>
                                @endif
                            </td>
                            <td>
                                <label class="badge badge-light">{{$row['no_resi']}}</label>
                            </td>
                            <td>
                                <label class="badge badge-light">{{$row['tanggal_jalan']}}</label>
                            </td>
                            <td>
                                @if($row['tanggal_kembali'] == '0000-00-00')
                                -
                                @else 
                                    <label class="badge badge-light">{{$row['tanggal_kembali']}}</label>
                                @endif
                            </td>
                            <td>
                                <label class="badge badge-light">{{ucwords($row['customer'])}}</label>
                            </td>
                            <td>
                                <label class="badge badge-light">{{$row['no_pol']}}</label>
                            </td>
                            <td>
                                <label class="badge badge-light">{{ucwords($row['supir'])}}</label>
                            </td>
                            <td>
                                @if($row['is_sby'])
                                    <label class="badge badge-warning">{{ucwords($row['kota'])}}</label>
                                @else 
                                    <label class="badge badge-light">{{ucwords($row['kota'])}}</label>
                                @endif
                            </td>

                            <td>
                                <label class="badge badge-light">{{($row['berat_muatan'])}}</label>
                                <br>
                                <label class="badge badge-info">{{($row['jenis_perhitungan_berat'])}}</label>
                            </td>
                            <td>
                                {{monefy($row['solar_pertama'], false)}}
                            </td>
                            
                            <td>
                                {{monefy($row['uang_saku'], false)}}
                            </td>
                            <td>
                                {{monefy($row['biaya'], false)}}
                            </td>
                            <td>
                                {{monefy($row['total_biaya'], false)}}
                            </td>
                            <td>
                                {{monefy($row['total_balen'], false)}}
                            </td>
                            <?php 
                                $laba = ($row['biaya'] + $row['total_balen'] - $row['solar_pertama'] - $row['total_biaya']);
                                $kembalian = ($row['uang_saku'] + $row['total_balen_cash'] - $row['total_biaya']);
                                $sisa_uangsaku = ($row['uang_saku'] - $row['total_biaya']);
                            ?>
                            <td>
                                @if($laba < 0)
                                <span class="tx-danger">{{monefy($laba, false)}}</span>
                                @else 
                                <span class="tx-success">+{{monefy($laba, false)}}</span>
                                @endif
                            </td>
                            <td>
                                @if($sisa_uangsaku < 0)
                                <span class="tx-danger">{{monefy($sisa_uangsaku, false)}}</span>
                                @else 
                                {{monefy($sisa_uangsaku, false)}}
                                @endif
                            </td>
                            <td>
                                @if($kembalian < 0)
                                <span class="tx-danger">{{monefy($kembalian, false)}}</span>
                                @else 
                                {{monefy($kembalian, false)}}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        
    </div>
</div>
@end

@section('modal')
<div id="mDetail" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">Detail Biaya</div>
            <div class="modal-body px-0 py-0">
                <table class="table table-bordered table-striped mb-0"></table>
                <button class="btn btn-block btn-warning" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script>
    // VAR
    var url = "{{site_url('api/internal/biaya')}}";
    var token = "{{$this->session->auth['token']}}";
    // FUNCTIONS
    $('#table-data').bootstrapTable({
        pagination: true,
        search:true,
        showToggle: true,
        showColumns: true,
        pageSize:10,
        striped: true,
        showFilter: true,
        toolbar: '#toolbar'
    });

    function detail(ekspedisi_id) {
        $.getJSON(url + '/get_detail/' + token + '?ekspedisi_id=' + ekspedisi_id, function(result) {
            var html = "<tr><td>Solar</td><td>" + formatCurrency(result.solar) + "</td></tr>";
            html += "<tr><td>Timbangan</td><td>" + formatCurrency(result.timbangan) + "</td></tr>";
            html += "<tr><td>Makan</td><td>" + formatCurrency(result.makan) + "</td></tr>";
            html += "<tr><td>Kuli</td><td>" + formatCurrency(result.kuli) + "</td></tr>";
            html += "<tr><td>Lain-lain</td><td>" + formatCurrency(result.lain) + "</td></tr>";
            if (result.tujuan == 'surabaya') {
                html += "<tr><td>Biaya Tol</td><td>" + formatCurrency(result.tol) + "</td></tr>";
                html += "<tr><td>Ongkos Polisi</td><td>" + formatCurrency(result.polisi) + "</td></tr>";
                html += "<tr><td>Biaya Parkir</td><td>" + formatCurrency(result.parkir) + "</td></tr>";
                html += "<tr><td>Masuk Ruko</td><td>" + formatCurrency(result.masuk_ruko) + "</td></tr>";
                html += "<tr><td>Pengurus Pelabuhan</td><td>" + formatCurrency(result.pengurus_pelabuhan) + "</td></tr>";
                html += "<tr><td>Pak Serap</td><td>" + formatCurrency(result.pak_serap) + "</td></tr>";
            }
            $("#mDetail table").empty().html(html);
            $("#mDetail").modal('show');
        });
    }
</script>
@end