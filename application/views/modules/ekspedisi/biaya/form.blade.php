@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{site_url('ekspedisi')}}">Ekspedisi</a></li>
        <li class="breadcrumb-item"><a href="{{site_url('biaya')}}">Biaya</a></li>
        <li class="breadcrumb-item active">Formulir Isian</li>
    </ol>
</nav>
@end

@section('content')
<div class="card">
    <div class="card-header"><h5>FORM INPUT BIAYA | #{{$ekspedisi->no_resi}}</h5></div>
    <div class="card-body">
        <form action="{{site_url('biaya/save/'.$this->session->auth['token'])}}" method="POST" id="form-biaya">
            <input type="hidden" name="id" value="{{$biaya?$biaya->id:''}}">
            <input type="hidden" name="ekspedisi_id" value="{{$ekspedisi->id}}">
            <div class="form-row">
                <div class="form-group col-12 col-md-4">
                    <label>Customer</label>
                    <input type="text" class="form-control" value="{{$ekspedisi->customer}}" readonly>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label>Nomor Polisi</label>
                    <input type="text" class="form-control" value="{{$ekspedisi->no_pol}}" readonly>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label>Tujuan</label>
                    <input type="text" class="form-control" value="{{$ekspedisi->tujuan}}" readonly>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-12 col-md-4">
                    <label>Tanggal Berangkat</label>
                    <input type="text" class="form-control" value="{{$ekspedisi->tanggal_jalan}}" readonly>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label>Tanggal Kembali</label>
                    <input type="date" name="tanggal_kembali" class="form-control" value="{{$ekspedisi->tanggal_kembali}}" required>
                </div>
            </div>
            <hr>
            <div class="form-row">
                <div class="form-group col-12 col-md-4">
                    <label>Solar</label>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                        <input class="form-control autonumeric" type="text" name="solar" placeholder="Tulis biaya solar" value="{{$biaya?$biaya->solar:''}}" required>
                    </div>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label>Timbangan</label>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                        <input class="form-control autonumeric" type="text" name="timbangan" placeholder="Tulis biaya timbangan" value="{{$biaya?$biaya->timbangan:''}}" required>
                    </div>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label>Upah Supir</label>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                        <input class="form-control autonumeric" type="text" name="upah" placeholder="Tulis upah supir" value="{{$biaya?$biaya->upah:''}}" required>
                    </div>
                </div>
                
            </div>
            <div class="form-row">
                <div class="form-group col-12 col-md-4">
                    <label>Makan</label>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                        <input class="form-control autonumeric" type="text" name="makan" placeholder="Tulis biaya makan" value="{{$biaya?$biaya->makan:''}}" required>
                    </div>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label>Kuli</label>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                        <input class="form-control autonumeric" type="text" name="kuli" placeholder="Tulis upah kuli" value="{{$biaya?$biaya->kuli:''}}" required>
                    </div>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label>Lain-Lain</label>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                        <input class="form-control autonumeric" type="text" name="lain" placeholder="Tulis biaya lain-lain" value="{{$biaya?$biaya->lain:''}}" required>
                    </div>
                </div>
            </div>
            @if($ekspedisi->is_sby == 1)
            <hr>
            <h5>BIAYA TAMBAHAN (SBY)</h5>
            <div class="form-row mt-4">
                <div class="form-group col-12 col-md-4">
                    <label for="">Biaya Tol</label>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                        <input class="form-control autonumeric" type="text" name="tol" placeholder="Tulis biaya tol" value="{{$biaya?$biaya->tol:''}}">
                    </div>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label for="">Ongkos Polisi</label>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                        <input class="form-control autonumeric" type="text" name="polisi" placeholder="Tulis ongkos polisi" value="{{$biaya?$biaya->polisi:''}}">
                    </div>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label for="">Biaya Parkir</label>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                        <input class="form-control autonumeric" type="text" name="parkir" placeholder="Tulis biaya parkir" value="{{$biaya?$biaya->parkir:''}}">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-12 col-md-4">
                    <label for="">Masuk Ruko</label>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                        <input class="form-control autonumeric" type="text" name="masuk_ruko" placeholder="Tulis biaya masuk ruko" value="{{$biaya?$biaya->masuk_ruko:''}}">
                    </div>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label for="">Pengurus Pelabuhan</label>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                        <input class="form-control autonumeric" type="text" name="pengurus_pelabuhan" placeholder="Tulis biaya pengurus pelabuhan" value="{{$biaya?$biaya->pengurus_pelabuhan:''}}">
                    </div>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label for="">Pak Serap</label>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                        <input class="form-control autonumeric" type="text" name="pak_serap" placeholder="Tulis biaya pak serap" value="{{$biaya?$biaya->pak_serap:''}}">
                    </div>
                </div>
            </div>
            @endif
            <hr>
            <div class="card-header">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck1" name="has_balen" value="1" {{$ekspedisi->has_balen == 1?'checked':''}}>
                    <label class="custom-control-label" for="customCheck1">Balen</label>
                </div>
            </div>
            <div class="card-body has-balen">
                <div id="toolbar" class="mg-b-10">
                @if($ekspedisi->status_biaya == 0 && $ekspedisi->is_done != 1)
                    <button class="btn btn-primary" type="button" id="tambah">+ Tambah Data</button>
                @endif
                </div>
                <?php 
                    $total_balen = 0;
                    $total_kembalian_balen = 0;
                ?>
                @if(!empty($balen))    
                <table class="table table-striped mg-t-10 table-white" id="tabel_penjualan">
                    <thead>
                        <tr>
                            <th data-formatter="reformat_number" class="text-center">No.</th>
                            <th data-searchable="false">Aksi</th>
                            <th data-sortable="true">Tanggal Balen</th>
                            <th data-sortable="true">Customer</th>
                            <th data-sortable="true">Kota</th>
                            <th data-sortable="true">Jenis Barang</th>
                            <th data-sortable="true">Keterangan</th>
                            <th data-sortable="true">Jenis Hitungan</th>
                            <th data-sortable="true">Pembayaran</th>
                            <th data-sortable="true">Kuantitas</th>
                            <th data-sortable="true">Harga Satuan</th>
                            <th data-sortable="true">Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $no = 1;
                            
                            if(!empty($balen)){
                                foreach($balen as $row){
                                    echo "<tr>";
                                    echo "<td class='text-center'>".$no++."</td>";
                                    echo "<td class='text-nowrap'>";
                                    if($ekspedisi->status_biaya == 0){
                                        echo "<button type='button' class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus data?' onclick='hapus_data(this)' data-id='".$row['id']."'><i class='fa fa-trash'></i></button> ";
                                    }
                                    echo "</td>";
                                    echo "<td>".$row['tanggal_balen']."</td>";
                                    echo "<td>".$row['customer']."</td>";
                                    echo "<td>".$row['kota']."</td>";
                                    echo "<td><label class='badge badge-light'>".ucwords($row['jenis_barang'])."</label></td>";
                                    echo "<td>".$row['keterangan']."</td>";
                                    echo "<td><label class='badge badge-light'>".ucwords($row['jenis_perhitungan_berat'])."</label></td>";
                                    if($row['jenis_pembayaran'] == 'cash'){
                                        echo "<td><label class='badge badge-primary'>".ucwords($row['jenis_pembayaran'])."</label></td>";
                                        $total_kembalian_balen += $row['biaya'];
                                    }else{
                                        echo "<td><label class='badge badge-warning'>".ucwords($row['jenis_pembayaran'])."</label><br><label class='badge badge-light'>".$row['kas_label']."</label></td>";
                                    }
                                    echo "<td>".monefy($row['berat'], false)."</td>";
                                    echo "<td>".monefy($row['harga_satuan'], false)."</td>";
                                    echo "<td>".monefy($row['biaya'], false)."</td>";
                                    echo "</tr>";
                                    $total_balen += $row['biaya'];
                                }
                            }
                        ?>
                    </tbody>
                </table>
                @else
                <p class="text-center">Tidak ada data balen</p>
                @endif
            </div>
            <table class="table table-white">
                <tr>
                    <td>Uang Saku</td>
                    <!-- <td>:</td> -->
                    <td><b>{{monefy($ekspedisi->uang_saku, false)}}</b></td>
                </tr>
                <tr>
                    <td>Ongkos Berangkat</td>
                    <!-- <td>:</td> -->
                    <td><b>{{monefy($ekspedisi->biaya, false)}}</b></td>
                </tr>
                <tr>
                    <td>Solar Pertama</td>
                    <!-- <td>:</td> -->
                    <td><b>{{monefy($ekspedisi->solar_pertama, false)}}</b></td>
                </tr>
                <tr>
                    <td>Total Biaya</td>
                    <!-- <td>:</td> -->
                    <td><b><span id="total_biaya"></span></b></td>
                </tr>
                @if($ekspedisi->has_balen)
                <tr>
                    <td>Total Balen</td>
                    <!-- <td>:</td> -->
                    <td><b>{{monefy($total_balen, false)}}</span></b> 
                    <br>&emsp;<small>{{monefy($total_kembalian_balen)}} (cash) </small>
                    <br>&emsp;<small>{{monefy($total_balen - $total_kembalian_balen)}} (transfer)</small></td>
                </tr>
                @endif
                <tr>
                    <td><b>Sisa Uang Saku</b> <small>(uang saku - total pengeluaran) *tidak termasuk solar pertama</small></td>
                    <!-- <td>:</td> -->
                    <td><b><span id="sisa_uangsaku" class="tx-18"></span></b></td>
                </tr>
                <tr>
                    <td><b>Uang Kembali</b> <small>(sisa uang saku + balen tunai / cash)</small></td>
                    <!-- <td>:</td> -->
                    <td><b><span id="kembalian" class="tx-18"></span></b></td>
                </tr>
                <tr>
                    <td><b>Laba / Rugi</b> <small>(total pendapatan - total pengeluaran - solar pertama)</small></td>
                    <!-- <td>:</td> -->
                    <td><b><span id="perhitungan" class="tx-18"></span></b></td>
                </tr>
            </table>
            <div class="form-group inline mg-t-10">
                <a href="{{site_url('biaya')}}" class="btn btn-warning" onclick="return confirm('Apakah anda yakin ingin kembali?')"><i class="fa fa-arrow-left mr-2"></i>Kembali</a>
                @if($ekspedisi->status_biaya == 0 && $ekspedisi->is_done != 1)
                <button type="button" class="btn btn-primary" onclick="save_draft(1)"><i class="fa fa-save mr-2"></i>Save as Draft</button>    
                <button type="button" class="btn btn-success" onclick="kirim_pengajuan()"><i class="fa fa-send mr-2"></i>Selesai</button>
                @elseif($ekspedisi->status_biaya == 1 && $ekspedisi->is_done != 1)
                <button type="button" class="btn btn-danger" onclick="batal_pengajuan()"><i class="fa fa-times mr-2"></i>Batalkan</button>
                @endif                
            </div>
        </form>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Tambah Balen</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="tab-pane fade show active" id="tab-penjualan" role="tabpanel" aria-labelledby="pills-home-tab">
                        <form action="{{base_url('biaya/submit_tambah_balen')}}" id="form-transaksi" method="post">
                            <div class="form-layout form-layout-4">
                                <input type="hidden" name="ekspedisi_id" value="{{$id}}">
                                <div class="row">
                                    <label class="col-sm-4 form-control-label">Tanggal Balen <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <input type="date" class="form-control" name="tanggal_balen" required="" autocomplete="off"> 
                                    </div>
                                </div>
                                
                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Kota <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <select name="id_area" class="form-control select2-modal" required="">
                                            <option value="">- Pilih Salah Satu -</option>
                                            @foreach ($area as $row)
                                                <option value="{{$row->id}}">{{$row->kota}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Customer <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <select name="customer_id" class="form-control select2-modal" required="">
                                            <option value="">- Pilih Salah Satu -</option>
                                            @foreach ($customer as $row)
                                                <option value="{{$row['id']}}">{{ucwords($row['nama'])}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Jenis Barang <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <input type="text" class="form-control" name="jenis_barang" autocomplete="off" required=""> 
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Keterangan <span class="tx-danger"></span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <input type="text" class="form-control" name="keterangan" autocomplete="off"> 
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Jenis Perhitungan Berat <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <div class="custom-control custom-radio form-check form-check-inline">
                                            <input class="custom-control-input form-check-input" type="radio" name="jenis_perhitungan_berat" id="inlineRadio1" value="rit" required="">
                                            <label class="custom-control-label form-check-label" for="inlineRadio1">Rit</label>
                                        </div>
                                        <div class="custom-control custom-radio form-check form-check-inline">
                                            <input class="custom-control-input form-check-input" type="radio" name="jenis_perhitungan_berat" id="inlineRadio2" value="tonase" required="">
                                            <label class="custom-control-label form-check-label" for="inlineRadio2">Tonase</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label" id="satuan">Berat <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <input type="text" class="form-control autonumeric" name="berat" autocomplete="off" required=""> 
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Harga Satuan <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <input type="text" class="form-control autonumeric" name="harga_satuan" autocomplete="off" required=""> 
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Biaya <span class="tx-danger">*</span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <input type="text" class="form-control autonumeric" name="biaya" autocomplete="off" required=""> 
                                    </div>
                                </div>

                                <div class="row mg-t-10">
                                    <label class="col-sm-4 form-control-label">Metode Pembayaran <span class="tx-danger"></span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        @foreach ($pembayaran as $key => $row)
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="radio-{{$key}}" name="jenis_pembayaran" value="{{$row}}" class="custom-control-input selesai" required="">
                                            <label class="custom-control-label" for="radio-{{$key}}">{{ucwords($row)}}</label>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                               
                                <div class="row mg-t-10 transfer">
                                    <label class="col-sm-4 form-control-label">Rekening Penerima <span class="tx-danger"></span></label>
                                    <div class="col-sm-8 mg-t-10">
                                        <select name="transfer_rekening" class="form-control select2-modal">
                                            <option value="">- Pilih Salah Satu -</option>
                                            @foreach ($rek_penerima as $row)
                                            <option value="{{$row->id}}">{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="row mg-t-20">
                                    <label class="col-sm-4 form-control-label"></label>
                                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@end

@section('style')
<link rel="stylesheet" href="{{base_url('assets/plugins/toggle/toggle.css')}}">
@end

@section('js')
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script>
    // VAR
    var url = "{{site_url('api/internal/ekspedisi')}}";
    var token = "{{$this->session->auth['token']}}";
    // FUNCTIONS

    $(document).ready(function() {
        $('.select2-modal').select2({
            dropdownParent: $('#modal_form')
        });
        $('.autonumeric').autoNumeric('init',{
            'mDec': 0
        });
        cek_balen();
        set_subtotal();
        cek_status();
    });

    $('#tambah').click(function() {
        $('#form-transaksi').trigger('reset');
        save_draft(0);
        cek_metode_pembayaran();
        $('[name=ekspedisi_id]').val({{$ekspedisi->id}});
        $("#modal_form").modal('show');
    });

    $("[name=jenis_perhitungan_berat]").on('change', function() {
        set_satuan();
    });

    $("[name=berat], [name=harga_satuan]").on('keyup change', function() {
        hitung_biaya();
    });

    $("#form-biaya input[type=text]").on('keyup change', function() {
        set_subtotal();
    });

    $('[name=jenis_pembayaran]').change(function(){
        cek_metode_pembayaran();
    });

    $('#table-data').bootstrapTable({
        pagination: true,
        search:true,
        showToggle: true,
        showColumns: true,
        pageSize:10,
        striped: true,
        showFilter: true,
        toolbar: '#toolbar'
    });

    $('[name=has_balen]').change(function(){
        cek_balen();
    });

    function cek_balen(){
        if ($('[name=has_balen]').prop('checked')){
            $('.has-balen').fadeIn();
        }else{
            console.log($(this));
            $('.has-balen').hide();
            reset_balen();
        }
        
    }

    function reset_balen(){
        $('.form-balen').trigger('reset');

    }

    function set_satuan(){
        if($("[name=jenis_perhitungan_berat]:checked").val() == 'rit'){
            $('#satuan').empty().html('Jumlah Rit');
        }else{
            $('#satuan').empty().html('Tonase (KG)');
        }
    }

    function kirim_pengajuan(){
        Swal.fire({
            title: 'Konfirmasi',
            text: "Data akan diproses, lanjutkan?",
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                let data = $('#form-biaya').serializeArray();
                $.ajax({
                    url: "<?= base_url('api/internal/biaya/submit_form')?>",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'token' : "{{$this->session->auth['token']}}",
                        'data': data
                    },
                    success: function(result) {
                        reload_page();
                    },
                    error: function(e) {
                        console.log(e);
                    },
                    complete: function(e) {}
                });
            }
        })
    }

    function batal_pengajuan(){
        Swal.fire({
            title: 'Konfirmasi',
            text: "Apakah anda yakin?",
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "<?= base_url('api/internal/biaya/batal_pengajuan')?>",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'token' : "{{$this->session->auth['token']}}",
                        'id': "{{$id}}"
                    },
                    success: function(result) {
                        reload_page();
                    },
                    error: function(e) {
                        console.log(e);
                    },
                    complete: function(e) {}
                });
            }
        })
    }

    function cek_status(){
        let status_biaya = "{{$ekspedisi->status_biaya}}";
        let is_done = "{{$ekspedisi->is_done}}";
        if(status_biaya == 1 || is_done == 1){
            $('#form-biaya input').prop('disabled', true);
        }
    }

    function save_draft(load){
        let data = $('#form-biaya').serializeArray();
        $.ajax({
            url: "<?= base_url('api/internal/biaya/save_draft')?>",
            type: "POST",
            dataType: "json",
            data: {
                'token' : "{{$this->session->auth['token']}}",
                'data': data
            },
            success: function(result) {
                if(load==1){
                    reload_page();
                }
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {}
        });
    }

    function hitung_biaya(){
        let berat = $('[name=berat]').autoNumeric('get');
        let harga = $('[name=harga_satuan').autoNumeric('get');
        let biaya = getNum(parseInt(berat)) * getNum(parseInt(harga));
        $('[name=biaya]').val(biaya);
        $('.autonumeric').autoNumeric('destroy');
        $('.autonumeric').autoNumeric('init', {
            'mDec': 0
        });
    }

    function set_subtotal(){
        let sby = "{{$ekspedisi->is_sby}}";
        let solar = ($('[name=solar]').autoNumeric('get'));
        let timbangan = ($('[name=timbangan]').autoNumeric('get'));
        let upah = ($('[name=upah]').autoNumeric('get'));
        let makan = ($('[name=makan]').autoNumeric('get'));
        let kuli = ($('[name=kuli]').autoNumeric('get'));
        let lain = ($('[name=lain]').autoNumeric('get'));
        let tol = 0;
        let polisi = 0;
        let parkir = 0;
        let masuk_ruko = 0;
        let pengurus_pelabuhan = 0;
        let pak_serap = 0;
        let total_biaya = 0;
        if(sby == 1){
            tol = ($('[name=tol]').autoNumeric('get'));
            polisi = ($('[name=polisi]').autoNumeric('get'));
            parkir = ($('[name=parkir]').autoNumeric('get'));
            masuk_ruko = ($('[name=masuk_ruko]').autoNumeric('get'));
            pengurus_pelabuhan = ($('[name=pengurus_pelabuhan]').autoNumeric('get'));
            pak_serap = ($('[name=pak_serap]').autoNumeric('get'));
        }
        
        total_biaya = getNum(parseInt(solar)) + getNum(parseInt(timbangan)) + getNum(parseInt(upah)) + getNum(parseInt(makan)) + getNum(parseInt(kuli)) + getNum(parseInt(lain)) + getNum(parseInt(tol)) + getNum(parseInt(polisi)) + getNum(parseInt(parkir)) + getNum(parseInt(masuk_ruko)) + getNum(parseInt(pengurus_pelabuhan)) + getNum(parseInt(pak_serap));
        $('#total_biaya').html(total_biaya.toLocaleString().replace(/,/g, '.'));
        set_labarugi(total_biaya);
    }

    function set_labarugi(total_biaya){
        let uang_saku = {{$ekspedisi->uang_saku}};
        let ongkos = {{$ekspedisi->biaya}};
        let solar_pertama = {{$ekspedisi->solar_pertama}};
        let balen = {{$total_balen}};
        let balen_cash = {{$total_kembalian_balen}};
        let perhitungan = ongkos + balen - solar_pertama - total_biaya;
        let kembalian = uang_saku + balen_cash - total_biaya;
        let sisa_uangsaku = uang_saku - total_biaya;
        $('#perhitungan').html(perhitungan.toLocaleString().replace(/,/g, '.'));
        $('#kembalian').html(kembalian.toLocaleString().replace(/,/g, '.'));
        $('#sisa_uangsaku').html(sisa_uangsaku.toLocaleString().replace(/,/g, '.'));
    }

    function hapus_data(e){
        Swal.fire({
            title: 'Konfirmasi',
            text: "Apakah anda yakin ingin menghapus data ini?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "<?= base_url('api/internal/biaya/delete_balen')?>",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'token' : "{{$this->session->auth['token']}}",
                        'id': $(e).data().id
                    },
                    success: function(result) {
                        reload_page();
                    },
                    error: function(e) {
                        console.log(e);
                    },
                    complete: function(e) {}
                });
            }
        })
    }

    function getNum(val) {
        if (isNaN(val)) {
            return 0;
        }
        return val;
    }

    function cek_metode_pembayaran(){
        let jenis_pembayaran = $('[name=jenis_pembayaran]:checked').val();
        if(jenis_pembayaran == 'transfer'){
            $('.transfer').fadeIn();
        }else{
            $('.transfer').hide();
        }
    }

</script>
@end