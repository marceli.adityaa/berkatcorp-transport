@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active">Ekspedisi</li>
    </ol>
</nav>
@end

@section('content')
<!-- batas -->
<!-- <div class="row mb-4">
    <div class="col-12 col-lg-4">
        <div class="card card-widget">
            <div class="card-body">
                <span class="tx-12 tx-uppercase">Ekspedisi Bulan Ini</span><br>
                <h3 class="mt-2 tx-bold">{{$widget['total']}} Ekspedisi</h3>
                <i class="fa fa-truck widget-icon"></i>
            </div>
        </div>
    </div>
    <div class="col-12 col-lg-4">
        <div class="card card-widget">
            <div class="card-body">
                <span class="tx-12 tx-uppercase">Ekspedisi Aktif</span><br>
                <h3 class="mt-2 tx-bold">{{$widget['aktif']}} Ekspedisi</h3>
                <i class="fa fa-truck widget-icon"></i>
            </div>
        </div>
    </div>
    <div class="col-12 col-lg-4">
        <div class="card card-widget">
            <div class="card-body">
                <span class="tx-12 tx-uppercase">Pendapatan Bulan Ini</span><br>
                <h3 class="mt-2 tx-bold">Rp {{monefy($widget['current'], false)}}</h3>
                <i class="fa fa-dollar widget-icon"></i>
            </div>
        </div>
    </div>
</div> -->
<!-- batas -->
<!-- <div id="accordion2" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion2" href="#collapse2" aria-expanded="true" aria-controls="collapse2" class="tx-gray-800 transition danger">
                    Ekspedisi Aktif
                </a>
            </h6>
        <div id="collapse2" class="collapse show" role="tabpanel" aria-labelledby="heading2">
            <table class="table mb-0" data-search="true">
                <thead>
                    <tr>
                        <th>NO. RESI</th>
                        <th>ENTRI</th>
                        <th>CEK KELUAR</th>
                        <th>CEK MASUK</th>
                        <th>ENTRI BIAYA</th>
                        <th>VALIDASI BIAYA</th>
                        <th>VERIFIKASI</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($eks_aktif['rows'] AS $eks) <tr>
                        <td><b>{{$eks->no_resi}}</b><br><small>{{$eks->no_pol}} / {{$eks->pegawai_nama}}</small></td>
                        <td>{{!empty($eks->tanggal_entri)?'<span class="badge badge-primary"><i class="fa fa-check mr-2"></i>'.datify($eks->tanggal_entri,'m/d/y - H:i').'</span>':'<span class="badge badge-danger"><i class="fa fa-history mr-2"></i>menunggu</span>'}}</td>
                        <td>{{!empty($eks->tanggal_cek_berangkat)?'<span class="badge badge-primary"><i class="fa fa-check mr-2"></i>'.datify($eks->tanggal_cek_berangkat,'m/d/y - H:i').'</span>':'<span class="badge badge-danger"><i class="fa fa-history mr-2"></i>menunggu</span>'}}</td>
                        <td>{{!empty($eks->tanggal_cek_pulang)?'<span class="badge badge-primary"><i class="fa fa-check mr-2"></i>'.datify($eks->tanggal_cek_pulang,'m/d/y - H:i').'</span>':'<span class="badge badge-danger"><i class="fa fa-history mr-2"></i>menunggu</span>'}}</td>
                        <td>{{!empty($eks->tanggal_input_biaya)?'<span class="badge badge-primary"><i class="fa fa-check mr-2"></i>'.datify($eks->tanggal_input_biaya,'m/d/y - H:i').'</span>':'<span class="badge badge-danger"><i class="fa fa-history mr-2"></i>menunggu</span>'}}</td>
                        <td>{{!empty($eks->tanggal_validasi_biaya)?'<span class="badge badge-primary"><i class="fa fa-check mr-2"></i>'.datify($eks->tanggal_validasi_biaya,'m/d/y - H:i').'</span>':'<span class="badge badge-danger"><i class="fa fa-history mr-2"></i>menunggu</span>'}}</td>
                        <td>
                            @if($eks->tanggal_validasi_biaya == null)
                            <button class="btn btn-block btn-sm btn-secondary"><i class="fa fa-history mr-2"></i>berlangsung</button>
                            @else
                            <a href="{{site_url('ekspedisi/detail/'.$eks->id)}}" class="btn btn-block btn-sm btn-primary"><i class="fa fa-check mr-2"></i>verifikasi</a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div> -->
<!-- batas -->
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('ekspedisi/index?')?>" id="form-filter">
                    <?php $get = $this->input->get()?>
                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>

                        <label class="col-sm-3 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Kendaraan</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="kendaraan" class="form-control select2">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$kendaraan as $row){
                                        if(isset($get['kendaraan']) && $get['kendaraan'] == $row['id']){
                                            echo '<option value="'.$row['id'].'" selected>'.strtoupper($row['no_pol']).'</option>';
                                        }else{
                                            echo '<option value="'.$row['id'].'">'.strtoupper($row['no_pol']).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>

                        <label class="col-sm-3 form-control-label">Supir</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="supir" class="form-control select2">
                                <option value="all">All</option>
                                <?php
                                    foreach($supir as $row){
                                        if(isset($get['supir']) && $get['supir'] == $row->id){
                                            echo '<option value="'.$row->id.'" selected>'.ucwords($row->nama).'</option>';
                                        }else{
                                            echo '<option value="'.$row->id.'">'.ucwords($row->nama).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Customer</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="customer" class="form-control select2">
                                <option value="all">All</option>
                                <?php
                                    foreach($customer as $row){
                                        if(isset($get['customer']) && $get['customer'] == $row['id']){
                                            echo '<option value="'.$row['id'].'" selected>'.ucwords($row['nama']).'</option>';
                                        }else{
                                            echo '<option value="'.$row['id'].'">'.ucwords($row['nama']).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                        <label class="col-sm-3 form-control-label">Status</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control select2">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$status as $key => $row){
                                        if(!empty($get['status']) && $get['status'] == $key){
                                            echo '<option value="'.$key.'" selected>'.ucwords($row).'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.ucwords($row).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label"></label>
                        <div class="col-sm-9 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                            <!-- <button type="button" class="btn btn-success" onclick="export_data(this)"><i class="fa fa-file-excel-o mg-r-10"></i>Export (dalam perbaikan)</button> -->
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card mt-4">
    <div class="card-header card-header-default">DATA EKSPEDISI</div>
    <div class="card-body">
        <div id="toolbar">
            <a href="{{site_url('ekspedisi/tambah')}}" class="btn btn-primary"><i class="fa fa-plus mr-2"></i>Ekspedisi Baru</a>
        </div>
        <!-- <table class="table table-js table-sm" id="tbData" data-search="true" data-toolbar="#toolbar" data-search-on-enter-key="true" data-pagination="true" data-side-pagination="server" data-show-refresh="true" data-url="{{site_url('api/internal/ekspedisi/get_many/'.$this->session->auth['token'].'?callback=callbackTable&sort=no_resi&order=desc')}}">
            <thead>
                <tr>
                    <th class="text-left" data-field="aksi">AKSI</th>
                    <th class="text-center" data-field="status">STATUS</th>
                    <th class="text-center" data-field="no_resi" data-sortable="true">NO RESI</th>
                    <th data-field="pegawai_nama">SOPIR</th>
                    <th class="text-center" data-field="no_pol" data-sortable="true">KENDARAAN</th>
                    <th data-field="sender_nama">PENGIRIM</th>
                    <th data-field="reciver_nama">PENERIMA</th>
                    <th class="text-center" data-field="tanggal_jalan" data-formatter="formatDate" data-sortable="true">TGL JALAN</th>
                    <th class="text-center" data-field="tujuan" data-sortable="true">TUJUAN</th>
                    <th class="text-center" data-field="jenis_barang">JENIS BARANG</th>
                    <th class="text-center" data-field="berat">BERAT MUATAN</th>
                    <th class="text-center" data-field="biaya">BIAYA</th>
                </tr>
            </thead>
        </table> -->

        <table class="table table-white table-bordered" id="table-data">
            <thead>
                <tr>
                    <th class="text-left nowrap">AKSI</th>
                    <th class="text-center">STATUS</th>
                    <th class="text-center" data-sortable="true">NO RESI</th>
                    <th class="text-center" data-sortable="true">TGL JALAN</th>
                    <th class="text-center" data-sortable="true">TGL KEMBALI</th>
                    <th class="text-center" data-sortable="true">CUSTOMER</th>
                    <th class="text-center" data-sortable="true">KENDARAAN</th>
                    <th class="text-left" data-sortable="true">SUPIR</th>
                    <th class="text-center" data-sortable="true">TUJUAN</th>
                    <th class="text-left" data-sortable="true">JENIS BARANG</th>
                    <th class="text-center">BERAT MUATAN</th>
                    <th class="text-center">UANG SAKU</th>
                    <th class="text-center">SOLAR PERTAMA</th>
                    <th class="text-right">ONGKOS BERANGKAT</th>
                </tr>
            </thead>
            <tbody>
                @if (!empty($ekspedisi))
                    @foreach ($ekspedisi as $row)
                        <tr>
                            <td>
                                @if($row['status_berangkat'] != 1) 
                                    <a href="{{site_url('ekspedisi/sunting/'.$row['id'])}}" class='btn btn-warning btn-sm' title='Edit data'><i class='icon-pencil'></i></a>
                                    <button class='btn btn-danger btn-sm' title='Hapus transaksi' onclick="hapus({{$row['id']}})"><i class='icon-trash'></i></button>
                                @endif
                            </td>
                            <td>
                                @if($row['status_berangkat'] == 0)
                                <span class="badge badge-secondary">menunggu</span>
                                @elseif ($row['status_berangkat'] == 2)
                                <span class="badge badge-danger">dibatalkan</span>
                                @elseif ($row['status_berangkat'] == 1 && $row['is_done'] == 0)
                                <span class="badge badge-warning">berlangsung</span>
                                @elseif ($row['status_berangkat'] == 1 && $row['is_done'] == 1)
                                <span class="badge badge-success">selesai</span>
                                @endif
                            </td>
                            <td>
                                <label class="badge badge-light">{{$row['no_resi']}}</label>
                            </td>
                            <td>
                                <label class="badge badge-light">{{$row['tanggal_jalan']}}</label>
                            </td>
                            <td>
                                @if($row['tanggal_kembali'] == '0000-00-00')
                                -
                                @else 
                                    <label class="badge badge-light">{{$row['tanggal_kembali']}}</label>
                                @endif
                            </td>
                            <td>
                                <label class="badge badge-light">{{ucwords($row['customer'])}}</label>
                            </td>
                            <td>
                                <label class="badge badge-light">{{$row['no_pol']}}</label>
                            </td>
                            <td>
                                <label class="badge badge-light">{{ucwords($row['supir'])}}</label>
                            </td>
                            <td>
                                @if($row['is_sby'])
                                    <label class="badge badge-warning">{{ucwords($row['kota'])}}</label>
                                @else 
                                    <label class="badge badge-light">{{ucwords($row['kota'])}}</label>
                                @endif
                            </td>
                            <td>
                                <label class="badge badge-light">{{ucwords($row['jenis_barang'])}}</label>
                            </td>
                            <td>
                                <label class="badge badge-light">{{($row['berat_muatan'])}}</label>
                            </td>
                            <td>
                                {{monefy($row['uang_saku'], false)}}<br>
                                @if($row['id_kas'] != 0)
                                    <label class="badge badge-light">{{($row['kas_label'])}}</label>
                                @endif
                            </td>
                            <td>
                                {{monefy($row['solar_pertama'], false)}}
                            </td>
                            <td>
                                {{monefy($row['biaya'], false)}}
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>
@end

@section('js')
<script>
    // VAR
    var url = "{{site_url('api/internal/ekspedisi')}}";
    var token = "{{$this->session->auth['token']}}";
    // FUNCTIONS

    $('#table-data').bootstrapTable({
        pagination: true,
        search:true,
        showToggle: true,
        showColumns: true,
        pageSize:10,
        striped: true,
        showFilter: true,
        toolbar: '#toolbar'
    });

    function hapus(id) {
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Data ekspedisi yang terkait akan dihapus, lanjutkan?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url + '/hapus/' + token,
                    method: 'POST',
                    data: {
                        id: id
                    },
                    dataType: 'json',
                    success: function(result, status, xhr) {
                        if (result.status === 'success') {
                            Toast.fire('Sukses!', result.message, 'success');
                            reload_page();
                        } else {
                            Swal.fire('Error!', result.message, 'error');
                        }
                    },
                    error: function(xhr, status, error) {
                        Toast.fire('Error!', 'Terjadi kesalahan pada sistem!', 'error');
                    }
                });
            }
        });
    }

    function export_data(el){
        let start = $("#form-filter [name=start]").val();
        let end = $("#form-filter [name=end]").val();
        let kendaraan = $("#form-filter [name=kendaraan]").val();
        let supir = $("#form-filter [name=supir]").val();
        let status = $("#form-filter [name=status]").val();
        var url = "<?= base_url('ekspedisi/export_rekap?')?>"+'start='+start+'&end='+end+'&kendaraan='+kendaraan+'&supir='+supir+'&status='+status;
        window.open(url, '_blank');
    }
</script>
@end