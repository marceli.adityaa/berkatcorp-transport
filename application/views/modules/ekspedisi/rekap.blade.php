@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active">Ekspedisi</li>
        <li class="breadcrumb-item active">Rekap</li>
    </ol>
</nav>
@end

@section('content')
<!-- batas -->
<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('ekspedisi/rekap?')?>" id="form-filter">
                    <?php $get = $this->input->get()?>
                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>

                        <label class="col-sm-3 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Kendaraan</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="kendaraan" class="form-control select2">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$kendaraan as $row){
                                        if(isset($get['kendaraan']) && $get['kendaraan'] == $row['id']){
                                            echo '<option value="'.$row['id'].'" selected>'.strtoupper($row['no_pol']).'</option>';
                                        }else{
                                            echo '<option value="'.$row['id'].'">'.strtoupper($row['no_pol']).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>

                        <label class="col-sm-3 form-control-label">Supir</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="supir" class="form-control select2">
                                <option value="all">All</option>
                                <?php
                                    foreach($supir as $row){
                                        if(isset($get['supir']) && $get['supir'] == $row->id){
                                            echo '<option value="'.$row->id.'" selected>'.ucwords($row->nama).'</option>';
                                        }else{
                                            echo '<option value="'.$row->id.'">'.ucwords($row->nama).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label">Customer</label>
                        <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                            <select name="customer" class="form-control select2">
                                <option value="all">All</option>
                                <?php
                                    foreach($customer as $row){
                                        if(isset($get['customer']) && $get['customer'] == $row['id']){
                                            echo '<option value="'.$row['id'].'" selected>'.ucwords($row['nama']).'</option>';
                                        }else{
                                            echo '<option value="'.$row['id'].'">'.ucwords($row['nama']).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                       
                    </div>
                    
                    <div class="row mg-t-10">
                        <label class="col-sm-3 form-control-label"></label>
                        <div class="col-sm-9 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                            <button type="button" class="btn btn-success" onclick="export_data(this)"><i class="fa fa-file-excel-o mg-r-10"></i>Export Data</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card mt-4">
    <div class="card-header card-header-default">DATA EKSPEDISI</div>
    <div class="card-body">
        <div id="toolbar">
        </div>
        <table class="table table-white table-bordered" id="table-data">
            <thead>
                <tr>
                    <th class="text-left nowrap">AKSI</th>
                    <th class="text-center" data-sortable="true">NO RESI</th>
                    <th class="text-center" data-sortable="true">TGL JALAN</th>
                    <th class="text-center" data-sortable="true">TGL KEMBALI</th>
                    <th class="text-center" data-sortable="true">CUSTOMER</th>
                    <th class="text-center" data-sortable="true">KENDARAAN</th>
                    <th class="text-left" data-sortable="true">SUPIR</th>
                    <th class="text-center" data-sortable="true">TUJUAN</th>
                    <th class="text-center">BERAT MUATAN</th>
                    <th class="text-center">SOLAR PERTAMA</th>
                    <th class="text-center">UANG SAKU</th>
                    <th class="text-right">ONGKOS BERANGKAT</th>
                    <th class="text-right">BIAYA EKSPEDISI</th>
                    <th class="text-right">PENDAPATAN BALEN</th>
                    <th class="text-right">LABA / RUGI</th>
                    <th class="text-right">SISA UANG SAKU</th>
                    <th class="text-right">UANG KEMBALI</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $subtotal = 0;
                ?>
                @if (!empty($ekspedisi))
                    @foreach ($ekspedisi as $row)
                        <tr>
                            <td>
                            <a href="{{site_url('ekspedisi/detail_rekap/'.$row['id'])}}" class='btn btn-info btn-sm' title='Detail Transaksi' target="_blank"><i class='icon-list'></i></a>
                            <button type="button" class='btn btn-primary btn-sm' title='Kelengkapan' onclick="detail({{$row['id']}})"><i class='icon-social-dropbox'></i></button>
                            </td>

                            <td>
                                <label class="badge badge-light">{{$row['no_resi']}}</label>
                            </td>
                            <td>
                                <label class="badge badge-light">{{$row['tanggal_jalan']}}</label>
                            </td>
                            <td>
                                @if($row['tanggal_kembali'] == '0000-00-00')
                                -
                                @else 
                                    <label class="badge badge-light">{{$row['tanggal_kembali']}}</label>
                                @endif
                            </td>
                            <td>
                                <label class="badge badge-light">{{ucwords($row['customer'])}}</label>
                            </td>
                            <td>
                                <label class="badge badge-light">{{$row['no_pol']}}</label>
                            </td>
                            <td>
                                <label class="badge badge-light">{{ucwords($row['supir'])}}</label>
                            </td>
                            <td>
                                @if($row['is_sby'])
                                    <label class="badge badge-warning">{{ucwords($row['kota'])}}</label>
                                @else 
                                    <label class="badge badge-light">{{ucwords($row['kota'])}}</label>
                                @endif
                            </td>
                            
                            <td>
                                <label class="badge badge-light">{{($row['berat_muatan'])}}</label>
                            </td>
                            <td>
                                <label class="badge badge-light">{{($row['solar_pertama'])}}</label>
                            </td>
                            <td>
                                {{monefy($row['uang_saku'], false)}}<br>
                                
                            </td>
                            <td>
                                {{monefy($row['biaya'], false)}}
                            </td>
                            <td>
                                {{monefy($row['total_biaya'], false)}}
                            </td>
                            <td>
                                @if(!empty($row['total_balen']))
                                    {{monefy($row['total_balen'], false)}}
                                @else 
                                -
                                @endif
                            </td>
                            <?php 
                                $laba = ($row['biaya'] + $row['total_balen'] - $row['solar_pertama'] - $row['total_biaya']);
                                $kembalian = ($row['uang_saku'] + $row['total_balen_cash'] - $row['total_biaya']);
                                $sisa_uangsaku = ($row['uang_saku'] - $row['total_biaya']);
                            ?>
                            <td>
                                @if($laba < 0)
                                <span class="tx-danger">{{monefy($laba, false)}}</span>
                                @else 
                                <span class="tx-success">+{{monefy($laba, false)}}</span>
                                @endif
                                <?php $subtotal += $laba?>
                            </td>
                            <td>
                                @if($sisa_uangsaku < 0)
                                <span class="tx-danger">{{monefy($sisa_uangsaku, false)}}</span>
                                @else 
                                {{monefy($sisa_uangsaku, false)}}
                                @endif
                            </td>
                            <td>
                                @if($kembalian < 0)
                                <span class="tx-danger">{{monefy($kembalian, false)}}</span>
                                @else 
                                {{monefy($kembalian, false)}}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        <table class="table table-white" style="font-weight:800;font-size:18px">
            <tr>
                <td class="text-right">Total L/R : </td>
                <td class="text-left"><?= monefy($subtotal, false)?></td>
            </tr>

        </table>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="mDetail" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">DETAIL KELENGKAPAN</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body px-0 py-0">
                <table class="table table-bordered table-striped mb-0"></table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-block btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script>
    // VAR
    var url = "{{site_url('api/internal/ekspedisi')}}";
    var token = "{{$this->session->auth['token']}}";
    // FUNCTIONS

    $('#table-data').bootstrapTable({
        pagination: true,
        search:true,
        showToggle: true,
        showColumns: true,
        pageSize:10,
        striped: true,
        showFilter: true,
        toolbar: '#toolbar'
    });

    function hapus(id) {
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Data ekspedisi yang terkait akan dihapus, lanjutkan?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url + '/hapus/' + token,
                    method: 'POST',
                    data: {
                        id: id
                    },
                    dataType: 'json',
                    success: function(result, status, xhr) {
                        if (result.status === 'success') {
                            Toast.fire('Sukses!', result.message, 'success');
                            reload_page();
                        } else {
                            Swal.fire('Error!', result.message, 'error');
                        }
                    },
                    error: function(xhr, status, error) {
                        Toast.fire('Error!', 'Terjadi kesalahan pada sistem!', 'error');
                    }
                });
            }
        });
    }

    function export_data(el){
        let start = $("#form-filter [name=start]").val();
        let end = $("#form-filter [name=end]").val();
        let kendaraan = $("#form-filter [name=kendaraan]").val();
        let supir = $("#form-filter [name=supir]").val();
        var url = "<?= base_url('ekspedisi/export_rekap?')?>"+'start='+start+'&end='+end+'&kendaraan='+kendaraan+'&supir='+supir;
        window.open(url, '_blank');
    }

    function detail(id) {
        $.ajax({
            url: "{{site_url('api/internal/kelengkapan')}}"+'/get_detail/' + token,
            type: "POST",
            dataType: "json",
            data: {
                'id' : id,
            },
            success: function(result) {
                var html = "<tr class='tx-bold'><td>CHECKLIST</td><td class='text-center'>CEK AWAL</td><td class='text-center'>CEK AKHIR</td></tr>";
                
                html += "<tr><td>KM</td><td class='text-center'>" + result.km_awal + "</td><td class='text-center'>" + result.km_akhir + "</td></tr>";
                html += "<tr><td>Pemadam</td><td class='text-center'>" + setCheck(result.pemadam_awal) + "</td><td class='text-center'>" + setCheck(result.pemadam_akhir) + "</td></tr>";

                html += "<tr><td>Dongkrak</td><td class='text-center'>" + setCheck(result.dongkrak_awal) + "</td><td class='text-center'>" + setCheck(result.dongkrak_akhir) + "</td></tr>";
                html += "<tr><td>Kunci Roda</td><td class='text-center'>" + setCheck(result.kunci_roda_awal) + "</td><td class='text-center'>" + setCheck(result.kunci_roda_akhir) + "</td></tr>";

                html += "<tr><td>Stang Pendek</td><td class='text-center'>" + setCheck(result.stang_pendek_awal) + "</td><td class='text-center'>" + setCheck(result.stang_pendek_akhir) + "</td></tr>";
                html += "<tr><td>Stang Panjang</td><td class='text-center'>" + setCheck(result.stang_panjang_awal) + "</td><td class='text-center'>" + setCheck(result.stang_panjang_akhir) + "</td></tr>";

                html += "<tr><td>Terpal</td><td class='text-center'>" + setCheck(result.terpal_awal) + "</td><td class='text-center'>" + setCheck(result.terpal_akhir) + "</td></tr>";
                html += "<tr><td>Tampar</td><td class='text-center'>" + setCheck(result.tampar_awal) + "</td><td class='text-center'>" + setCheck(result.tampar_akhir) + "</td></tr>";

                html += "<tr><td>P3K</td><td class='text-center'>" + setCheck(result.p3k_awal) + "</td><td class='text-center'>" + setCheck(result.p3k_akhir) + "</td></tr>";
                html += "<tr><td>Ban Serep</td><td class='text-center'>" + setCheck(result.ban_serep_awal) + "</td><td class='text-center'>" + setCheck(result.ban_serep_akhir) + "</td></tr>";

                html += "<tr><td>Buku Kir</td><td class='text-center'>" + setCheck(result.buku_kir_awal) + "</td><td class='text-center'>" + setCheck(result.buku_kir_akhir) + "</td></tr>";
                html += "<tr><td>STNK</td><td class='text-center'>" + setCheck(result.stnk_awal) + "</td><td class='text-center'>" + setCheck(result.stnk_akhir) + "</td></tr>";

                html += "<tr><td>Surat Jalan</td><td class='text-center'>" + setCheck(result.surat_jalan_awal) + "</td><td class='text-center'>" + setCheck(result.surat_jalan_akhir) + "</td></tr>";
                html += "<tr><td>Cek Fisik</td><td class='text-center'>" + setCheck(result.cek_fisik_awal) + "</td><td class='text-center'>" + setCheck(result.cek_fisik_akhir) + "</td></tr>";
                
                $("#mDetail table").empty().html(html);
                $("#mDetail").modal('show');
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {
            }
        });
        
    }

    function setCheck(status) {
        return (status == 1) ? "<i class='fa fa-check text-primary'></i>" : "<i class='fa fa-times text-danger'></i>";
    }
</script>
@end