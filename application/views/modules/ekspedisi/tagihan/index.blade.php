@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{site_url('ekspedisi')}}">Ekspedisi</a></li>
        <li class="breadcrumb-item active">Tagihan</li>
    </ol>
</nav>
@end

@section('content')

<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('tagihan/index?')?>" id="form-filter">
                    <?php $get = $this->input->get()?>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Status</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control select2">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$status as $key => $row){
                                        if(!empty($get['status']) && $get['status'] == $key){
                                            echo '<option value="'.$key.'" selected>'.ucwords($row).'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.ucwords($row).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card mt-4">
    <div class="card-header card-header-default">DATA TAGIHAN</div>
    <div class="card-body">
        <div id="toolbar">
        </div>
        <table class="table table-white table-bordered" id="table-data">
            <thead>
                <tr>
                    <th class="text-left">AKSI</th>
                    <th class="text-center" data-sortable="true">STATUS</th>
                    <th class="text-center" data-sortable="true">TANGGAL</th>
                    <th class="text-center" data-sortable="true">NO RESI</th>
                    <th class="text-center" data-sortable="true">CUSTOMER</th>
                    <th class="text-center" data-sortable="true">KENDARAAN</th>
                    <th class="text-center" data-sortable="true">SOPIR</th>
                    <th class="text-center" data-sortable="true">AREA</th>
                    <th class="text-center" data-sortable="true">SUMBER</th>
                    <th class="text-left" data-sortable="true">PEMBAYARAN</th>
                    <th class="text-center" data-sortable="true">KAS</th>
                    <th class="text-right" data-sortable="true">SUBTOTAL</th>
                </tr>
            </thead>
            <tbody>
            <?php 
                $waiting = 0;
                $lunas = 0;
            ?>
                @if (!empty($tagihan))
                    @foreach ($tagihan as $row)
                        <tr>
                            <td class="nowrap">
                                <a href="{{site_url('tagihan/detail/'.$row['id_ekspedisi'])}}" class='btn btn-info btn-sm' title='Detail Transaksi' target="_blank"><i class='icon-list'></i></a>
                                @if($row['is_lunas'] != 1) 
                                    <button class='btn btn-success btn-sm' title='Setujui ekspedisi' onclick="setuju({{$row['id']}})"><i class='fa fa-check'></i></button>
                                    @if($row['is_lunas'] != 2)
                                    <button class='btn btn-danger btn-sm' title='Batalkan ekspedisi' onclick="tolak({{$row['id']}})"><i class='fa fa-times'></i></button>
                                    @endif
                                @endif
                            </td>
                            <td>
                                @if($row['is_lunas'] == 0)
                                <span class="badge badge-secondary">menunggu</span>
                                @elseif ($row['is_lunas'] == 1)
                                <span class="badge badge-success">lunas</span>
                                @else
                                <span class="badge badge-danger">ditolak</span>
                                @endif
                            </td>
                            <td>
                                <label class="badge badge-light">{{$row['tanggal']}}</label>
                            </td>
                            <td>
                                <label class="badge badge-light">{{$row['no_resi']}}</label>
                            </td>
                            <td>
                                <label class="badge badge-light">{{$row['customer']}}</label>
                            </td>
                            <td>
                                <label class="badge badge-light">{{$row['no_pol']}}</label>
                            </td>
                            <td>
                                <label class="badge badge-light">{{ucwords($row['supir'])}}</label>
                            </td>
                            <td>
                                <label class="badge badge-light">{{ucwords($row['kota'])}}</label>
                            </td>
                            <td>
                                @if($row['sumber'] == 'ekspedisi')
                                <label class="badge badge-info">{{($row['sumber'])}}</label>
                                @else
                                <label class="badge badge-warning">{{($row['sumber'])}}</label>
                                @endif
                            </td>
                            <td>
                                @if($row['pembayaran'] == 'cash')
                                <label class="badge badge-info">{{($row['pembayaran'])}}</label>
                                @elseif($row['pembayaran'] == 'transfer')
                                <label class="badge badge-warning">{{($row['pembayaran'])}}</label>
                                @else
                                -
                                @endif
                            </td>
                            <td>
                                @if(!empty($row['id_kas']))
                                <label class="badge badge-light">{{($row['kas_label'])}}</label>
                                @else 
                                <label class="badge badge-light">-</label>
                                @endif
                            </td>
                            <td>
                                <label class="badge badge-light">{{monefy($row['subtotal'], false)}}</label>
                            </td>
                            <?php 
                            if($row['is_lunas'] == 1){
                                $lunas += $row['subtotal'];
                            }elseif($row['is_lunas'] == 0){
                                $waiting += $row['subtotal'];
                            }?>

                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        <table class="table table-white">
            <tr class="tx-semibold tx-16">
                <td>Belum lunas</td>
                <td class="text-center"><?= monefy($waiting,false)?></td>
            </tr>
            <tr class="tx-semibold tx-16">
                <td>Lunas</td>
                <td class="text-center"><?= monefy($lunas,false)?></td>
            </tr>
            <tr class="tx-bold tx-18">
                <td>Total Pendapatan</td>
                <td class="text-center"><?= monefy($lunas+$waiting,false)?></td>
            </tr>
        </table>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <form method="post" action="<?= base_url('api/internal/tagihan/setuju_pengajuan')?>" id="form-ekspedisi">
                <div class="modal-header bg-midnightblack">
                    <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Persetujuan Ekspedisi | <label id="kode" class="tx-white"></label></h6>
                    <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="">
                    <input type="hidden" name="id_ekspedisi" value="">
                    <input type="hidden" name="url" value="{{$_SERVER['QUERY_STRING']}}">
                    <div class="form-layout form-layout-4">
      

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Tanggal</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="tanggal" class="form-control field-disabled" value="" required="">
                            </div>
                        </div>

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Customer</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="customer" class="form-control field-disabled" value="" required="">
                            </div>
                        </div>

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Area</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="id_area" class="form-control select2 field-disabled" required="">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($area as $row)
                                        <option value="{{$row->id}}">{{$row->kota}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Customer</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="no_pol" class="form-control field-disabled" value="" required="">
                            </div>
                        </div>

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Supir</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="pegawai_id" class="form-control select2-modal field-disabled" required="">
                                    <option value="">- Pilih Salah Satu</option>
                                    @foreach($supir as $row)
                                        <option value="{{$row->id}}">{{ucwords($row->nama)}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>     

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Sumber</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="sumber" class="form-control field-disabled" value="" required="">
                            </div>
                        </div>

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Metode Pembayaran</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                @foreach ($pembayaran as $key => $row)
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="radio-{{$key}}" name="jenis_pembayaran" value="{{$row}}" class="custom-control-input" required>

                                    @if($row == 'tbd')
                                        <label class="custom-control-label" for="radio-{{$key}}">{{ucwords('Belum Tahu')}}</label>
                                    @else
                                        <label class="custom-control-label" for="radio-{{$key}}">{{ucwords($row)}}</label>
                                    @endif
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="row col-12 my-3 cash">
                            <label class="col-sm-4 form-control-label">Sumber Kas</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="id_kas" class="form-control select2">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($kas as $row)
                                        <option value="{{$row->id}}">{{$row->label}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row col-12 my-3 transfer">
                            <label class="col-sm-4 form-control-label">Rekening Penerima</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="id_rek_penerima" class="form-control select2">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($rek_penerima as $row)
                                        <option value="{{$row->id}}">{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Tanggal Bayar</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="date" name="tgl_bayar" class="form-control" value="" required="">
                            </div>
                        </div>

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Subtotal</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="subtotal" class="form-control autonumeric" value="" autocomplete="off" placeholder="Masukkan jumlah subtotal">
                            </div>
                        </div>

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Jumlah Bayar</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="jumlah_bayar" class="form-control autonumeric" value="" autocomplete="off" placeholder="Masukkan jumlah bayar" required="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save mr-2"></i>Setujui</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
@end

@section('js')
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script>
    // VAR
    var url = "{{site_url('api/internal/tagihan')}}";
    var token = "{{$this->session->auth['token']}}";
    // FUNCTIONS

    $(document).ready(function() {
        $('.select2-modal').select2({
            dropdownParent: $('#modal_form')
        });
        cek_metode_pembayaran()
    });

    $('[name=jenis_pembayaran]').change(function(){
        cek_metode_pembayaran();
    });

    $('#table-data').bootstrapTable({
        pagination: true,
        search:true,
        showToggle: true,
        showColumns: true,
        pageSize:10,
        striped: true,
        showFilter: true,
        toolbar: '#toolbar'
    });

    function cek_metode_pembayaran(){
        let jenis_pembayaran = $('[name=jenis_pembayaran]:checked').val();
        if(jenis_pembayaran == 'cash'){
            $('.cash').fadeIn();
            $('.transfer').hide();
        }else if(jenis_pembayaran == 'transfer'){
            $('.cash').hide();
            $('.transfer').fadeIn();
        }else{
            $('.cash').hide();
            $('.transfer').hide();
        }
    }

    $('[name=jenis_pembayaran]').change(function(){
        cek_metode_pembayaran();
    });

    function setuju(id) {
        $.ajax({
            url: url + '/get_detail_validasi/' + token,
            type: "POST",
            dataType: "json",
            data: {
                'id' : id,
            },
            success: function(result) {
                $('#form-ekspedisi').trigger('reset');
                $('#form-ekspedisi .field-disabled').attr('disabled', 'true');
                $('#kode').empty().html(result.no_resi);
                $('[name=tanggal]').val(result.tanggal);
                $('[name=customer]').val(result.customer);
                $('[name=id]').val(result.id);
                $('[name=id_ekspedisi]').val(result.id_ekspedisi);
                $('[name=no_pol]').val(result.no_pol);
                if(result.pembayaran != 'tbd'){
                    $('[name=jenis_pembayaran][value='+result.pembayaran+']').attr('checked', true);
                }else{
                    $('[name=jenis_pembayaran]').attr('checked', false);
                }
                if(result.pembayaran == 'cash' && result.id_kas != "0"){
                    $('[name=id_kas]').val(result.id_kas).change();
                }else if(result.pembayaran == 'transfer' && result.id_kas != "0"){
                    $('[name=id_rek_penerima]').val(result.id_kas).change();
                }
                $('#form-ekspedisi [name=pegawai_id]').val(result.pegawai_id).change();
                $('#form-ekspedisi [name=id_area]').val(result.id_area).change();
                $('[name=sumber]').val(result.sumber);
                $('[name=subtotal]').val(result.subtotal);
                $("#modal_form").modal('show');
                $('.autonumeric').autoNumeric('destroy');
                $('.autonumeric').autoNumeric('init', {
                    'mDec': 0
                });
                cek_metode_pembayaran();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {
            }
        });
        
    }

    function tolak(id) {
        Swal.fire({
            title: 'Konfirmasi?',
            text: "Tolak tagihan?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url + '/tolak_pengajuan/' + token,
                    method: 'POST',
                    data: {
                        id: id
                    },
                    dataType: 'json',
                    success: function(result, status, xhr) {
                            reload_page();
                    },
                    error: function(xhr, status, error) {
                        console.log(error);
                    }
                });
            }
        });
    }

    
</script>
@end