@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{site_url('ekspedisi')}}">Ekspedisi</a></li>
        <li class="breadcrumb-item active">Formulir Isian</li>
    </ol>
</nav>
@end

@section('content')
<div class="card">
    <div class="card-body">
        <form action="{{site_url('ekspedisi/save/'.$this->session->auth['token'])}}" method="POST">
            <input type="hidden" name="id" value="{{isset($ekspedisi)?$ekspedisi->id:''}}">
            <!-- batas -->
            <h4 class="mb-4">Detail Ekspedisi</h4>
            <div class="form-row">
                <div class="form-group col-12 col-sm-6">
                    <label>Nomor Resi</label>
                    <input class="form-control iNoResi" type="text" placeholder="(Terisi secara otomatis)" value="{{isset($ekspedisi)?$ekspedisi->no_resi:''}}" disabled>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-12 col-sm-6">
                    <label>Customer <span class="tx-danger">*</span></label>
                    <select name="customer_id" class="form-control select2" data-placeholder="Pilih Customer" required="">
                        <option></option>
                        @foreach($customer AS $row)
                        <option value="{{$row['id']}}" {{isset($ekspedisi) && $ekspedisi->customer_id == $row['id']?'selected':''}}>{{ucwords($row['nama'])}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-12 col-sm-6">
                    <label>Tanggal Jalan <span class="tx-danger">*</span></label>
                    <input type="date" name="tanggal_jalan" class="form-control" value="{{isset($ekspedisi)?$ekspedisi->tanggal_jalan:date('Y-m-d')}}" required>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-12 col-sm-6">
                    <label>Pilih Kendaraan <span class="tx-danger">*</span></label>
                    <select name="kendaraan_id" class="form-control select2" data-placeholder="Pilih Kendaraan" required="">
                        <option></option>
                        @foreach($kendaraan['rows'] AS $s)
                        <option value="{{$s->id}}" {{isset($ekspedisi) && $ekspedisi->kendaraan_id == $s->id?'selected':''}}>{{$s->no_pol}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-12 col-sm-6">
                    <label>Pilih Sopir <span class="tx-danger">*</span></label>
                    <select name="pegawai_id" class="form-control select2" data-placeholder="Pilih Sopir" required="">
                        <option></option>
                        @foreach($sopir['rows'] AS $s)
                        <option value="{{$s->id}}" {{isset($ekspedisi) && $ekspedisi->pegawai_id == $s->id?'selected':''}}>{{$s->nama}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-12 col-sm-6">
                    <label>Uang saku <span class="tx-danger">*</span></label>
                    <input type="number" name="uang_saku" class="form-control" value="{{isset($ekspedisi)?$ekspedisi->uang_saku:'0'}}" placeholder="Masukkan uang saku">
                </div>
                <div class="form-group col-12 col-sm-6">
                    <label>Solar pertama <span class="tx-danger">*</span></label>
                    <input type="number" name="solar_pertama" class="form-control" value="{{isset($ekspedisi)?$ekspedisi->solar_pertama:'0'}}" placeholder="Masukkan solar pertama">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-12 col-sm-6">
                    <label>Metode Pembayaran <span class="tx-danger"></span></label>
                    <div class="col-12 row">
                        @foreach ($pembayaran as $key => $row)
                        <div class="custom-control custom-radio custom-control-inline">
                            @if(isset($ekspedisi) && $ekspedisi->jenis_pembayaran == $row)
                            <input type="radio" id="radio-{{$key}}" name="jenis_pembayaran" value="{{$row}}" class="custom-control-input selesai" checked>
                            @else
                            <input type="radio" id="radio-{{$key}}" name="jenis_pembayaran" value="{{$row}}" class="custom-control-input selesai">
                            @endif

                            @if($row == 'tbd')
                                <label class="custom-control-label" for="radio-{{$key}}">{{ucwords('Belum Tahu')}}</label>
                            @else
                                <label class="custom-control-label" for="radio-{{$key}}">{{ucwords($row)}}</label>
                            @endif
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="form-group col-12 col-sm-6 cash">
                    <label>Sumber Kas <span class="tx-danger"></span></label>
                    <select name="id_kas" class="form-control select2">
                        <option value="">- Pilih Salah Satu -</option>
                        @foreach ($kas as $row)
                            @if(isset($ekspedisi) && $ekspedisi->id_kas == $row->id)
                            <option value="{{$row->id}}" selected="">{{$row->label}}</option>
                            @else
                            <option value="{{$row->id}}">{{$row->label}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-12 col-sm-6 transfer">
                    <label>Sumber Kas <span class="tx-danger"></span></label>
                    <select name="id_rek_pengirim" class="form-control select2">
                        <option value="">- Pilih Salah Satu -</option>
                        @foreach ($rek_pengirim as $row)
                            @if(isset($ekspedisi) && $ekspedisi->id_kas == $row->id)
                            <option value="{{$row->id}}" selected>{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                            @else
                            <option value="{{$row->id}}">{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <!-- batas -->
            <div class="form-row">
                <div class="form-group col-12 col-sm-6">
                    <label>Jenis Barang</label>
                    <input type="text" name="jenis_barang" class="form-control" value="{{isset($ekspedisi)?$ekspedisi->jenis_barang:''}}" placeholder="Masukkan jenis barang">
                </div>
                <div class="form-group col-12 col-sm-6">
                    <label>Catatan </label>
                    <input type="text" name="catatan" class="form-control" value="{{isset($ekspedisi)?$ekspedisi->catatan:''}}" placeholder="Masukkan catatan jika ada" autocomplete="off">
                </div>
            </div>
            <hr>
            <!-- batas -->
            <!-- <div class="form-row">
                <div class="col-12 col-md-6">
                    <h4 class="mb-4">Detail Pengirim</h4>
                    <div class="form-row">
                        <div class="form-group col-12 col-md-6">
                            <label for="">Nama Pengirim/Instansi</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-btn">
                                        <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#mSender"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                                <input type="text" name="sender_nama" class="form-control" value="{{isset($ekspedisi)?$ekspedisi->sender_nama:''}}" placeholder="Tulis nama pengirim" required>
                            </div>
                        </div>
                        <div class="form-group col-12 col-md-6">
                            <label for="">Nomor Telpon</label>
                            <input type="text" name="sender_telpon" class="form-control" value="{{isset($ekspedisi)?$ekspedisi->sender_telpon:''}}" placeholder="Tulis nomor telpon">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Alamat</label>
                        <textarea name="sender_alamat" cols="30" rows="3" class="form-control" placeholder="Tulis alamat" required>{{isset($ekspedisi)?$ekspedisi->sender_alamat:''}}</textarea>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <h4 class="mb-4">Detail Penerima</h4>
                    <div class="form-row">
                        <div class="form-group col-12 col-md-6">
                            <label for="">Nama Penerima/Instansi</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-btn">
                                        <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#mReciver"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                                <input type="text" name="reciver_nama" class="form-control" value="{{isset($ekspedisi)?$ekspedisi->reciver_nama:''}}" placeholder="Tulis nama penerima" required>
                            </div>
                        </div>
                        <div class="form-group col-12 col-md-6">
                            <label for="">Nomor Telpon</label>
                            <input type="text" name="reciver_telpon" class="form-control" value="{{isset($ekspedisi)?$ekspedisi->reciver_telpon:''}}" placeholder="Tulis nomor telpon">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Alamat</label>
                        <textarea name="reciver_alamat" cols="30" rows="3" class="form-control" placeholder="Tulis alamat" required>{{isset($ekspedisi)?$ekspedisi->reciver_alamat:''}}</textarea>
                    </div>
                </div>
            </div>
            <hr> -->
            <!-- batas -->
            <h4 class="mb-4">Ongkos Berangkat</h4>
            <div class="form-row">
                <div class="form-group col-12 col-sm-6">
                    <label>Pilih Tujuan <span class="tx-danger">*</span></label>
                    <select name="id_area" class="form-control select2" data-placeholder="Pilih Tujuan" required="">
                        <option></option>
                        @foreach($area AS $a)
                        <option value="{{$a->id}}" data-rit="{{$a->per_rit}}" data-tonase="{{$a->per_ton}}" {{isset($ekspedisi) && $ekspedisi->id_area == $a->id?'selected':''}}>{{ucwords($a->kota)}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-12 col-sm-6">
                    <label>Pilih Jenis Perhitungan</label>
                    <select name="jenis_perhitungan_berat" class="form-control">
                        <option value="rit" {{isset($ekspedisi) && $ekspedisi->jenis_perhitungan_berat == 'rit'?'selected':''}}>Rit</option>
                        <option value="tonase" {{isset($ekspedisi) && $ekspedisi->jenis_perhitungan_berat == 'tonase'?'selected':''}}>Tonase</option>
                    </select>
                </div>
            </div>
            <!-- batas -->
            <div class="form-row">
                <div class="form-group col-12 col-sm-6">
                    <label>Berat Muatan</label>
                    <div class="input-group">
                        <input type="number" name="berat_muatan" class="form-control" value="{{isset($ekspedisi)?$ekspedisi->berat_muatan:'0'}}" min="0" placeholder="Masukan beratan muatan">
                        <div class="input-group-append"><span class="input-group-text" id="satuan">Kg</span></div>
                    </div>
                </div>
                <div class="form-group col-12 col-sm-6">
                    <label>Harga Satuan</label>
                    <input type="number" name="harga_satuan" class="form-control" value="{{isset($ekspedisi)?$ekspedisi->harga_satuan:''}}" placeholder="Harga satuan">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-12 col-sm-6">
                    <label>Total Biaya</label>
                    <input type="number" name="biaya" class="form-control" value="{{isset($ekspedisi)?$ekspedisi->biaya:''}}" placeholder="Total biaya">
                </div>
            </div>
            <hr>
            <!-- batas -->
            <div class="form-group">
                <button class="btn btn-primary" type="submit"><i class="fa fa-save mr-2"></i>Simpan</button>
                <a href="{{site_url('ekspedisi')}}" class="btn btn-warning" onclick="return confirm('Apakah anda yakin?')"><i class="fa fa-arrow-left mr-2"></i>Kembali</a>
            </div>
        </form>
    </div>
</div>
@end

@section('modal')
<div id="mSender" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">Pilih Pengirim</div>
            <div class="modal-body">
                <table class="table table-js table-striped" data-toolbar="#toolbar" data-search="true" data-search-on-enter-key="true" data-pagination="true" data-side-pagination="server" data-show-refresh="true" data-url="{{site_url('api/internal/customer/get_many/'.$this->session->auth['token'].'?callback=csearch&is_sender=1')}}">
                    <thead>
                        <tr>
                            <th class="text-center fit" data-formatter="formatNomor">Nomor</th>
                            <th class="text-center" data-field="nama">Nama/Instansi</th>
                            <th class="text-left" data-field="alamat">Alamat</th>
                            <th class="text-center" data-field="telpon">Nomor Telpon</th>
                            <th class="text-center fit is_sender" data-field="aksi">Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<div id="mReciver" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">Pilih Penerima</div>
            <div class="modal-body">
                <table class="table table-js table-striped" data-toolbar="#toolbar" data-search="true" data-search-on-enter-key="true" data-pagination="true" data-side-pagination="server" data-show-refresh="true" data-url="{{site_url('api/internal/customer/get_many/'.$this->session->auth['token'].'?callback=csearch&is_reciver=1')}}">
                    <thead>
                        <tr>
                            <th class="text-center fit" data-formatter="formatNomor">Nomor</th>
                            <th class="text-center" data-field="nama">Nama/Instansi</th>
                            <th class="text-left" data-field="alamat">Alamat</th>
                            <th class="text-center" data-field="telpon">Nomor Telpon</th>
                            <th class="text-center fit is_reciver" data-field="aksi">Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script>
    // VARS
    var url = "{{site_url('api/internal/ekspedisi')}}";
    var token = "{{$this->session->auth['token']}}";
    // EVENTS
    $(document).ready(function(){
        set_satuan();
        cek_metode_pembayaran();
    });

    $('.select2').select2({});

    $("[name=id_area]").on('change', function(e) {
        var metode = $("[name=jenis_perhitungan_berat]").val();
        var biaya = $("[name=id_area] option:selected").data(metode);
        if (metode == 'tonase') {
            $('[name=harga_satuan').val(biaya/1000);
        }else{
            $('[name=harga_satuan').val(biaya);
        }
        hitung_biaya();
    });

    $("[name=jenis_perhitungan_berat]").on('change', function() {
        $("[name=id_area]").trigger("change");
        set_satuan();
    });

    $("[name=harga_satuan], [name=berat_muatan]").on('keyup change', function() {
        hitung_biaya();
    });

    $("[name=berat]").on('blur', function() {
        $("[name=id_area]").trigger("change");
    });

    $('[name=jenis_pembayaran]').change(function(){
        cek_metode_pembayaran();
    });

    // FUNCTIONS
    function pilih(el) {
        var tr = $(el).parent().parent();
        var jenis = $(el).parent().hasClass('is_sender')?'sender':'reciver';

        $("[name=" + jenis + "_nama]").val(tr.find(".sNama").text());
        $("[name=" + jenis + "_alamat]").val(tr.find(".salamat").text());
        $("[name=" + jenis + "_telpon]").val(tr.find(".stelpon").text());
        $(".modal").modal('hide');
    }

    function hitung_biaya(){
        let muatan = parseInt($('[name=berat_muatan]').val()) || 0;
        let harga_satuan = parseInt($('[name=harga_satuan]').val()) || 0;
        let total = muatan * harga_satuan;
        $('[name=biaya]').val(total);
    }

    function set_satuan(){
        if($("[name=jenis_perhitungan_berat]").find(":selected").val() == 'rit'){
            $('#satuan').empty().html('Rit');
        }else{
            $('#satuan').empty().html('Kg');
        }
    }

    function cek_metode_pembayaran(){
        let jenis_pembayaran = $('[name=jenis_pembayaran]:checked').val();
        if(jenis_pembayaran == 'cash'){
            $('.cash').fadeIn();
            $('.transfer').hide();
        }else if(jenis_pembayaran == 'transfer'){
            $('.cash').hide();
            $('.transfer').fadeIn();
        }else{
            $('.cash').hide();
            $('.transfer').hide();
        }
    }
</script>
@end