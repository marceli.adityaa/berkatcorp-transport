@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{site_url('ekspedisi')}}">Ekspedisi</a></li>
        <li class="breadcrumb-item active">Persetujuan Keberangkatan</li>
    </ol>
</nav>
@end

@section('content')

<div id="accordion" class="accordion mg-t-20" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h6 class="mg-b-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">
                    Filter Data
                </a>
            </h6>
        </div><!-- card-header -->

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block pd-20">
                <form method="get" action="<?= base_url('ekspedisi/persetujuan?')?>" id="form-filter">
                    <?php $get = $this->input->get()?>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Tanggal Mulai</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="start" autocomplete="off" value="{{!empty($_GET['start']) ? $_GET['start'] : ''}}">
                        </div>
                    </div>
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Sampai Dengan</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <input type="date" class="form-control" name="end" autocomplete="off" value="{{!empty($_GET['end']) ? $_GET['end'] : ''}}">
                        </div>
                    </div>

                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label">Status</label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <select name="status" class="form-control select2">
                                <option value="all">All</option>
                                <?php
                                    foreach((array)$status as $key => $row){
                                        if(!empty($get['status']) && $get['status'] == $key){
                                            echo '<option value="'.$key.'" selected>'.ucwords($row).'</option>';
                                        }else{
                                            echo '<option value="'.$key.'">'.ucwords($row).'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="row mg-t-10">
                        <label class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                            <button type="submit" class="btn btn-dark btn-submit"><i class="fa fa-search mg-r-10"></i>Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- card -->
    <!-- ADD MORE CARD HERE -->
</div>
<div class="card mt-4">
    <div class="card-header card-header-default">DATA EKSPEDISI</div>
    <div class="card-body">
        <div id="toolbar">
        </div>
        <table class="table table-white table-bordered" id="table-data">
            <thead>
                <tr>
                    <th class="text-left">AKSI</th>
                    <th class="text-center" data-sortable="true">STATUS</th>
                    <th class="text-center" data-sortable="true">NO RESI</th>
                    <th class="text-center" data-sortable="true">TGL JALAN</th>
                    <th class="text-center" data-sortable="true">CUSTOMER</th>
                    <th class="text-center" data-sortable="true">KENDARAAN</th>
                    <th class="text-left" data-sortable="true">SUPIR</th>
                    <th class="text-center" data-sortable="true">TUJUAN</th>
                    <th class="text-left" data-sortable="true">JENIS BARANG</th>
                    <th class="text-center">BERAT MUATAN</th>
                    <th class="text-center">UANG SAKU</th>
                    <th class="text-center">SOLAR PERTAMA</th>
                    <th class="text-right">ONGKOS BERANGKAT</th>
                </tr>
            </thead>
            <tbody>
                @if (!empty($ekspedisi))
                    @foreach ($ekspedisi as $row)
                        <tr>
                            <td class="nowrap">
                                @if($row['status_berangkat'] == 0) 
                                    <button class='btn btn-success btn-sm' title='Setujui ekspedisi' onclick="setuju({{$row['id']}})"><i class='fa fa-check'></i></button>
                                    <button class='btn btn-danger btn-sm' title='Batalkan ekspedisi' onclick="tolak({{$row['id']}})"><i class='fa fa-times'></i></button>
                                @elseif($row['status_berangkat'] == 2)
                                    <button class='btn btn-success btn-sm' title='Setujui ekspedisi' onclick="setuju({{$row['id']}})"><i class='fa fa-check'></i></button>
                                @endif
                            </td>
                            <td>
                                @if($row['status_berangkat'] == 0)
                                <span class="badge badge-secondary">menunggu</span>
                                @elseif ($row['status_berangkat'] == 2)
                                <span class="badge badge-danger">ditolak</span>
                                @elseif ($row['status_berangkat'] == 1)
                                <span class="badge badge-success">disetujui</span>
                                @endif
                            </td>
                            <td>
                                <label class="badge badge-light">{{$row['no_resi']}}</label>
                            </td>
                            <td>
                                <label class="badge badge-light">{{$row['tanggal_jalan']}}</label>
                            </td>
                            <td>
                                <label class="badge badge-light">{{ucwords($row['customer'])}}</label>
                            </td>
                            <td>
                                <label class="badge badge-light">{{$row['no_pol']}}</label>
                            </td>
                            <td>
                                <label class="badge badge-light">{{ucwords($row['supir'])}}</label>
                            </td>
                            <td>
                                @if($row['is_sby'])
                                    <label class="badge badge-warning">{{ucwords($row['kota'])}}</label>
                                @else 
                                    <label class="badge badge-light">{{ucwords($row['kota'])}}</label>
                                @endif
                            </td>
                            <td>
                                <label class="badge badge-light">{{ucwords($row['jenis_barang'])}}</label>
                            </td>
                            <td>
                                <label class="badge badge-light">{{($row['berat_muatan'])}}</label>
                            </td>
                            <td>
                                {{monefy($row['uang_saku'], false)}}<br>
                                @if($row['id_kas'] != 0)
                                    <label class="badge badge-light">{{($row['kas_label'])}}</label>
                                @endif
                            </td>
                            <td>
                                {{monefy($row['solar_pertama'], false)}}
                            </td>
                            <td>
                                {{monefy($row['biaya'], false)}}
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="modal_form" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <form method="post" action="<?= base_url('api/internal/ekspedisi/setuju_pengajuan')?>" id="form-ekspedisi">
                <div class="modal-header bg-midnightblack">
                    <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">Form Persetujuan Ekspedisi | <label id="kode" class="tx-white"></label></h6>
                    <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="">
                    <input type="hidden" name="url" value="{{$_SERVER['QUERY_STRING']}}">
                    <div class="form-layout form-layout-4">
                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Tanggal Jalan</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="tanggal_jalan" class="form-control" value="" required="">
                            </div>
                        </div>

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Tujuan</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="id_area" class="form-control select2" required="">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($area as $row)
                                        <option value="{{$row->id}}">{{$row->kota}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Kendaraan</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="kendaraan_id" class="form-control select2-modal" required="">
                                    <option value="">- Pilih Salah Satu</option>
                                    @foreach($kendaraan as $row)
                                        <option value="{{$row['id']}}">{{ucwords($row['no_pol'])}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Supir</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="pegawai_id" class="form-control select2-modal" required="">
                                    <option value="">- Pilih Salah Satu</option>
                                    @foreach($supir as $row)
                                        <option value="{{$row->id}}">{{ucwords($row->nama)}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>                        

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Uang saku</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="uang_saku" class="form-control autonumeric" value="" autocomplete="off" placeholder="Masukkan jumlah uang saku">
                            </div>
                        </div>

                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Solar Pertama</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" name="solar_pertama" class="form-control autonumeric" value="" autocomplete="off" placeholder="Masukkan jumlah solar pertama">
                            </div>
                        </div>
                        
                        <div class="row col-12 my-3">
                            <label class="col-sm-4 form-control-label">Metode Pembayaran</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                @foreach ($pembayaran as $key => $row)
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="radio-{{$key}}" name="jenis_pembayaran" value="{{$row}}" class="custom-control-input" required="">

                                    @if($row == 'tbd')
                                        <label class="custom-control-label" for="radio-{{$key}}">{{ucwords('Belum Tahu')}}</label>
                                    @else
                                        <label class="custom-control-label" for="radio-{{$key}}">{{ucwords($row)}}</label>
                                    @endif
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="row col-12 my-3 cash">
                            <label class="col-sm-4 form-control-label">Sumber Kas</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="id_kas" class="form-control select2-modal">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($kas as $row)
                                        <option value="{{$row->id}}">{{$row->label}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row col-12 my-3 transfer">
                            <label class="col-sm-4 form-control-label">Rekening Pengirim</label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <select name="id_rek_pengirim" class="form-control select2-modal">
                                    <option value="">- Pilih Salah Satu -</option>
                                    @foreach ($rek_pengirim as $row)
                                        <option value="{{$row->id}}">{{$row->bank.' | '.$row->no_rekening.' | '.$row->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save mr-2"></i>Setujui</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
@end

@section('js')
<script src="<?= base_url()?>assets/plugins/autoNumeric/autoNumeric.js"></script>
<script>
    // VAR
    var url = "{{site_url('api/internal/ekspedisi')}}";
    var token = "{{$this->session->auth['token']}}";
    // FUNCTIONS

    $(document).ready(function() {
        $('.select2-modal').select2({
            dropdownParent: $('#modal_form')
        });
        cek_metode_pembayaran()
    });

    $('#table-data').bootstrapTable({
        pagination: true,
        search:true,
        showToggle: true,
        showColumns: true,
        pageSize:10,
        striped: true,
        showFilter: true,
        toolbar: '#toolbar'
    });

    function cek_metode_pembayaran(){
        let jenis_pembayaran = $('[name=jenis_pembayaran]:checked').val();
        if(jenis_pembayaran == 'cash'){
            $('.cash').fadeIn();
            $('.transfer').hide();
        }else if(jenis_pembayaran == 'transfer'){
            $('.cash').hide();
            $('.transfer').fadeIn();
        }else{
            $('.cash').hide();
            $('.transfer').hide();
        }
    }

    $('[name=jenis_pembayaran]').change(function(){
        cek_metode_pembayaran();
    });

    function setuju(id) {
        $.ajax({
            url: url + '/get_detail_ekspedisi/' + token,
            type: "POST",
            dataType: "json",
            data: {
                'id' : id,
            },
            success: function(result) {
                $('#kode').empty().html(result.no_resi);
                $('[name=tanggal_jalan]').val(result.tanggal_jalan);
                $('[name=id]').val(result.id);
                $('#form-ekspedisi [name=kendaraan_id]').val(result.kendaraan_id).change();
                $('#form-ekspedisi [name=pegawai_id]').val(result.pegawai_id).change();
                $('#form-ekspedisi [name=id_area]').val(result.id_area).change();
                
                if(result.jenis_pembayaran == 'cash'){
                    if(result.id_kas == 0){
                        $('[name=id_kas]').val('').change();
                    }else{
                        $('[name=id_kas]').val(result.id_kas).change();
                    }
                    $('[name=id_rek_pengirim]').val('').change();
                }else if(result.jenis_pembayaran == 'transfer'){
                    $('[name=id_kas]').val('').change();
                    if(result.id_kas == 0){
                        $('[name=id_rek_pengirim]').val('').change();
                    }else{
                        $('[name=id_rek_pengirim]').val(result.id_kas).change();
                    }
                }
                
                $('[name=jenis_pembayaran][value='+result.jenis_pembayaran+']').prop('checked', true);
                $('[name=solar_pertama]').val(result.solar_pertama);
                $('[name=uang_saku]').val(result.uang_saku);
                $("#modal_form").modal('show');
                $('.autonumeric').autoNumeric('destroy');
                $('.autonumeric').autoNumeric('init', {
                    'mDec': 0
                });
                cek_metode_pembayaran();
            },
            error: function(e) {
                console.log(e);
            },
            complete: function(e) {
            }
        });
        
    }

    function tolak(id) {
        Swal.fire({
            title: 'Konfirmasi?',
            text: "Tolak pengajuan data ekspedisi?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url + '/tolak_pengajuan/' + token,
                    method: 'POST',
                    data: {
                        id: id
                    },
                    dataType: 'json',
                    success: function(result, status, xhr) {
                        if (result.status === 'success') {
                            // Toast.fire('Sukses!', result.message, 'success');
                            reload_page();
                        } else {
                            // Swal.fire('Error!', result.message, 'error');
                        }
                    },
                    error: function(xhr, status, error) {
                        // Toast.fire('Error!', 'Terjadi kesalahan pada sistem!', 'error');
                    }
                });
            }
        });
    }

    
</script>
@end