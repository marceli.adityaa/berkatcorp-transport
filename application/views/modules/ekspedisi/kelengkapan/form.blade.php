@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item">Ekspedisi</li>
        <li class="breadcrumb-item"><a href="{{site_url('kelengkapan')}}">Kelengkapan</a></li>
        <li class="breadcrumb-item active">Formulir Kelengkapan</li>
    </ol>
</nav>
@end

@section('content')
<div class="card">
    <div class="card-header">Formulir Pengecekan</div>
    <div class="card-body px-0 py-0">
        <form action="{{site_url('kelengkapan/save/'.$this->session->auth['token'])}}" method="POST">
        <input type="hidden" name="id" value="">
        <input type="hidden" name="ekspedisi_id" value="{{$ekspedisi->id}}">
            <table class="table table-bordered table-white">
                <tr>
                    <td class="text-right">No Resi</td>
                    <td><input type="text" class="form-control text-center" readonly value="{{$ekspedisi->no_resi}}"></td>
                </tr>
                <tr>
                    <td class="text-right w-25">No Pol/Sopir</td>
                    <td>
                        <div class="input-group">
                            <input type="text" class="form-control text-center" readonly value="{{$ekspedisi->no_pol}}">
                            <input type="text" class="form-control text-center" readonly value="{{$ekspedisi->pegawai_nama}}">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="text-right">Jenis/Pemeriksa</td>
                    <td>
                        <div class="input-group">
                            <input type="text" name="jenis_cek" class="form-control text-center" value="{{$jenis_cek}}" readonly>
                            <input type="text" name="pemeriksa" class="form-control text-center" value="{{$this->session->auth['nama']}}" readonly>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="text-right">KM</td>
                    <td><input type="number" name="km" class="form-control text-center" placeholder="Masukkan KM kendaraan" required></td>
                </tr>
                <tr>
                    <td class="text-right">Kelengkapan</td>
                    <td>
                        <table class="w-100">
                            <tr>
                                <td>Pemadam</td>
                                <td><label class="switch"><input name="pemadam" type="checkbox" value="1"><span class="slider round"></span></label></td>
                                <td>Dongkrak</td>
                                <td><label class="switch"><input name="dongkrak" type="checkbox" value="1"><span class="slider round"></span></label></td>
                            </tr>
                            <tr>
                                <td>Kunci Roda</td>
                                <td><label class="switch"><input name="kunci_roda" type="checkbox" value="1"><span class="slider round"></span></label></td>
                                <td>Stang Pendek</td>
                                <td><label class="switch"><input name="stang_pendek" type="checkbox" value="1"><span class="slider round"></span></label></td>
                            </tr>
                            <tr>
                                <td>Stang Panjang</td>
                                <td><label class="switch"><input name="stang_panjang" type="checkbox" value="1"><span class="slider round"></span></label></td>
                                <td>Terpal</td>
                                <td><label class="switch"><input name="terpal" type="checkbox" value="1"><span class="slider round"></span></label></td>
                            </tr>
                            <tr>
                                <td>Tampar</td>
                                <td><label class="switch"><input name="tampar" type="checkbox" value="1"><span class="slider round"></span></label></td>
                                <td>P3K</td>
                                <td><label class="switch"><input name="p3k" type="checkbox" value="1"><span class="slider round"></span></label></td>
                            </tr>
                            <tr>
                                <td>Ban Serep</td>
                                <td><label class="switch"><input name="ban_serep" type="checkbox" value="1"><span class="slider round"></span></label></td>
                                <td>Buku KIR</td>
                                <td><label class="switch"><input name="buku_kir" type="checkbox" value="1"><span class="slider round"></span></label></td>
                            </tr>
                            <tr>
                                <td>STNK</td>
                                <td><label class="switch"><input name="stnk" type="checkbox" value="1"><span class="slider round"></span></label></td>
                                <td>Surat Jalan</td>
                                <td><label class="switch"><input name="surat_jalan" type="checkbox" value="1"><span class="slider round"></span></label></td>
                            </tr>
                            <tr>
                                <td>Cek Fisik</td>
                                <td colspan="3"><label class="switch"><input name="cek_fisik" type="checkbox" value="1"><span class="slider round"></span></label></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td><a href="{{site_url('kelengkapan')}}" class="btn btn-dark btn-block"><i class="fa fa-arrow-left mr-2"></i>Kembali</a></td>
                    <td><button class="btn btn-primary btn-block"><i class="fa fa-save mr-2"></i>Simpan</button></td>
                </tr>
            </table>
        </form>
    </div>
</div>
@end

@section('style')
<link rel="stylesheet" href="{{base_url('assets/plugins/toggle/toggle.css')}}">
@end