@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{site_url('ekspedisi')}}">Ekspedisi</a></li>
        <li class="breadcrumb-item active">Kelengkapan</li>
    </ol>
</nav>
@end

@section('content')
<div class="card">
    <div class="card-header">
        <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="tBerangkat" data-toggle="tab" href="#berangkat" role="tab"><i class="fa fa-plane mr-2"></i>BERANGKAT</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="tPulang" data-toggle="tab" href="#pulang" role="tab"><i class="fa fa-truck mr-2"></i>PULANG</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="tRiwayat" data-toggle="tab" href="#riwayat" role="tab"><i class="fa fa-history mr-2"></i>RIWAYAT</a>
            </li>
        </ul>
    </div>
    <div class="card-body px-0 py-0 tab-content">
        <div class="tab-pane fade show active" id="berangkat">
            <!-- berangkat -->
            <table class="table table-js table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">AKSI</th>
                        <th class="text-center" data-sortable="true">NO RESI</th>
                        <th class="text-center" data-sortable="true">NO POL</th>
                        <th class="text-center" data-sortable="true">SOPIR</th>
                        <th class="text-center" data-sortable="true">TUJUAN</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($berangkat['rows'] AS $b)
                    <tr>
                        <td class="text-center"><a href="{{site_url('kelengkapan/form/'.$b->id.'/berangkat')}}" class="btn btn-block btn-primary"><i class="icon-check"></i></a></td>
                        <td class="text-center text-uppercase">{{$b->no_resi}}</td>
                        <td class="text-center text-uppercase">{{$b->no_pol}}</td>
                        <td class="text-center text-uppercase">{{$b->pegawai_nama}}</td>
                        <td class="text-center text-uppercase">{{$b->tujuan}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="tab-pane fade" id="pulang">
            <!-- pulang -->
            <table class="table table-js table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">AKSI</th>
                        <th class="text-center" data-sortable="true">NO RESI</th>
                        <th class="text-center" data-sortable="true">NO POL</th>
                        <th class="text-center" data-sortable="true">SOPIR</th>
                        <th class="text-center" data-sortable="true">TUJUAN</th>
                        <th class="text-center" data-sortable="true">BERANGKAT</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($pulang['rows'] AS $p)
                    <tr>
                        <td class="text-center"><a href="{{site_url('kelengkapan/form/'.$p->id.'/pulang')}}" class="btn btn-block btn-primary"><i class="icon-check"></i></a></td>
                        <td class="text-center text-uppercase text-small">{{$p->no_resi}}</td>
                        <td class="text-center text-uppercase text-small">{{$p->no_pol}}</td>
                        <td class="text-center text-uppercase text-small">{{$p->pegawai_nama}}</td>
                        <td class="text-center text-uppercase text-small">{{$p->tujuan}}</td>
                        <td class="text-center text-uppercase text-small">{{datify($p->tanggal_cek_berangkat, 'd/m, H:i')}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="tab-pane fade px-3 py-3" id="riwayat">
            <!-- riwayat -->
            <table class="table table-js table-bordered" data-search="true" data-toolbar="#toolbar" data-search-on-enter-key="true" data-pagination="true" data-side-pagination="server" data-show-refresh="true" data-url="{{site_url('api/internal/kelengkapan/get_many/'.$this->session->auth['token'].'?callback=ctable')}}">
                <thead>
                    <tr>
                        <th class="text-center" data-field="aksi">AKSI</th>
                        <th class="text-center" data-field="no_resi" data-sortable="true">NO. RESI</th>
                        <th class="text-center" data-field="no_pol" data-sortable="true">NO. POL</th>
                        <th class="text-center" data-field="pegawai_nama" data-sortable="true">SOPIR</th>
                        <th class="text-center" data-field="jenis_cek" data-sortable="true">JENIS<br>PEMERIKSAAN</th>
                        <th class="text-center" data-field="pemeriksa" data-sortable="true">PEMERIKSA</th>
                        <th class="text-center" data-field="insert_time" data-sortable="true">Tanggal Periksa</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@end

@section('modal')
<div class="modal fade" tabindex="-1" role="dialog" id="mDetail" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-vertical-center modal-lg" role="document" style="min-width:50%">
        <div class="modal-content">
            <div class="modal-header bg-midnightblack">
                <h6 class="tx-14 mg-b-0 tx-uppercase color-brown tx-bold">DETAIL KELENGKAPAN</h6>
                <button type="button" class="close tx-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body px-0 py-0">
                <table class="table table-bordered table-striped mb-0"></table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-block btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Tutup</button>
            </div>
        </div>
    </div>
</div>
@end

@section('style')
<style>
    .text-small {
        font-size: 10pt;
    }
</style>
@end

@section('js')
<script>
    // VAR
    var url = "{{site_url('api/internal/kelengkapan')}}";
    var token = "{{$this->session->auth['token']}}";
    // FUNCTIONS
    function detail(id) {
        $.getJSON(url + '/get/' + token + '?id=' + id, function(result) {
            var html = "<tr><td>KM</td><td class='text-center'>" + result.km + "</td><td>Pemadam</td><td class='text-center'>" + setCheck(result.pemadam) + "</td></tr>";
            html += "<tr><td>Dongkrak</td><td class='text-center'>" + setCheck(result.dongkrak) + "</td><td>Kunci Roda</td><td class='text-center'>" + setCheck(result.kunci_roda) + "</td></tr>";
            html += "<tr><td>Stang Pendek</td><td class='text-center'>" + setCheck(result.stang_pendek) + "</td><td>Stang Panjang</td><td class='text-center'>" + setCheck(result.stang_panjang) + "</td></tr>";
            html += "<tr><td>Terpal</td><td class='text-center'>" + setCheck(result.terpal) + "</td><td>Tampar</td><td class='text-center'>" + setCheck(result.tampar) + "</td></tr>";
            html += "<tr><td>P3K</td><td class='text-center'>" + setCheck(result.p3k) + "</td><td>Ban Serep</td><td class='text-center'>" + setCheck(result.ban_serep) + "</td></tr>";
            html += "<tr><td>Buku KIR</td><td class='text-center'>" + setCheck(result.buku_kir) + "</td><td>STNK</td><td class='text-center'>" + setCheck(result.stnk) + "</td></tr>";
            html += "<tr><td>Surat Jalan</td><td class='text-center'>" + setCheck(result.surat_jalan) + "</td><td>Cek Fisik</td><td class='text-center'>" + setCheck(result.cek_fisik) + "</td></tr>";
            $("#mDetail table").empty().html(html);
        });
        $("#mDetail").modal('show');
    }

    function setCheck(status) {
        return (status == 1) ? "<i class='fa fa-check text-primary'></i>" : "<i class='fa fa-times text-danger'></i>";
    }
</script>
@end