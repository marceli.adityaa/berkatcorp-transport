@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item active">Dashboard</li>
	</ol>
</nav>
@end

@section('content')
<div class="form-row">
	<div class="col-12 col-md-4">
		<div class="card card-widget">
			<div class="card-body">
				<span class="tx-12 tx-uppercase">Total Ekspedisi {{date('Y')}}</span>
				<h3 class="mt-2 tx-bold">{{$wEkspedisi}} Ekspedisi</h3>
				<i class="fa fa-truck widget-icon"></i>
			</div>
		</div>
	</div>
	<div class="col-12 col-md-4">
		<div class="card card-widget">
			<div class="card-body">
				<span class="tx-12 tx-uppercase">Laba/Rugi {{date('Y')}}</span>
				<h4 class="mt-2 tx-bold">Rp {{monefy($wJumlah['annual'], false)}}</h4>
				<i class="fa fa-truck widget-icon"></i>
			</div>
		</div>
	</div>
	<div class="col-12 col-md-4">
		<div class="card card-widget">
			<div class="card-body">
				<span class="tx-12 tx-uppercase">Laba/Rugi Bulan Ini</span>
				<h4 class="mt-2 tx-bold">Rp {{monefy($wJumlah['current'], false)}}</h4>
				<i class="fa fa-truck widget-icon"></i>
			</div>
		</div>
	</div>
</div>

<div class="row mt-4">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <span>Grafik Laba/Rugi Tahun {{date('Y')}}</span>
                <canvas id="chart1" width="100%" height="35px"></canvas>
            </div>
        </div>
    </div>
</div>
@end

@section('style')
<link rel="stylesheet" href="{{base_url('assets/plugins/chartjs/chart.min.css')}}">
@end

@section('js')
<script src="{{base_url('assets/plugins/chartjs/chart.min.js')}}"></script>
<script>
    var gData = "{{implode(',', $graph)}}".split(",");
    var myLineChart = new Chart($("#chart1"), {
        type: 'line',
        data: {
            labels: ['Jan','Feb','Mar','Apr','Mei','Jun','Jul','Aug','Sep','Okt','Nov','Des'],
            datasets: [{
                label: 'Laba/Rugi (dalam juta)',
                data: gData,
                backgroundColor: 'rgba(107, 186, 255,.2)',
                borderColor: 'rgba(107, 186, 255)',
                pointBackgroundColor: 'rgba(107, 186, 255)'
            }]
        },
        options:{scales:{yAxes:[{ticks:{precision:0}}]}}
    });
</script>
@end