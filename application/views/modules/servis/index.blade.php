@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{site_url('kendaraan')}}">Kendaraan</a></li>
        <li class="breadcrumb-item active">Servis</li>
    </ol>
</nav>
@end

@section('content')
<div class="card">
    <div class="card-header">Data Servis Kendaraan</div>
    <div class="card-body">
        <div id="toolbar">
            <a href="{{site_url('servis/add')}}" class="btn btn-primary">+ Servis Baru</a>
        </div>
        <table class="table table-striped table-bordered table-js" data-search="true" data-toolbar="#toolbar" data-search-on-enter-key="true" data-pagination="true" data-side-pagination="server" data-show-refresh="true" data-url="{{site_url('api/internal/servis/get_many/'.$this->session->auth['token'].'?callback=callback_table')}}">
            <thead>
                <tr>
                    <th class="text-center text-nowrap" data-field="aksi">Aksi</th>
                    <th class="text-center" data-field="uuid">Kode Servis</th>
                    <th class="text-center" data-field="no_pol">No Pol</th>
                    <th class="text-center" data-field="pegawai_nama">Sopir</th>
                    <th class="text-center" data-field="tanggal" data-formatter="formatDate">Tanggal</th>
                    <th class="text-center" data-field="lokasi">Lokasi</th>
                    <th class="text-center" data-field="mekanik">Mekanik</th>
                    <th class="text-left" data-field="keterangan">Keterangan</th>
                    <th class="text-center" data-field="insert_time">Waktu Entri</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@end

@section('modal')
<div id="mDetail" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="my-modal-title">Rincian Servis</h5>
                <button class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body px-0 py-0">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Jenis Perbaikan</th>
                            <th class="text-center">Jumlah Item</th>
                            <th class="text-center">Total Biaya</th>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="4" class="text-center">Tidak ada rincian</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@end

@section('js')
<script>
    // VAR
    var url = "{{site_url('api/internal/servis')}}";
    var token = "{{$this->session->auth['token']}}";
    // FUNCTIONS
    function detail(id) {
        $.getJSON(url + '/get_detail_servis/' + id + '?token=' + token, function(result) {
            var html = "";
            $.each(result, function(index, value) {
                html += "<tr>";
                html += "<td class='text-left'>" + value.perbaikan + "</td>";
                html += "<td class='text-center'>" + value.jumlah + "</td>";
                html += "<td class='text-center'>" + value.biaya + "</td>";
                html += "<td class='text-left'>" + value.keterangan + "</td>";
                html += "<tr>";
            });
            $("#mDetail table tbody").empty().html(html);
        });
        $("#mDetail").modal('show');
    }
</script>
@end