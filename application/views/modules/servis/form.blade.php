@layout('commons/index')

@section('breadcrumb')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{site_url('dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{site_url('kendaraan')}}">Kendaraan</a></li>
        <li class="breadcrumb-item"><a href="{{site_url('servis')}}">Servis</a></li>
        <li class="breadcrumb-item active">Tambah</li>
    </ol>
</nav>
@end

@section('content')
<div class="card">
    <div class="card-header">Formulir Servis Kendaraan</div>
    <div class="card-body">
        <form action="{{site_url('servis/save')}}" method="POST">
            <input type="hidden" name="id" value="{{isset($servis)?$servis->id:''}}">
            <div class="form-row">
                <div class="form-group col-12 col-lg-6">
                    <label>Kode Transaksi</label>
                    <input class="form-control" type="text" name="uuid" value="{{isset($servis)?$servis->uuid:$uuid}}" readonly>
                </div>
                <div class="form-group col-12 col-lg-6">
                    <label>Tanggal Servis</label>
                    <input class="form-control" type="date" name="tanggal" placeholder="Pilih tanggal servis" value="{{isset($servis)?$servis->tanggal:''}}" required>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-12 col-lg-6">
                    <label>Sopir</label>
                    <select class="form-control select2" name="pegawai_id" data-placeholder="Pilih sopir" required>
                        <option></option>
                        @foreach($sopir AS $s)
                        <option value="{{$s->id}}" {{isset($servis) && $servis->pegawai_id==$s->id?'selected':''}}>{{$s->nama}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-12 col-lg-6">
                    <label>Kendaran</label>
                    <select class="form-control select2" name="kendaraan_id" data-placeholder="Pilih Kendaraan" required>
                        <option></option>
                        @foreach($kendaraan AS $k)
                        <option value="{{$k->id}}" {{isset($servis) && $servis->kendaraan_id==$k->id?'selected':''}}>{{$k->no_pol}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-12 col-lg-4">
                    <label>Mekanik</label>
                    <input class="form-control" type="text" name="mekanik" value="{{isset($servis)?$servis->mekanik:''}}" placeholder="Tulis nama mekanik">
                </div>
                <div class="form-group col-12 col-lg-4">
                    <label>Lokasi Bengkel</label>
                    <input class="form-control" type="text" name="lokasi" value="{{isset($servis)?$servis->lokasi:''}}" placeholder="Tulis lokasi bengkel">
                </div>
                <div class="form-group col-12 col-lg-4">
                    <label>Keterangan (opsional)</label>
                    <input class="form-control" type="text" name="keterangan" value="{{isset($servis)?$servis->keterangan:''}}" placeholder="Tulis keterangan">
                </div>
            </div>
            <div class="card mt-3">
                <div class="card-header d-flex">
                    <span>Rincian servis</span>
                    <button type="button" class="btn btn-sm btn-primary ml-auto bAddRow">tambah</button>
                </div>
                <div class="card-body px-0 py-0">
                    <table class="table table-sm striped table-bordered" id="tRincian">
                        <thead>
                            <tr>
                                <th class="text-center fit">Aksi</th>
                                <th class="text-center">Perbaikan</th>
                                <th class="text-center fit">Jumlah Item</th>
                                <th class="text-center">Biaya (total)</th>
                                <th class="text-center">Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($servis))
                            @foreach($servis->rincian AS $r)
                            <tr>
                                <td><button type="button" class="btn btn-sm btn-danger bDelRow"><i class="fa fa-trash"></i></button>
                                <td><input type="text" name="rincian[{{$r->id}}][perbaikan]" class="form-control" value="{{$r->perbaikan}}" placeholder="Tulis jenis perbaikan"></td>
                                <td><input type="number" name="rincian[{{$r->id}}][jumlah]" class="form-control text-center" value="{{$r->jumlah}}" value="1" placeholder="Tulis jumlah item"></td>
                                <td><input type="number" name="rincian[{{$r->id}}][biaya]" class="form-control" value="{{$r->biaya}}" placeholder="Tulis biaya perbaikan"></td>
                                <td><input type="text" name="rincian[{{$r->id}}][keterangan]" class="form-control" value="{{$r->keterangan}}" placeholder="Tulis keterangan (opsional)"></td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="form-group text-right">
                <button class="btn btn-primary"><i class="fa fa-save mr-2"></i>Simpan Data</button>
                <a href="{{site_url('servis')}}" onclick="return confirm('Data belum disimpan, apakah anda yakin ingin kembali?')" class="btn btn-warning"><i class="fa fa-reply mr-2"></i>Kembali</a>
            </div>
        </form>
    </div>
</div>
@end

@section('js')
<script>
    // VAR
    var elIndex = 1;
    // INIT
    $(".select2").select2();

    // EVENTS
    $(".bAddRow").on("click", function() {
        var html = '<tr><td><button type="button" class="btn btn-sm btn-danger bDelRow"><i class="fa fa-trash"></i></button></td>';
        html += '<td><input type="text" name="rincian[' + (elIndex) + '][perbaikan]" class="form-control" placeholder="Tulis jenis perbaikan"></td>';
        html += '<td><input type="number" name="rincian[' + (elIndex) + '][jumlah]" class="form-control text-center" value="1" placeholder="Tulis jumlah item"></td>';
        html += '<td><input type="number" name="rincian[' + (elIndex) + '][biaya]" class="form-control" placeholder="Tulis biaya perbaikan"></td>';
        html += '<td><input type="text" name="rincian[' + (elIndex++) + '][keterangan]" class="form-control" placeholder="Tulis keterangan (opsional)"></td></tr>';
        $("#tRincian tbody").append(html);
    });

    $("#tRincian tbody").on("click", ".bDelRow", function(e) {
        $(e.currentTarget).parent().parent().remove();
    });
</script>
@end