<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelengkapan_model extends MY_Model {
	public $_table = 'kelengkapan';
	private $kolom = array('');

	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_data($filter = array())
	{
		# LIMIT, OFFSET, AND SORT
		$limit  = isset($filter['limit']) ? $filter['limit'] : '';
		$offset = isset($filter['offset']) ? $filter['offset'] : '';
		$sort   = isset($filter['sort']) ? $filter['sort'] : 'insert_time';
		$order  = isset($filter['sort']) && isset($filter['order']) ? $filter['order'] : 'DESC';

		# SELECT
		$this->select('kelengkapan.id, jenis_cek, pemeriksa, no_pol, no_resi, pegawai_id, kelengkapan.insert_time, ekspedisi_id');

		# WHERE
		$where['ekspedisi.id'] = isset($filter['id']) ? $filter['id'] : '';

		# JOIN
		$this->join('ekspedisi', 'ekspedisi_id = ekspedisi.id');
		$this->join('kendaraan', 'kendaraan_id = kendaraan.id');

		# EXCLUDE
		$excludes = isset($filter['excludes']) ? $filter['excludes'] : array();

		# UNSET FILTER
		$filter = $this->unsetFilter($this->setFilter($filter));
		$where  = trim_array($where);

		# SET LIKE
		if (count($filter) > 0) {
			$this->group_start();
			$this->or_like($filter);
			$this->group_end();
		}

		# SET WHERE NOT IN
		foreach ($excludes as $key => $value) {
			if (!empty($value)) {
				$this->where_not_in($key, $value);
			}
		}

		# Set total
		$clone = clone ($this->db);
		$results['total'] = $clone->where($where)->from($this->_table)->count_all_results();

		if (!empty($limit) or !empty($offset)) {
			$this->limit($limit, $offset);
		}

		# SET SORT
		if (!empty($sort)) {
			$this->order_by($sort, $order);
		}

		$results['rows'] = $this->get_many_by($where);
		return isset($results['total']) ? $results : $results['rows'];
	}

	private function setFilter($filter = array())
	{
		if (isset($filter['search'])) {
			foreach ($this->kolom as $k) {
				$filter[$k] = $filter['search'];
			}
		}

		return $filter;
	}

	private function unsetFilter($filter)
	{
		unset($filter['limit'], $filter['offset'], $filter['order'], $filter['sort']);
		unset($filter['id'], $filter['search'], $filter['excludes']);
		return trim_array($filter);
	}

	public function get_detail($id){
		$this->select('a.id, b.km as km_awal, b.pemeriksa as pemeriksa_awal, b.pemadam as pemadam_awal, b.dongkrak as dongkrak_awal, b.kunci_roda as kunci_roda_awal, b.stang_pendek as stang_pendek_awal, b.stang_panjang as stang_panjang_awal, b.terpal as terpal_awal, b.tampar as tampar_awal, b.p3k as p3k_awal, b.ban_serep as ban_serep_awal, b.buku_kir as buku_kir_awal, b.stnk as stnk_awal, b.surat_jalan as surat_jalan_awal, b.cek_fisik as cek_fisik_awal, c.km as km_akhir, c.pemeriksa as pemeriksa_akhir, c.pemadam as pemadam_akhir, c.dongkrak as dongkrak_akhir, c.kunci_roda as kunci_roda_akhir, c.stang_pendek as stang_pendek_akhir, c.stang_panjang as stang_panjang_akhir, c.terpal as terpal_akhir, c.tampar as tampar_akhir, c.p3k as p3k_akhir, c.ban_serep as ban_serep_akhir, c.buku_kir as buku_kir_akhir, c.stnk as stnk_akhir, c.surat_jalan as surat_jalan_akhir, c.cek_fisik as cek_fisik_akhir');
		$this->where('a.id', $id);
		$this->join('kelengkapan b', 'a.id = b.ekspedisi_id and b.jenis_cek = "berangkat"');
		$this->join('kelengkapan c', 'a.id = c.ekspedisi_id and c.jenis_cek = "pulang"');
		$result = $this->db->get('ekspedisi a');
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
	}
}