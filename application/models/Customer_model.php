<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_model extends MY_Model {
	public $_table = 'customer';
	public $kolom = ['nama','alamat','telpon'];

	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_data($filter = array())
	{
		# LIMIT, OFFSET, AND SORT
		$limit  = isset($filter['limit']) ? $filter['limit'] : '';
		$offset = isset($filter['offset']) ? $filter['offset'] : '';
		$sort   = isset($filter['sort']) ? $filter['sort'] : '';
		$order  = isset($filter['order']) ? $filter['order'] : '';

		# SELECT
		$this->select("*");
		
		# WHERE
		$where = array();

		# CUSTOM WHERE
		if (isset($filter['custom_where'])) {
			$where = array_merge($where, $filter['custom_where']);
		}

		# JOIN
		// JOIN HERE

		# EXCLUDE
		$excludes = isset($filter['excludes']) ? $filter['excludes'] : array();

		# UNSET FILTER
		$filter = $this->unsetFilter($this->setFilter($filter));
		$where  = trim_array($where);

		# SET LIKE
		if (count($filter) > 0) {
			$this->group_start();
			$this->or_like($filter);
			$this->group_end();
		}

		# SET WHERE NOT IN
		if (count($excludes) > 0) {
			foreach ($excludes as $key => $value) {
				$this->where_not_in($key, $value);
			}
		}

		# Set total
		$clone = clone ($this->db);
		$results['total'] = $clone->where($where)->from($this->_table)->count_all_results();

		if (!empty($limit) or !empty($offset)) {
			$this->limit($limit, $offset);
		}

		# SET SORT
		if (!empty($sort)) {
			$this->order_by($sort, $order);
		}

		$results['rows'] = $this->get_many_by($where);
		return isset($results['total']) ? $results : $results['rows'];
	}

	private function setFilter($filter = array())
	{
		if (isset($filter['search'])) {
			foreach ($this->kolom as $k) {
				$filter[$k] = $filter['search'];
			}
		}

		return $filter;
	}

	private function unsetFilter($filter)
	{
		unset($filter['limit'], $filter['offset'], $filter['order'], $filter['sort']);
		unset($filter['id'], $filter['search'], $filter['excludes'], $filter['custom_where']);
		return trim_array($filter);
	}

	public function get_active(){
		$this->db->order_by('nama');
		$this->db->where('is_disabled', 0);
		$result = $this->db->get('customer');
		if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}
}