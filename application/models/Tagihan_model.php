<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tagihan_model extends MY_Model {
	public $_table = 'tagihan';

	public function __construct()
	{
		parent::__construct();
    }

    public function get_detail($id = null){
        $this->select("a.*, b.pegawai_id, b.has_balen, b.no_resi, c.kota, d.nama as customer, e.no_pol");
		$this->join('ekspedisi b', 'a.id_ekspedisi = b.id');
		$this->join('area c', 'a.id_area = c.id');
		$this->join('customer d', 'a.customer_id = d.id');
		$this->join('kendaraan e', 'b.kendaraan_id = e.id');
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        $result = $this->db->get('tagihan a');
        if ($result->num_rows() > 0) {
            if(!empty($id)){
                return $result->row_array();
            }else{
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

	public function get_data(){
		$get = $this->input->get();
		$this->select("a.*, b.pegawai_id, b.has_balen, b.no_resi, c.kota, d.nama as customer, e.no_pol");
		$this->join('ekspedisi b', 'a.id_ekspedisi = b.id');
		$this->join('area c', 'a.id_area = c.id');
		$this->join('customer d', 'a.customer_id = d.id');
		$this->join('kendaraan e', 'b.kendaraan_id = e.id');
		if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('a.tanggal >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('a.tanggal <=', $get['end']);  
        }
        if(isset($get['kendaraan']) && $get['kendaraan'] != 'all'){
            $this->db->where('b.kendaraan_id', $get['kendaraan']);  
        }
        if(isset($get['supir']) && $get['supir'] != 'all'){
            $this->db->where('b.pegawai_id', $get['supir']);  
        }
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                # Belum lunas
				$this->db->where('a.is_lunas', 0);
            }else if($get['status'] == 2){
                # Sudah lunas
				$this->db->where('a.is_lunas', 1);
            }
			
		}
		$this->db->order_by('tanggal');
		$result = $this->db->get('tagihan a');
        
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}
}