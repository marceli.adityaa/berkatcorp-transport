<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kendaraan_biaya_model extends MY_Model
{
    public $_table = 'kendaraan_biaya';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_data($id = null)
    {
        if(!empty($id)){
            $this->db->where('id', $id);
        }
        $result = $this->db->get('kendaraan_biaya');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }


    public function get_data_kelola($id = null)
    {
        $get = $this->input->get();
        $this->db->select('a.*, b.nama as kategori, concat(c.no_pol," | ",c.merk," | ",c.tipe) as kendaraan, c.no_pol');
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        if(isset($get['kategori']) && $get['kategori']!= 'all'){
            $this->db->where('a.id_kategori', $get['kategori']);
        }

        if(!empty($get['start'])){
            $this->db->where('a.tanggal >=', $get['start']);
        }
        if(!empty($get['end'])){
            $this->db->where('a.tanggal <=', $get['end']);
        }
        if(isset($get['status']) && $get['status']!= 'all'){
            if($get['status'] == 1){
                # belum diotorisasi
                $this->db->where('a.is_verifikasi', 0);
            }else if($get['status'] == 2){
                # acc
                $this->db->where('a.is_verifikasi', 1);
            }else if($get['status'] == 2){
                # tolak
                $this->db->where('a.is_verifikasi', 2);
            }   
        }
        $this->db->join('master_biaya_kendaraan b', 'a.id_kategori = b.id');
        $this->db->join('kendaraan c', 'a.id_kendaraan = c.id');
        $this->db->order_by('a.tanggal');
        $this->db->order_by('b.nama');
        $result = $this->db->get('kendaraan_biaya a');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_rekap_beban($id = null)
    {
        $get = $this->input->get();
        $this->db->select('a.*, b.nama as biaya, c.nama as kategori');
        $this->db->where('a.is_verifikasi', 1);
        if(!empty($id)){
            $this->db->where('a.id', $id);
        }
        if(isset($get['kategori']) && $get['kategori']!= 'all'){
            $this->db->where('b.id_biaya', $get['kategori']);
        }

        if(isset($get['biaya']) && $get['biaya']!= 'all'){
            $this->db->where('a.id_beban', $get['biaya']);
        }

        if(!empty($get['start'])){
            $this->db->where('a.tgl_transaksi >=', $get['start']);
        }
        if(!empty($get['end'])){
            $this->db->where('a.tgl_transaksi <=', $get['end']);
        }

        $this->db->join('master_akun_biaya_detail b', 'a.id_beban = b.id');
        $this->db->join('master_akun_biaya c', 'b.id_biaya = c.id');
        $this->db->order_by('a.tgl_transaksi', 'desc');
        $this->db->order_by('c.nama');
        $this->db->order_by('b.nama');
        $result = $this->db->get('beban a');
        if ($result->num_rows() > 0) {
            if (!empty($id)) {
                return $result->row_array();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    
}
