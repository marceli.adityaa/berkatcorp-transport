<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Piutang_detail_model extends MY_Model
{
    public $_table = 'piutang_detail';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_data($id = null)
    {
        $this->db->select('a.id_customer, b.*');
        if(!empty($id)){
            $this->db->where('b.id', $id);
        }
        $this->db->join('piutang_detail b', 'b.id_piutang = a.id');
        $result = $this->db->get('piutang a');
        if ($result->num_rows() > 0) {
            if(!empty($id)){
                return $result->row_array();
            }else{
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_data_piutang_verified($piutang, $id = null)
    {
        $this->db->select('c.nama as debitur, b.*');   
        $this->db->where('a.id', $piutang);
        if(!empty($id)){
            $this->db->where('b.id <>', $id);
        }
        $this->db->where('b.is_verifikasi', 1);
        $this->db->join('piutang_detail b', 'a.id = b.id_piutang');
        $this->db->join('customer c', 'a.id_customer = c.id');
        $result = $this->db->get('piutang a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_piutang_detail($piutang)
    {
        $this->db->select('c.nama as debitur, b.*');   
        $this->db->where('a.id', $piutang);
        $this->db->join('piutang_detail b', 'a.id = b.id_piutang');
        $this->db->join('customer c', 'a.id_customer = c.id');
        $this->db->order_by('b.tgl_transaksi');
        $result = $this->db->get('piutang a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_rekap_piutang_detail($piutang)
    {
        $this->db->select('a.jenis_piutang, c.nama as debitur, b.*');   
        $this->db->where('a.id', $piutang);
        $this->db->where('b.is_verifikasi', 1);
        $this->db->join('piutang_detail b', 'a.id = b.id_piutang');
        $this->db->join('customer c', 'a.id_customer = c.id');
        $this->db->order_by('b.tgl_transaksi');
        $result = $this->db->get('piutang a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_persetujuan_piutang()
    {
        $get = $this->input->get();
        $this->db->select('c.nama as debitur, b.*');   
        if(isset($get['debitur']) && $get['debitur'] != 'all'){
            $this->db->where('a.id_customer', $get['debitur']);
        }
        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('b.tgl_transaksi >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('b.tgl_transaksi <=', $get['end']);
        }
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                $this->db->where('b.is_verifikasi', 0);
            }else if($get['status'] == 2){
                $this->db->where('b.is_verifikasi', 1);
            }else if($get['status'] == 3){
                $this->db->where('b.is_verifikasi', 2);
            }
        }
        $this->db->join('piutang_detail b', 'a.id = b.id_piutang');
        $this->db->join('customer c', 'a.id_customer = c.id');
        $result = $this->db->get('piutang a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function cek_data_validasi($id)
    {
        $this->db->where('is_verifikasi', 1);
        $this->db->where('id_piutang', $id);
        $result = $this->db->get('piutang_detail');
        if ($result->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function delete_by_transaksi($id)
    {
        $this->db->where('id_piutang', $id);
        return $this->db->delete('piutang_detail');
    }
}
