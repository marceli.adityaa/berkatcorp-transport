<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ekspedisi_model extends MY_Model
{
	public $_table = 'ekspedisi';
	private $kolom = array('no_pol', 'lokasi', 'jenis_barang', 'tanggal_transaksi');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Pegawai_model', 'pegawai');
	}

	public function get_data($filter = array())
	{
		# LIMIT, OFFSET, AND SORT
		$limit  = isset($filter['limit']) ? $filter['limit'] : '';
		$offset = isset($filter['offset']) ? $filter['offset'] : '';
		$sort   = isset($filter['sort']) ? $filter['sort'] : 'tanggal_entri';
		$order  = isset($filter['order']) ? $filter['order'] : '';

		# SELECT
		$this->select("ekspedisi.*, kendaraan.no_pol");

		# WHERE
		$where['ekspedisi.id'] = isset($filter['id']) ? $filter['id'] : '';
		$where['pegawai_id'] = isset($filter['pegawai_id']) ? $filter['pegawai_id'] : '';
		$where['is_done'] = isset($filter['is_done']) ? $filter['is_done'] : '';
		$where['ekspedisi.is_deleted'] = isset($filter['is_deleted']) ? $filter['is_deleted'] : '';

		# CUSTOM WHERE
		if(isset($filter['custom_where'])) {
			$where = array_merge($where, $filter['custom_where']);
		}

		# JOIN
		$this->join('kendaraan', 'kendaraan_id = kendaraan.id');

		# EXCLUDE
		$excludes = isset($filter['excludes']) ? $filter['excludes'] : array();

		# UNSET FILTER
		$filter = $this->unsetFilter($this->setFilter($filter));
		$where  = trim_array($where);

		# SET LIKE
		if (count($filter) > 0) {
			$this->group_start();
			$this->or_like($filter);
			$this->group_end();
		}

		# SET WHERE NOT IN
		if (count($excludes) > 0) {
			foreach ($excludes as $key => $value) {
				$this->where_not_in($key, $value);
			}
		}

		# Set total
		$clone = clone ($this->db);
		$results['total'] = $clone->where($where)->from($this->_table)->count_all_results();

		if (!empty($limit) or !empty($offset)) {
			$this->limit($limit, $offset);
		}

		# SET SORT
		if (!empty($sort)) {
			$this->order_by($sort, $order);
		}

		$results['rows'] = $this->get_many_by($where);
		return isset($results['total']) ? $results : $results['rows'];
	}

	private function setFilter($filter = array())
	{
		if (isset($filter['search'])) {
			foreach ($this->kolom as $k) {
				$filter[$k] = $filter['search'];
			}
		}

		return $filter;
	}

	private function unsetFilter($filter)
	{
		unset($filter['limit'], $filter['offset'], $filter['order'], $filter['sort']);
		unset($filter['id'], $filter['search'], $filter['excludes'], $filter['custom_where']);
		unset($filter['is_done'], $filter['pegawai_id'], $filter['is_deleted']);
		return trim_array($filter);
	}

	public function getEkspedisiAktif()
	{
		$data = $this->get_data(['is_done' => '0','ekspedisi.is_deleted'=>'0']);
		$data['rows'] = $this->pegawai->join($data['rows']);

		return $data;
	}

	public function get_active($param)
	{
		$this->db->select('a.*, b.no_pol');
		$this->db->join('kendaraan b', 'a.kendaraan_id = b.id');
		if(isset($param['start']) && $param['start'] != ''){
			$this->db->where('a.tanggal_jalan >=', $param['start']);
		}
		if(isset($param['end']) && $param['end'] != ''){
            $this->db->where('a.tanggal_jalan <=', $param['end']);  
        }
        if(isset($param['kendaraan']) && $param['kendaraan'] != 'all'){
            $this->db->where('a.kendaraan_id', $param['kendaraan']);  
        }
		$this->db->where('a.status_berangkat<>', 2);
        if(isset($param['status']) && $param['status'] != 'all'){
            if($param['status'] == 1){
                # menunggu input
                $this->db->where('a.is_done', 0);
                $this->db->where('a.status_berangkat', 0);
            }else if($param['status'] == 2){
                # proses
                $this->db->where('a.is_done', 0);
                $this->db->where('a.status_berangkat', 1);
            }else if($param['status'] == 3){
                # selesai
				$this->db->where('a.status_berangkat', 1);
                $this->db->where('a.is_done', 1);
            }
        }
		$this->db->where('a.is_deleted', 0);
		$this->db->order_by('tanggal_jalan', 'desc');
		$result = $this->db->get('ekspedisi a');
		if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}

	public function get_detail_resi($resi)
	{
		$this->db->select('a.*, b.no_pol');
		$this->db->join('kendaraan b', 'a.kendaraan_id = b.id');
		$this->db->where('a.is_done', 0);
		$this->db->where('a.is_deleted', 0);
		$this->db->where('a.no_resi', $resi);
		$result = $this->db->get('ekspedisi a');
		if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return false;
        }
	}

	public function get_detail_resi2($resi)
	{
		$this->db->select('a.*, b.no_pol');
		$this->db->join('kendaraan b', 'a.kendaraan_id = b.id');
		$this->db->where('a.is_deleted', 0);
		$this->db->where('a.no_resi', $resi);
		$result = $this->db->get('ekspedisi a');
		if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return false;
        }
	}

	public function get_detail_ekspedisi($id)
	{
		$this->db->select('a.*, b.no_pol');
		$this->db->join('kendaraan b', 'a.kendaraan_id = b.id');
		$this->db->where('a.id', $id);
		$result = $this->db->get('ekspedisi a');
		if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return false;
        }
	}

	public function getEkspedisiBerangkat()
	{
		$data['rows'] = $this->where([
			// 'status_berangkat' => 1,
			'is_done' => '0',
			'ekspedisi.is_deleted' => '0',
			'tanggal_cek_berangkat' => null,
			'tanggal_cek_pulang' => null,
			'tanggal_input_biaya' => null,
			'tanggal_verifikasi_akhir' => null,
		])
			->join('kendaraan', 'kendaraan_id=kendaraan.id')
			->join('area', 'id_area=area.id')
			->select('ekspedisi.*, no_pol, kota as tujuan')
			->get_all();
		$data['rows'] = $this->pegawai->join($data['rows']);

		return $data;
	}

	public function getEkspedisiPulang()
	{
		$data['rows'] = $this->where([
			// 'status_berangkat' => 1,
			'is_done' => '0',
			'ekspedisi.is_deleted' => '0',
			'tanggal_cek_berangkat !=' => null,
			'tanggal_cek_pulang' => null,
			'tanggal_input_biaya' => null,
			'tanggal_verifikasi_akhir' => null,
		])
			->join('kendaraan', 'kendaraan_id=kendaraan.id')
			->select('ekspedisi.*, no_pol')
			->get_all();
		$data['rows'] = $this->pegawai->join($data['rows']);

		return $data;
	}

	public function get_biaya_belum_entri()
	{
		# Ambil data pegawai
		$this->load->model('Pegawai_model', 'pegawai');
        $pegawai = $this->pegawai->get($this->session->auth['id']);
		# Tentukan apakah sopir
		$filter = array();
        if ($pegawai['kelompok_id'] == 2) {
            $filter['pegawai_id'] = $pegawai['id'];
		}
		
		$filter['tanggal_cek_pulang!='] = null;
		$filter['tanggal_input_biaya'] = null;

		return $this->count_by($filter);
	}

	public function get_widget()
	{
		$month = date('m');
		$year  = date('Y');

		$result['total'] = $this->select("COUNT(id) AS total")->get_by(['MONTH(tanggal_jalan)' => $month, 'YEAR(tanggal_jalan)' => $year])->total;
		$result['aktif'] = $this->select("COUNT(id) AS total")->get_by(['is_done' => 0])->total;

		return $result;
	}

	public function get_data_ekspedisi()
	{
		$get = $this->input->get();
		$this->select("a.*, b.no_pol, c.kota, c.is_sby, d.nama as customer");
		$this->join('kendaraan b', 'a.kendaraan_id = b.id');
		$this->join('area c', 'a.id_area = c.id');
		$this->join('customer d', 'a.customer_id = d.id');
		if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('tanggal_jalan >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('tanggal_jalan <=', $get['end']);  
        }
        if(isset($get['kendaraan']) && $get['kendaraan'] != 'all'){
            $this->db->where('a.kendaraan_id', $get['kendaraan']);  
        }
        if(isset($get['supir']) && $get['supir'] != 'all'){
            $this->db->where('a.pegawai_id', $get['supir']);  
        }
		if(isset($get['customer']) && $get['customer'] != 'all'){
            $this->db->where('a.customer_id', $get['customer']);  
        }

        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                # Menunggu
				$this->db->where('a.is_done', 0);
				$this->db->where('a.status_berangkat', 0);
            }else if($get['status'] == 2){
                # Berlangsung
				$this->db->where('a.is_done', 0);
				$this->db->where('a.status_berangkat', 1);
            }else if($get['status'] == 3){
				# Selesai
				$this->db->where('a.is_done', 1);
				$this->db->where('a.status_berangkat', 1);
			}else{
				# Dibatalkan
				$this->db->where('a.status_berangkat', 2);
			}
			
		}
		$this->db->order_by('tanggal_jalan');
		$result = $this->db->get('ekspedisi a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }

	}

	public function get_data_persetujuan_ekspedisi()
	{
		$get = $this->input->get();
		$this->select("a.*, b.no_pol, c.kota, c.is_sby, d.nama as customer");
		$this->join('kendaraan b', 'a.kendaraan_id = b.id');
		$this->join('area c', 'a.id_area = c.id');
		$this->join('customer d', 'a.customer_id = d.id');
		if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('tanggal_jalan >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('tanggal_jalan <=', $get['end']);  
        }
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                # Menunggu
				$this->db->where('a.is_done', 0);
				$this->db->where('a.status_berangkat', 0);
            }else if($get['status'] == 2){
                # disetujui
				$this->db->where('a.is_done', 0);
				$this->db->where('a.status_berangkat', 1);
            }else if($get['status'] == 3){
				# ditolak
				$this->db->where('a.is_done', 1);
				$this->db->where('a.status_berangkat', 2);
			}
			
		}
		$this->db->order_by('tanggal_jalan');
		$result = $this->db->get('ekspedisi a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }

	}

	public function get_data_biaya(){
		$get = $this->input->get();
		$this->select("a.*, b.no_pol, c.kota, c.is_sby, d.solar, d.timbangan, d.tol, d.polisi, d.parkir, d.masuk_ruko, d.kuli, d.makan, d.pengurus_pelabuhan, d.pak_serap, d.lain, d.upah, ifnull(sum(e.biaya), 0) as total_balen, ifnull(sum(if(e.jenis_pembayaran = 'cash', e.biaya, 0)), 0) as total_balen_cash, (d.solar+d.timbangan+d.tol+d.polisi+d.parkir+d.masuk_ruko+d.kuli+d.makan+d.pengurus_pelabuhan+d.pak_serap+d.lain+d.upah) as total_biaya, f.nama as customer");
		$this->join('kendaraan b', 'a.kendaraan_id = b.id');
		$this->join('area c', 'a.id_area = c.id');
		$this->join('biaya d', 'a.id = d.ekspedisi_id', 'left');
		$this->join('balen e', 'a.id = e.ekspedisi_id', 'left');
		$this->join('customer f', 'a.customer_id = f.id');
		if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('tanggal_jalan >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('tanggal_jalan <=', $get['end']);  
        }
        if(isset($get['kendaraan']) && $get['kendaraan'] != 'all'){
            $this->db->where('a.kendaraan_id', $get['kendaraan']);  
        }
        if(isset($get['supir']) && $get['supir'] != 'all'){
            $this->db->where('a.pegawai_id', $get['supir']);  
        }
		$this->db->where('a.status_berangkat', 1);
		$this->db->where('a.tanggal_cek_berangkat <>', NULL);
		$this->db->where('a.tanggal_cek_pulang <>', NULL);
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                # Menunggu input
				$this->db->where('a.status_biaya', 0);
				$this->db->where('a.is_done', 0);
            }else if($get['status'] == 2){
                # Menunggu persetujuan
				$this->db->where('a.status_biaya', 1);
				$this->db->where('a.is_done', 0);
            }else if($get['status'] == 3){
				# Disetujui
				$this->db->where('a.status_biaya', 1);
				$this->db->where('a.is_done', 1);
			}else{
				# Dibatalkan
				$this->db->where('a.status_biaya', 1);
				$this->db->where('a.is_done', 2);
			}
			
		}
		$this->db->group_by('a.id');
		$this->db->order_by('tanggal_jalan');
		$result = $this->db->get('ekspedisi a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}

	public function get_data_validasi_biaya($id = null){
		$get = $this->input->get();
		$this->select("a.*, b.no_pol, c.kota, c.is_sby, d.solar, d.timbangan, d.tol, d.polisi, d.parkir, d.masuk_ruko, d.kuli, d.makan, d.pengurus_pelabuhan, d.pak_serap, d.lain, d.upah, ifnull(sum(e.biaya), 0) as total_balen, ifnull(sum(if(e.jenis_pembayaran = 'cash', e.biaya, 0)), 0) as total_balen_cash, (d.solar+d.timbangan+d.tol+d.polisi+d.parkir+d.masuk_ruko+d.kuli+d.makan+d.pengurus_pelabuhan+d.pak_serap+d.lain+d.upah) as total_biaya, f.nama as customer");
		$this->join('kendaraan b', 'a.kendaraan_id = b.id');
		$this->join('area c', 'a.id_area = c.id');
		$this->join('customer f', 'a.customer_id = f.id');
		$this->join('biaya d', 'a.id = d.ekspedisi_id', 'left');
		$this->join('balen e', 'a.id = e.ekspedisi_id', 'left');
		if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('tanggal_jalan >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('tanggal_jalan <=', $get['end']);  
        }

		if(!empty($id)){
			$this->db->where('a.id', $id);
		}
        
		$this->db->where('a.status_berangkat', 1);
		$this->db->where('a.status_biaya', 1);
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                # Menunggu input
				$this->db->where('a.is_done', 0);
            }else if($get['status'] == 2){
                # Disetujui
				$this->db->where('a.is_done', 1);
            }else if($get['status'] == 3){
				# Ditolak
				$this->db->where('a.is_done', 2);
			}
			
		}
		$this->db->group_by('a.id');
		$this->db->order_by('tanggal_jalan');
		$result = $this->db->get('ekspedisi a');
        if ($result->num_rows() > 0) {
			if(!empty($id)){
				return $result->row_array();
			}else{
				return $result->result_array();
			}
        } else {
            return false;
        }
	}

	public function get_rekap_ekspedisi()
	{
		$get = $this->input->get();
		$this->select("a.*, b.no_pol, c.kota, c.is_sby, d.nama as customer, ifnull(sum(f.biaya), 0) as total_balen, ifnull(sum(if(f.jenis_pembayaran = 'cash', f.biaya, 0)), 0) as total_balen_cash, (e.solar+e.timbangan+e.tol+e.polisi+e.parkir+e.masuk_ruko+e.kuli+e.makan+e.pengurus_pelabuhan+e.pak_serap+e.lain+e.upah) as total_biaya, e.upah, g.km as km_awal, h.km as km_akhir, (h.km - g.km) as km_total");
		$this->join('kendaraan b', 'a.kendaraan_id = b.id');
		$this->join('area c', 'a.id_area = c.id');
		$this->join('customer d', 'a.customer_id = d.id');
		$this->join('biaya e', 'a.id = e.ekspedisi_id', 'left');
		$this->join('balen f', 'a.id = f.ekspedisi_id', 'left');
		$this->join('kelengkapan g', 'a.id = g.ekspedisi_id and g.jenis_cek = "berangkat"');
		$this->join('kelengkapan h', 'a.id = h.ekspedisi_id and h.jenis_cek = "pulang"');
		if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('tanggal_jalan >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('tanggal_jalan <=', $get['end']);  
        }
        if(isset($get['kendaraan']) && $get['kendaraan'] != 'all'){
            $this->db->where('a.kendaraan_id', $get['kendaraan']);  
        }
        if(isset($get['supir']) && $get['supir'] != 'all'){
            $this->db->where('a.pegawai_id', $get['supir']);  
        }
		if(isset($get['customer']) && $get['customer'] != 'all'){
            $this->db->where('a.customer_id', $get['customer']);  
        }

        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                # Menunggu
				$this->db->where('a.is_done', 0);
				$this->db->where('a.status_berangkat', 0);
            }else if($get['status'] == 2){
                # Berlangsung
				$this->db->where('a.is_done', 0);
				$this->db->where('a.status_berangkat', 1);
            }else if($get['status'] == 3){
				# Selesai
				$this->db->where('a.is_done', 1);
				$this->db->where('a.status_berangkat', 1);
			}else{
				# Dibatalkan
				$this->db->where('a.status_berangkat', 2);
			}
			
		}
		$this->db->group_by('a.id');
		$this->db->order_by('tanggal_jalan');
		$result = $this->db->get('ekspedisi a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }

	}


	public function update_muatan($param)
	{
		$this->db->set(array('berat_muatan' => $param['muatan']));
		$this->db->where('no_resi', $param['resi']);
		return $this->db->update('ekspedisi');
	}

	public function update_status_muatan($resi, $status)
	{
		$this->db->set(array('status_muatan' => $status));
		$this->db->where('no_resi', $resi);
		return $this->db->update('ekspedisi');
	}
}
