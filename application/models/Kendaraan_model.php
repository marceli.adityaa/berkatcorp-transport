<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kendaraan_model extends MY_Model
{
	public $_table = 'kendaraan';
	private $kolom = array('no_pol');

	public function __construct()
	{
		parent::__construct();
	}

	public function get_data($filter = array())
	{
		# LIMIT, OFFSET, AND SORT
		$limit  = isset($filter['limit']) ? $filter['limit'] : '';
		$offset = isset($filter['offset']) ? $filter['offset'] : '';
		$sort   = isset($filter['sort']) ? $filter['sort'] : 'no_pol';
		$order  = isset($filter['order']) ? $filter['order'] : '';

		# SELECT

		# WHERE
		$where['id'] = isset($filter['id']) ? $filter['id'] : '';

		# JOIN


		# EXCLUDE
		$excludes = isset($filter['excludes']) ? $filter['excludes'] : array();

		# UNSET FILTER
		$filter = $this->unsetFilter($this->setFilter($filter));
		$where  = trim_array($where);

		# SET LIKE
		if (count($filter) > 0) {
			$this->group_start();
			$this->or_like($filter);
			$this->group_end();
		}

		# SET WHERE NOT IN
		foreach ($excludes as $key => $value) {
			if (!empty($value)) {
				$this->where_not_in($key, $value);
			}
		}

		# Set total
		$clone = clone ($this->db);
		$results['total'] = $clone->where($where)->from($this->_table)->count_all_results();

		if (!empty($limit) or !empty($offset)) {
			$this->limit($limit, $offset);
		}

		# SET SORT
		if (!empty($sort)) {
			$this->order_by($sort, $order);
		}

		$results['rows'] = $this->get_many_by($where);
		return isset($results['total']) ? $results : $results['rows'];
	}

	private function setFilter($filter = array())
	{
		if (isset($filter['search'])) {
			foreach ($this->kolom as $k) {
				$filter[$k] = $filter['search'];
			}
		}

		return $filter;
	}

	private function unsetFilter($filter)
	{
		unset($filter['limit'], $filter['offset'], $filter['order'], $filter['sort']);
		unset($filter['id'], $filter['search'], $filter['excludes']);
		return trim_array($filter);
	}

	public function getAvailableVehicle()
	{
		$this->load->model('Ekspedisi_model', 'ekspedisi');
		$isUsed = $this->ekspedisi->select("kendaraan_id")->get_many_by(['is_done' => '0']);
		$isUsed = array_column($isUsed, 'kendaraan_id');
		return $this->get_data(['excludes' => ['id' => $isUsed]]);
	}

	public function get_active(){
		$this->db->where('is_disabled', 0);
		$this->db->where('is_deleted', 0);
		$result = $this->db->get('kendaraan');
		if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}

	public function get_data_kendaraan(){
		$get = $this->input->get();

		// if(isset($get['start']) && $get['start'] != ''){
        //     $this->db->where('tanggal_jalan >=', $get['start']);
        // }
        // if(isset($get['end']) && $get['end'] != ''){
        //     $this->db->where('tanggal_jalan <=', $get['end']);  
        // }
        
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                # Aktif
				$this->db->where('is_disabled', 0);
            }else if($get['status'] == 2){
                # Nonaktif
				$this->db->where('is_disabled', 1);
            }
			
		}
		$this->db->order_by('merk');
		$this->db->order_by('tipe');
		$result = $this->db->get('kendaraan');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}


}
