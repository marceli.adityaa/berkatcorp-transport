<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Area_model extends MY_Model {
	public $_table = 'area';
	private $kolom = array('kota');

	public function __construct()
	{
		parent::__construct();
	}

	public function get_data($filter = array())
	{
		# LIMIT, OFFSET, AND SORT
		$limit  = isset($filter['limit'])?$filter['limit']:'';
		$offset = isset($filter['offset'])?$filter['offset']:'';
		$sort   = isset($filter['sort'])?$filter['sort']:'kota';
		$order  = isset($filter['order'])?$filter['order']:'';

		# SELECT

		# WHERE
        $where['id'] = isset($filter['id'])?$filter['id']:'';
        $where['is_disabled'] = isset($filter['is_disabled'])?$filter['is_disabled']:'';

		# JOIN
		

		# EXCLUDE
		$excludes = isset($filter['excludes'])?$filter['excludes']:array();

		# UNSET FILTER
		$filter = $this->unsetFilter($this->setFilter($filter));
		$where  = trim_array($where);

		# SET LIKE
		if (count($filter) > 0) {
			$this->group_start();
			$this->or_like($filter);
			$this->group_end();
		}

		# SET WHERE NOT IN
		if (count($excludes) > 0) {
			foreach ($excludes as $key => $value) {
				$this->where_not_in($key, $value);
			}
		}

		# Set total
		$clone = clone($this->db);
		$results['total'] = $clone->where($where)->from($this->_table)->count_all_results();

		if (!empty($limit) OR !empty($offset)) {
			$this->limit($limit, $offset);
		}

		# SET SORT
		if (!empty($sort)) {
			$this->order_by($sort, $order);
		}

		$results['rows'] = $this->get_many_by($where);
		return isset($results['total'])?$results:$results['rows'];
	}

	private function setFilter($filter = array())
	{
		if (isset($filter['search'])) {
			foreach ($this->kolom as $k) {
				$filter[$k] = $filter['search'];
			}
		}

		return $filter;
	}

	private function unsetFilter($filter)
	{
		unset($filter['limit'],$filter['offset'], $filter['order'], $filter['sort']);
		unset($filter['id'], $filter['search'], $filter['excludes'], $filter['is_disabled']);
		return trim_array($filter);
	}
}