<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Balen_model extends MY_Model {
	public $_table = 'balen';

	public function __construct()
	{
		parent::__construct();
    }

	public function get_data_balen($ekspedisi){
		$this->db->select("a.*, b.nama as customer, c.kota");
		$this->db->where('a.ekspedisi_id', $ekspedisi);
		$this->db->join('customer b', 'a.customer_id = b.id');
		$this->db->join('area c', 'a.id_area = c.id');
		$result = $this->db->get('balen a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}

	public function get_data_balen_param(){
		$get = $this->input->get();
		$this->select("a.*, c.kota");
		$this->join('ekspedisi a', 'b.ekspedisi_id = a.id');
		$this->join('area c', 'b.id_area = c.id');
		if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('a.tanggal_jalan >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('a.tanggal_jalan <=', $get['end']);  
        }
        if(isset($get['kendaraan']) && $get['kendaraan'] != 'all'){
            $this->db->where('a.kendaraan_id', $get['kendaraan']);  
        }
        if(isset($get['supir']) && $get['supir'] != 'all'){
            $this->db->where('a.pegawai_id', $get['supir']);  
        }
		$this->db->where('a.status_berangkat', 1);
		$this->db->where('a.tanggal_cek_berangkat <>', NULL);
		$this->db->where('a.tanggal_cek_pulang <>', NULL);
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                # Menunggu input
				$this->db->where('a.status_biaya', 0);
				$this->db->where('a.is_done', 0);
            }else if($get['status'] == 2){
                # Menunggu persetujuan
				$this->db->where('a.status_biaya', 1);
				$this->db->where('a.is_done', 0);
            }else if($get['status'] == 3){
				# Disetujui
				$this->db->where('a.status_biaya', 1);
				$this->db->where('a.is_done', 1);
			}else{
				# Dibatalkan
				$this->db->where('a.status_biaya', 1);
				$this->db->where('a.is_done', 2);
			}
			
		}
		$this->db->order_by('tanggal_jalan');
		$result = $this->db->get('balen b');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
	}
}