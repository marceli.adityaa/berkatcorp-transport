<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

defined('BASEPATH') or exit('No direct script access allowed');

class Biaya_model extends MY_Model
{
	public $_table = 'biaya';
	private $kolom = array('');

	public function __construct()
	{
		parent::__construct();
	}

	public function get_data($filter = array())
	{
		# LIMIT, OFFSET, AND SORT
		$limit  = isset($filter['limit']) ? $filter['limit'] : '';
		$offset = isset($filter['offset']) ? $filter['offset'] : '';
		$sort   = isset($filter['sort']) ? $filter['sort'] : 'tanggal_validasi_biaya';
		$order  = isset($filter['order']) ? $filter['order'] : '';

		# SELECT
		$this->select("biaya.*, ekspedisi.*, kendaraan.no_pol");

		# WHERE
		$where = array();

		# CUSTOM WHERE
		if (isset($filter['custom_where'])) {
			$where = array_merge($where, $filter['custom_where']);
		}

		# JOIN
		$this->join('ekspedisi', 'ekspedisi_id = ekspedisi.id');
		$this->join('kendaraan', 'kendaraan_id = kendaraan.id');

		# EXCLUDE
		$excludes = isset($filter['excludes']) ? $filter['excludes'] : array();

		# UNSET FILTER
		$filter = $this->unsetFilter($this->setFilter($filter));
		$where  = trim_array($where);

		# SET LIKE
		if (count($filter) > 0) {
			$this->group_start();
			$this->or_like($filter);
			$this->group_end();
		}

		# SET WHERE NOT IN
		if (count($excludes) > 0) {
			foreach ($excludes as $key => $value) {
				$this->where_not_in($key, $value);
			}
		}

		# Set total
		$clone = clone ($this->db);
		$results['total'] = $clone->where($where)->from($this->_table)->count_all_results();

		if (!empty($limit) or !empty($offset)) {
			$this->limit($limit, $offset);
		}

		# SET SORT
		if (!empty($sort)) {
			$this->order_by($sort, $order);
		}

		$results['rows'] = $this->get_many_by($where);
		return isset($results['total']) ? $results : $results['rows'];
	}

	private function setFilter($filter = array())
	{
		if (isset($filter['search'])) {
			foreach ($this->kolom as $k) {
				$filter[$k] = $filter['search'];
			}
		}

		return $filter;
	}

	private function unsetFilter($filter)
	{
		unset($filter['limit'], $filter['offset'], $filter['order'], $filter['sort']);
		unset($filter['id'], $filter['search'], $filter['excludes'], $filter['custom_where']);
		unset($filter['is_done'], $filter['pegawai_id']);
		return trim_array($filter);
	}

	public function getWidget()
	{
		$this->load->model('Ekspedisi_model', 'ekspedisi');
		$widget['menunggu'] = $this->ekspedisi->count_by(['tanggal_input_biaya!=' => null, 'tanggal_validasi_biaya' => null]);

		# Pendapatan tahun ini
		$widget['annual'] = 0;
		$current = $this->join('ekspedisi', 'ekspedisi_id=ekspedisi.id')->where("YEAR(tanggal_jalan) = YEAR(CURDATE()) AND tanggal_validasi_biaya IS NOT NULL")->get_all();
		foreach ($current as $d) {
			$widget['annual'] += ($d->biaya + $d->balen) - ($d->solar + $d->timbangan + $d->tol + $d->polisi + $d->parkir + $d->masuk_ruko + $d->kuli + $d->makan + $d->pengurus_pelabuhan + $d->pak_serap + $d->lain + $d->upah);
		}

		# Pendapatan Bulan Ini
		$widget['current'] = 0;
		$current = $this->join('ekspedisi', 'ekspedisi_id=ekspedisi.id')->where("YEAR(tanggal_jalan) = YEAR(CURDATE()) AND MONTH(tanggal_jalan) = MONTH(CURDATE()) AND tanggal_validasi_biaya IS NOT NULL")->get_all();
		foreach ($current as $d) {
			$widget['current'] += ($d->biaya + $d->balen) - ($d->solar + $d->timbangan + $d->tol + $d->polisi + $d->parkir + $d->masuk_ruko + $d->kuli + $d->makan + $d->pengurus_pelabuhan + $d->pak_serap + $d->lain + $d->upah);
		}

		# Pendapatan Bulan Ini
		$widget['last'] = 0;
		$last = $this->join('ekspedisi', 'ekspedisi_id=ekspedisi.id')->where("YEAR(tanggal_jalan) = YEAR(CURDATE() - INTERVAL 1 MONTH) AND MONTH(tanggal_jalan) = MONTH(CURDATE() - INTERVAL 1 MONTH) AND tanggal_validasi_biaya IS NOT NULL")->get_all();
		foreach ($last as $d) {
			$widget['last'] += ($d->biaya + $d->balen) - ($d->solar + $d->timbangan + $d->tol + $d->polisi + $d->parkir + $d->masuk_ruko + $d->kuli + $d->makan + $d->pengurus_pelabuhan + $d->pak_serap + $d->lain + $d->upah);
		}

		# Persentase
		$percenDiff = 100;
		if ($widget['current'] > 0 && $widget['last'] > 0) {
			$percenDiff = (abs($widget['current'] - $widget['last']) / $widget['current']) * 100;
		}
		$widget['persentase'] = floor($percenDiff);

		return $widget;
	}

	public function getGraphAnnual()
	{
		$result = [];

		for ($i = 1; $i <= 12; $i++) {
			$date = date('Y').'-'.$i.'-1';
			$total = 0;
			$data = $this->join('ekspedisi', 'ekspedisi_id=ekspedisi.id')->where("YEAR(tanggal_jalan) = YEAR('{$date}') AND MONTH(tanggal_jalan) = MONTH('{$date}') AND tanggal_validasi_biaya IS NOT NULL")->get_all();
			foreach ($data as $d) {
				$total += ($d->biaya + $d->balen) - ($d->solar + $d->timbangan + $d->tol + $d->polisi + $d->parkir + $d->masuk_ruko + $d->kuli + $d->makan + $d->pengurus_pelabuhan + $d->pak_serap + $d->lain + $d->upah);
			}
			array_push($result, number_format($total / 1000000, 2,'.',''));
		}
		return $result;
	}

	public function cetak($tahun, $bulan)
	{
		# Ambil data
		$this->load->model('Pegawai_model', 'pegawai');

		$filter['custom_where'] = ['YEAR(tanggal_jalan)' => $tahun, 'MONTH(tanggal_jalan)' => $bulan, 'tanggal_validasi_biaya!=' => 'null'];
		$filter['order'] = 'tanggal_jalan';
		$data = $this->get_data($filter);
		$data['rows'] = $this->pegawai->join($data['rows']);

		# INIT
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$kolom  = 1;
		$baris = 2;

		# HEADER
		$header = ['NO RESI', 'TANGGAL JALAN', 'NO POL', 'SOPIR', 'ONGKOS BERANGKAT', 'ONGKOS BALEN', 'SOLAR', 'TIMBANGAN', 'TOL', 'POLISI', 'PARKIR', 'MASUK RUKO', 'KULI', 'MAKAN', 'PELABUHAN', 'P.SERAP', 'LAIN-LAIN', 'FEE SOPIR', 'TOTAL BIAYA', 'LABA/RUGI'];
		$sheet->fromArray($header, null, 'A1');

		foreach ($data['rows'] as $d) {
			$kolom = 1;
			$sheet->setCellValueByColumnAndRow($kolom++, $baris, $d->no_resi);
			$sheet->setCellValueByColumnAndRow($kolom++, $baris, datify($d->tanggal_jalan, 'd/m/y'));
			$sheet->setCellValueByColumnAndRow($kolom++, $baris, $d->no_pol);
			$sheet->setCellValueByColumnAndRow($kolom++, $baris, $d->pegawai_nama);
			$sheet->setCellValueByColumnAndRow($kolom++, $baris, monefy($d->biaya));
			$sheet->setCellValueByColumnAndRow($kolom++, $baris, monefy($d->balen));
			$sheet->setCellValueByColumnAndRow($kolom++, $baris, monefy($d->solar));
			$sheet->setCellValueByColumnAndRow($kolom++, $baris, monefy($d->timbangan));
			$sheet->setCellValueByColumnAndRow($kolom++, $baris, monefy($d->tol));
			$sheet->setCellValueByColumnAndRow($kolom++, $baris, monefy($d->polisi));
			$sheet->setCellValueByColumnAndRow($kolom++, $baris, monefy($d->parkir));
			$sheet->setCellValueByColumnAndRow($kolom++, $baris, monefy($d->masuk_ruko));
			$sheet->setCellValueByColumnAndRow($kolom++, $baris, monefy($d->kuli));
			$sheet->setCellValueByColumnAndRow($kolom++, $baris, monefy($d->makan));
			$sheet->setCellValueByColumnAndRow($kolom++, $baris, monefy($d->pengurus_pelabuhan));
			$sheet->setCellValueByColumnAndRow($kolom++, $baris, monefy($d->pak_serap));
			$sheet->setCellValueByColumnAndRow($kolom++, $baris, monefy($d->lain));
			$sheet->setCellValueByColumnAndRow($kolom++, $baris, monefy($d->upah));
			# Hitung jumlah
			$total = $d->solar + $d->timbangan + $d->tol + $d->polisi + $d->parkir + $d->masuk_ruko + $d->kuli + $d->makan + $d->pengurus_pelabuhan + $d->pak_serap + $d->lain + $d->upah;
			$sheet->setCellValueByColumnAndRow($kolom++, $baris, monefy($total));
			$sheet->setCellValueByColumnAndRow($kolom++, $baris, monefy(($d->biaya + $d->balen) - $total));
			$baris++;
		}

		# STYLING
		$lastCell = $sheet->getHighestColumn() . '' . $sheet->getHighestRow();
		$sheet->getStyle('A1:' . $lastCell)->getAlignment()->setHorizontal('center')->setVertical('center');
		$sheet->getColumnDimension('B')->setAutoSize(true);

		# SAVE
		$writer = new Xlsx($spreadsheet);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="laporan_transportasi.xlsx"');
		$writer->save("php://output");
	}

}
