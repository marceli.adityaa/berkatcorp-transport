<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Hutang_detail_model extends MY_Model
{
    public $_table = 'hutang_detail';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_data($id = null)
    {
        $this->db->select('a.id_customer, b.*');
        if(!empty($id)){
            $this->db->where('b.id', $id);
        }
        $this->db->join('hutang_detail b', 'b.id_hutang = a.id');
        $result = $this->db->get('hutang a');
        if ($result->num_rows() > 0) {
            if(!empty($id)){
                return $result->row_array();
            }else{
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    public function get_data_hutang_verified($hutang, $id = null)
    {
        $this->db->select('c.nama as kreditur, b.*');   
        $this->db->where('a.id', $hutang);
        if(!empty($id)){
            $this->db->where('b.id <>', $id);
        }
        $this->db->where('b.is_verifikasi', 1);
        $this->db->join('hutang_detail b', 'a.id = b.id_hutang');
        $this->db->join('customer c', 'a.id_customer = c.id');
        $result = $this->db->get('hutang a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_hutang_detail($hutang)
    {
        $this->db->select('c.nama as kreditur, b.*');   
        $this->db->where('a.id', $hutang);
        $this->db->join('hutang_detail b', 'a.id = b.id_hutang');
        $this->db->join('customer c', 'a.id_customer = c.id');
        $this->db->order_by('b.tgl_transaksi');
        $result = $this->db->get('hutang a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_rekap_hutang_detail($hutang)
    {
        $this->db->select('c.nama as kreditur, b.*');   
        $this->db->where('a.id', $hutang);
        $this->db->where('b.is_verifikasi', 1);
        $this->db->join('hutang_detail b', 'a.id = b.id_hutang');
        $this->db->join('customer c', 'a.id_customer = c.id');
        $this->db->order_by('b.tgl_transaksi');
        $result = $this->db->get('hutang a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_persetujuan_hutang()
    {
        $get = $this->input->get();
        $this->db->select('c.nama as kreditur, b.*');   
        if(isset($get['kreditur']) && $get['kreditur'] != 'all'){
            $this->db->where('a.id_customer', $get['kreditur']);
        }
        if(isset($get['start']) && $get['start'] != ''){
            $this->db->where('b.tgl_transaksi >=', $get['start']);
        }
        if(isset($get['end']) && $get['end'] != ''){
            $this->db->where('b.tgl_transaksi <=', $get['end']);
        }
        if(isset($get['status']) && $get['status'] != 'all'){
            if($get['status'] == 1){
                $this->db->where('b.is_verifikasi', 0);
            }else if($get['status'] == 2){
                $this->db->where('b.is_verifikasi', 1);
            }else if($get['status'] == 3){
                $this->db->where('b.is_verifikasi', 2);
            }
        }
        $this->db->join('hutang_detail b', 'a.id = b.id_hutang');
        $this->db->join('customer c', 'a.id_customer = c.id');
        $result = $this->db->get('hutang a');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function cek_data_validasi($id)
    {
        $this->db->where('is_verifikasi', 1);
        $this->db->where('id_hutang', $id);
        $result = $this->db->get('hutang_detail');
        if ($result->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function delete_by_transaksi($id)
    {
        $this->db->where('id_hutang', $id);
        return $this->db->delete('hutang_detail');
    }
}
