<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pegawai_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get($id = '')
    {
        $url = $this->config->item('master_address') . "/api/external/pegawai/get/" . $this->session->auth['token'] . "?id=" . $id;
        $result = xcurl(array(CURLOPT_URL => $url));

        if (!$result['err']) {
            $result = (array) json_decode($result['response']);
        }

        return $result;
    }

    public function get_many($filter = array())
    {
        $result = ['total' => 0, 'rows' => []];
        $filter['is_disabled'] = 0;
        $param = http_build_query($filter);

        $url = $this->config->item('master_address') . "/api/external/pegawai/get_many/" . $this->session->auth['token'] . "?" . $param;
        $result = xcurl(array(CURLOPT_URL => $url));

        if (!$result['err']) {
            $result = (array) json_decode($result['response']);
        }

        return $result;
    }

    public function join($data)
    {
        $pegawai = $this->format($this->get_many()['rows']);
        if (is_array($data)) {
            foreach ($data as $index => $value) {
                if (isset($value->pegawai_id)) {
                    $data[$index]->pegawai_nama = $pegawai[$value->pegawai_id];
                }
            }
        } else {
            if (isset($data->pegawai_id)) {
                $data->pegawai_nama = $pegawai[$data->pegawai_id];
            }
        }
        return $data;
    }

    public function joins($data = [], $name, $target)
    {
        $user = $this->format($this->get_many()['rows']);
        if (!empty($data)) {
            if (array_key_exists(0, $data)){
                foreach ($data as $index => $value) {
                    if (isset($value[$target]) && $value[$target] != 0) {
                        $data[$index][$name] = $user[$value[$target]];
                    }
                }
            }else{
                $data[$name] = $user[$data[$target]];
            }
        }
        return $data;
    }

    private function format($data = array())
    {
        $result = array();
        foreach ($data as $d) {
            $result[$d->id] = $d->nama;
        }
        return $result;
    }

    public function getAvailableDriver()
    {
        $this->load->model('Ekspedisi_model', 'ekspedisi');

        $data = $this->get();
        $isUsed = $this->ekspedisi->select("pegawai_id")->get_many_by(['is_done' => '0']);
        $isUsed = array_column($isUsed, 'pegawai_id');
        foreach ($data['rows'] as $index => $value) {
            if (in_array($value->id, $isUsed)) {
                unset($data['rows'][$index]);
            }
        }
        return $data;
    }

    public function get_active_driver(){
        $url = $this->config->item('master_address') . "/api/external/pegawai/get_active_driver/" . $this->session->auth['token'];
        $result = xcurl(array(CURLOPT_URL => $url));
        if (!$result['err']) {
            $result = (array) json_decode($result['response']);
        }
        return $result;
    }
}
