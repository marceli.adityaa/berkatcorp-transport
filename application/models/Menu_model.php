<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu_model extends MY_Model
{
	public $_table = 'menu';
	public $appid = 'tran';

	public function __construct()
	{
		parent::__construct();
		$this->id_perusahaan = $this->config->item('id_perusahaan');
		$this->url_master = $this->config->item('url_master');
	}

	public function get_data()
	{ 
		$mParent = $this->order_by('urutan')->get_many_by(array('child_of' => 0));
		foreach ($mParent as $mp) {
			$mChild = $this->order_by('urutan')->get_many_by(array('child_of' => $mp->id));
			if ($mChild) {
				$mp->child = $mChild;
			}
		}
		return $mParent;
	}

	public function get_active(){
		$this->where('is_deleted', 0);
		$mParent = $this->order_by('urutan')->get_many_by(array('child_of' => 0));
		foreach ($mParent as $mp) {
			$mChild = $this->order_by('urutan')->get_many_by(array('child_of' => $mp->id));
			if ($mChild) {
				$mp->child = $mChild;
			}
		}
		return $mParent;
	}

	public function get_sidebar_menu()
	{	
		$sess = json_decode($this->session->auth['hak_akses'])->{$this->app_id};
		$menu = $this->order_by('urutan')->get_many_by(array('child_of'=>0, 'is_deleted' => 0));
		if($sess[0] != "*"){
			$akses = $sess;
		}else{
			$akses = array();
			$all = $this->order_by('urutan')->as_array()->get_all();
			foreach($all as $row){
				array_push($akses, $row['uuid']);
			}
		}
		# Generate menu
		foreach ($menu as $index=>$m) {
			if(!empty($m->query_badge)){
				$badge = $this->db->query($m->query_badge)->row_array()['total'];
				$menu[$index]->badge = $badge;
			}
			if (!in_array($m->uuid, $akses)) {
				unset($menu[$index]);
				continue;
			}
			
			$child = $this->order_by('urutan')->get_many_by(array('child_of'=>$m->id, 'is_deleted' => 0));
			if ($child) {
				$m->child = $child;
				foreach ($m->child as $key=>$c) {
					if (!in_array($c->uuid, $akses)) {
						unset($m->child[$key]);
						continue;
					}
					if(!empty($c->query_badge)){
						if($c->is_curl == 1){
							$param_curl = array(CURLOPT_URL => $this->url_master.$c->query_badge.'?token='.$this->session->auth['token'].'&perusahaan='.$this->id_perusahaan);
							$m->child[$key]->badge = json_decode(xcurl($param_curl)['response'])->total;
						}else{
							$badge = $this->db->query($c->query_badge)->row_array()['total'];
							$m->child[$key]->badge = $badge;
						}
						if($m->child[$key]->badge > 0){
							$menu[$index]->is_show = 1;
						}
					}
				}
			}
		}
		return $menu;
	}

	/*
	public function get_sidebar_menu()
	{
		$akses = json_decode($this->session->auth['hak_akses']);
		# Generate menu
		if (!isset($akses->{$this->appid}))
			return new stdClass();

		$akses = $akses->{$this->appid};
		$mParent = $this->where_in('uuid', $akses)->order_by('urutan')->get_many_by(array('is_disabled' => 0, 'child_of' => 0));
		foreach ($mParent as $mp) {
			$mChild = $this->where_in('uuid', $akses)->order_by('urutan')->get_many_by(array('is_disabled' => 0, 'child_of' => $mp->id));
			if ($mChild) {
				$mp->child = $mChild;
			}
		}
		return $mParent;
	}
	*/
}
