<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends MY_Controller {

	protected $ssoServerAddress;
	protected $defaultPage;
	protected $url;
	
	public function __construct()
	{
		parent::__construct();
		$this->ssoServerAddress = $this->config->item('sso_address');
		$this->defaultPage = site_url('dashboard');
	}

	public function index()
	{
		if ($this->hasAuth()) {
			$this->go($this->defaultPage, true);
		} else {
			$this->auth();
		}
	}

	private function auth()
	{
		$this->url = generate_save_url($this->input->get('source'));
		$this->url = !empty($this->url) ? $this->url : $this->defaultPage;

		if (!$this->validateSource()) {
			show_error('Sumber alamat tidak valid', 404);
		}

		$this->go($this->ssoServerAddress.'/signin?source='.urlencode($this->url), true, 'refresh');
	}

	public function deauth()
	{
		$this->session->sess_destroy();
		$this->auth();
	}

	public function acc_req()
	{
		$token = $this->input->get('token');
		$url = $this->input->get('source');
		$url = !empty($url) ? $url : $this->defaultPage;

		$c = file_get_contents($this->ssoServerAddress.'/get_credential?token='.$token.'&source='.urlencode($url));
		$c = (array)json_decode($c);

		if (in_array('error', $c)) {
			show_error($c['error']);
		}

		$this->storeAuth($c);
		$this->go($url, true);
	}

	private function validateSource()
	{
		$p = parse_url($this->url);
		$u = parse_url(site_url());
		return empty($this->url) OR (isset($p['host']) && isset($u['host']) && $p['host'] == $u['host']);
	}

	private function hasAuth()
	{
		return $this->session->userdata('auth');
	}

	private function storeAuth($c = array())
	{
		if($this->session->auth)
			$this->session->unset_userdata('auth');
		$this->session->set_userdata('auth', $c);
	}
}