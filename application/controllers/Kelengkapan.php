<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kelengkapan extends MY_Controller_admin
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Ekspedisi_model', 'ekspedisi');
		$this->load->model('Kelengkapan_model', 'kelengkapan');
	}

	public function index()
	{
		$data['berangkat'] = $this->ekspedisi->getEkspedisiBerangkat();
        $data['pulang'] = $this->ekspedisi->getEkspedisiPulang();
        set_session('title', 'Kelengkapan');
        set_activemenu('sub-ekspedisi', 'menu-kelengkapan');
        $this->render('ekspedisi/kelengkapan/index', $data);
    }
    
    public function form($ekspedisi_id = '', $jenis_cek = '')
    {
        if (empty($ekspedisi_id) || empty($jenis_cek)) {
            show_404();
        }
        # Ambil data ekspedisi
        $data['ekspedisi'] = $this->ekspedisi->get_data(['id' => $ekspedisi_id]);
        $data['ekspedisi'] = $this->pegawai->join($data['ekspedisi']['rows'])[0];
        # Ambil jenis
        $data['jenis_cek'] = $jenis_cek;
        set_session('title', 'Form Kelengkapan');
        set_activemenu('sub-ekspedisi', 'menu-kelengkapan');
        $this->render('ekspedisi/kelengkapan/form', $data);
    }

    public function save($token = '')
    {
        if (!$this->validateToken($token)) {
            show_404();
        }

        # Ambil data
        $data = $this->input->post();
        $varJenis = 'tanggal_cek_' . $data['jenis_cek'];

        if (empty($data['id'])) {
            # Insert kelengkapan
            $this->kelengkapan->insert($data);
            # Update timestamp
            $this->ekspedisi->update($data['ekspedisi_id'], [$varJenis => date('Y-m-d H:i:s')]);
        } else {
            # Update kelengkapan
        }

        #redirect
        $this->go('kelengkapan');
    }
}