<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Biaya_kendaraan extends MY_Controller_admin
{
	public $bypass_auth = true;

	public function __construct()
	{
		parent::__construct();
        $this->load->model('master_biaya_kendaraan_model', 'master_biaya_kendaraan');
	}
    
    public function index()
    {
		$data['status'] = array(1 => 'Aktif', 2 => 'Nonaktif');
		$data['list']   = $this->master_biaya_kendaraan->get_data();
		set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Master Data' => 'active', 'Biaya Kendaraan' => 'active'));
        set_session('title', 'Master Data - Biaya Kendaraan');
        set_activemenu('sub-master', 'menu-master-biaya-kendaraan');
		$this->render('master/v-biaya-kendaraan', $data);
    }

	public function submit_form(){
		$post = $this->input->post();
		if(!$post['id']){
			# Insert Statement
			$result = $this->master_biaya_kendaraan->insert($post);
			if($result){
				$this->message('Sukses memasukkan data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}else{
			# Update Statement
			$id = $post['id'];
			unset($post['id']);
			$result = $this->master_biaya_kendaraan->update($id, $post);
			if($result){
				$this->message('Sukses mengubah data', 'success');
			}else{
				$this->message('Gagal', 'error');
			}
		}
		$this->go('master/biaya_kendaraan');
	}
    
    public function json_get_detail(){
		$id = $this->input->post('id');
		$response = $this->master_biaya_kendaraan->as_array()->get($id);
		echo json_encode($response);
	}

	public function set_aktif(){
		$id = $this->input->post('id');
		$response = $this->master_biaya_kendaraan->update($id, array('status' => 1));
		echo json_encode($response);
	}

	public function set_nonaktif(){
		$id = $this->input->post('id');
		$response = $this->master_biaya_kendaraan->update($id, array('status' => 0));
		echo json_encode($response);
    }
}
