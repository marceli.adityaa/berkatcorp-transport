<?php
defined('BASEPATH') or exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Tagihan extends MY_Controller_admin
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Ekspedisi_model', 'ekspedisi');
        $this->load->model('Kelengkapan_model', 'kelengkapan');
        $this->load->model('Biaya_model', 'biaya');
        $this->load->model('Kendaraan_model', 'kendaraan');
        $this->load->model('Pegawai_model', 'pegawai');
        $this->load->model('Area_model', 'area');
        $this->load->model('Balen_model', 'balen');
        $this->load->model('Kas_model', 'kas');
        $this->load->model('Customer_model', 'customer');
        $this->load->model('Tagihan_model', 'tagihan');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
	}

	public function index()
	{
		if(empty($_GET['status'])){
            $_GET['status'] = 1;   
        }
        $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
        $param_rek_penerima = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
        $data['kas']        = json_decode(xcurl($param_kas)['response']);
        $data['rek_penerima'] = json_decode(xcurl($param_rek_penerima)['response']);
        $data['tagihan'] = $this->kas->join($this->pegawai->joins($this->tagihan->get_data(), 'supir', 'pegawai_id'), 'kas', 'id_kas');
        $data['kendaraan'] = $this->kendaraan->get_active();
        $data['status'] = array(1 => 'Menunggu', 2 => 'Lunas', 3 => 'Ditolak');
        $data['pembayaran'] = array('cash', 'transfer');
        $data['area'] = $this->area->order_by('kota')->get_all();
        $data['supir'] = $this->pegawai->get_active_driver()['rows'];

        set_session('title', 'Tagihan');
        set_activemenu('sub-ekspedisi', 'menu-tagihan');
        $this->render('ekspedisi/tagihan/index', $data);
    }

    public function detail($id){
        $param_rek_penerima = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
        $data['rek_penerima'] = json_decode(xcurl($param_rek_penerima)['response']);
        $data['pembayaran'] = array('cash', 'transfer');
        $data['ekspedisi'] = $this->ekspedisi->select("ekspedisi.*, no_pol, kota, is_sby, customer.nama as customer")->join("kendaraan", "kendaraan_id=kendaraan.id")->join("area", "area.id = id_area")->join('customer', 'customer_id = customer.id')->get_by(['ekspedisi.id' => $id]);
        $data['biaya'] = $this->biaya->get_by(['ekspedisi_id' => $id]);
        $data['balen'] = $this->kas->join($this->balen->get_data_balen($id), 'kas', 'transfer_rekening');
        $data['area'] = $this->area->order_by('kota')->get_all();
        $data['customer'] = $this->customer->get_active();
        $data['id'] = $id;
        set_session('title', 'Form Biaya');
        set_activemenu('sub-ekspedisi', 'menu-tagihan');
        $this->render('ekspedisi/tagihan/detail', $data);
    }

    public function cetak_laporan_biaya()
    {
        $tahun = $this->input->get('tahun');
        $bulan = $this->input->get('bulan');
        $this->biaya->cetak($tahun, $bulan);
    }
}