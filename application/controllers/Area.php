<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Area extends MY_Controller_admin
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Area_model', 'area');
    }

    public function index()
    {
    	set_session('title', 'Master Area');
        set_activemenu('sub-master', 'menu-master-area');
        $this->render('area/index');
    }
}
