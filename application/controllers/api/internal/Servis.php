<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Servis extends MY_Controller_api
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Servis_model', 'model');
		$this->load->model('Pegawai_model', 'pegawai');
	}

	public function callback_table($data = [])
	{
		$data['rows'] = $this->pegawai->join($data['rows']);

		foreach ($data['rows'] as $k => $v) {
			if (isset($v->insert_time))
				$v->insert_time = time_ago($v->update_time);
			# Aksi=========================================
			$v->aksi  = "<a href='".(site_url('servis/edit/'.$v->id))."' class='btn btn-warning btn-sm' title='Sunting'><i class='icon-pencil'></i></a> ";
			$v->aksi  .= "<button class='btn btn-secondary btn-sm' title='Sunting' onclick='detail(" . $v->id . ")'>rincian</button>";
		}

		return $data;
	}

	public function get_detail_servis($id = '')
	{
		$token = $this->input->get('token');

		if (empty($id) or empty($token)) {
			show_404();
		}

		if(!$this->validateToken($token)) {
            show_404();
        }

		$this->model->_table = 'servis_rincian';
		$result = $this->model->get_many_by(['servis_id' => $id]);
		die(json_encode($result));
	}
}
