<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Biaya extends MY_Controller_api
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Biaya_model', 'model');
        $this->load->model('Ekspedisi_model', 'ekspedisi');
        $this->load->model('Pegawai_model', 'pegawai');
        $this->load->model('Balen_model', 'balen');
        $this->has_uuid = TRUE;
    }

    public function get_ekspedisi($token = "")
    {
        # Token
        if (!$this->validateToken($token)) {
            show_404();
        }
        # Ambil filter
        $filter = $this->input->get();
        # Ambil data pegawai
        $pegawai = $this->pegawai->get($this->session->auth['id']);
        # Tentukan apakah sopir
        if ($pegawai['kelompok_id'] == 2) {
            $filter['pegawai_id'] = $pegawai['id'];
        }
        # Ambil data ekspedisi
        $filter['custom_where'] = ['tanggal_entri!=' => 'null', 'tanggal_cek_berangkat!=' => 'null', 'tanggal_cek_pulang!=' => 'null'];
        $result = $this->ekspedisi->get_data($filter);
        $result['rows'] = $this->pegawai->join($result['rows']);
        # Format
        foreach ($result['rows'] as $r) {
            if (empty($r->tanggal_input_biaya)) {
                $r->status = '<span class="badge badge-danger">belum entri</span>';
                $r->aksi = '<a href="' . site_url('biaya/form/' . $token . '/' . $r->id . '/false') . '" class="btn btn-primary btn-sm" title="input biaya"><i class="fa fa-calculator"></i></a>';
            } else if (empty($r->tanggal_validasi_biaya)) {
                $r->status = '<span class="badge badge-warning">menunggu</span>';
                $r->aksi = '<a href="' . site_url('biaya/form/' . $token . '/' . $r->id . '/true') . '" class="btn btn-warning btn-sm" title="sunting biaya"><i class="fa fa-pencil"></i></a>';
            } else {
                $r->status = '<span class="badge badge-success">selesai</span>';
                $r->aksi = '<button class="btn btn-secondary btn-sm" title="detail biaya" onclick="detail(' . $r->id . ')"><i class="fa fa-navicon"></i></button>';
            }
        }

        die(json_encode($result));
    }

    public function get_detail($token = '')
    {
        if (!$this->validateToken($token)) {
            show_404();
        }

        $id_ekspedisi = $this->input->get('ekspedisi_id');
        $result = $this->model->select("biaya.*, tujuan")->join('ekspedisi', 'ekspedisi_id=ekspedisi.id')->get_by(['ekspedisi_id' => $id_ekspedisi]);
        die(json_encode($result));
    }

    public function get_validasi($token = '')
    {
        if (!$this->validateToken($token)) {
            show_404();
        }

        $filter = $this->input->get();
        $filter['custom_where'] = ['tanggal_entri!=' => 'null', 'tanggal_cek_berangkat!=' => 'null', 'tanggal_cek_pulang!=' => 'null', 'tanggal_input_biaya!=' => 'null', 'tanggal_validasi_biaya!=' => 'null'];
        $result = $this->model->get_data($filter);
        $result['rows'] = $this->pegawai->join($result['rows']);

        foreach ($result['rows'] as $r) {
            if (empty($r->tanggal_verifikasi_akhir)) {
                $r->aksi = "<a href='" . site_url('ekspedisi/input_validasi_biaya/' . $this->session->auth['token'] . '/' . $r->id) . "' class='btn btn-sm btn-warning'><i class='fa fa-pencil'></i></a>";
            }
            $r->total = $r->solar + $r->timbangan + $r->tol + $r->polisi + $r->parkir + $r->masuk_ruko + $r->kuli + $r->makan + $r->pengurus_pelabuhan + $r->pak_serap + $r->lain + $r->upah;
            $r->labarugi = ($r->biaya + $r->balen) - $r->total;
            $r->labarugi = $r->labarugi > 0 ? "<b class='text-success'>+" . monefy($r->labarugi, false) . "</b>" : "<b class='text-danger'>" . monefy($r->labarugi, false) . "</b>";
        }

        die(json_encode($result));
    }

    public function save_draft(){
        $post = $this->input->post();
        if (!$this->validateToken($post['token'])) {
            show_404();
        }

        $input = array();
        foreach($post['data'] as $row){
            $input[$row['name']] = str_replace(",", "", $row['value']);
        }
        $id = $input['id'];
        if(isset($input['has_balen'])){
            $balen = $input['has_balen'];
            unset($input['has_balen']);
        }else{
            $balen = 0;
        }

        if(isset($input['tanggal_kembali'])){
            $tanggal_kembali = $input['tanggal_kembali'];
            unset($input['tanggal_kembali']);
        }else{
            $tanggal_kembali = '0000-00-00';
        }

        unset($input['id']);
        $this->ekspedisi->update($input['ekspedisi_id'], array('has_balen' => $balen, 'tanggal_kembali' => $tanggal_kembali));
        if(!empty($id)){
            $result = $this->model->update($id, $input);
        }else{
            $result = $this->model->insert($input);
        }
        if($result){
            $this->message('Berhasil mengubah data', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        echo json_encode($result);
    }

    public function submit_form(){
        $post = $this->input->post();
        if (!$this->validateToken($post['token'])) {
            show_404();
        }

        $input = array();
        foreach($post['data'] as $row){
            $input[$row['name']] = str_replace(",", "", $row['value']);
        }
        $id = $input['id'];
        if(isset($input['has_balen'])){
            $balen = $input['has_balen'];
            unset($input['has_balen']);
        }else{
            $balen = 0;
        }

        if(isset($input['tanggal_kembali'])){
            $tanggal_kembali = $input['tanggal_kembali'];
            unset($input['tanggal_kembali']);
        }else{
            $tanggal_kembali = '0000-00-00';
        }
        
        unset($input['id']);
        if(!empty($id)){
            $this->model->update($id, $input);
        }else{
            $result = $this->model->insert($input);
        }
        $result = $this->ekspedisi->update($input['ekspedisi_id'], array('has_balen' => $balen, 'tanggal_input_biaya' => setNewDateTime(), 'status_biaya' => 1, 'tanggal_kembali' => $tanggal_kembali));
        if($result){
            $this->message('Berhasil mengubah data', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        echo json_encode($result);
    }

    public function batal_pengajuan(){
        $post = $this->input->post();
        if (!$this->validateToken($post['token'])) {
            show_404();
        }

        $result = $this->ekspedisi->update($post['id'], array('status_biaya' => 0));
        if($result){
            $this->message('Berhasil mengubah data', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        echo json_encode($result);
    }

    public function delete_balen(){
        $post = $this->input->post();
        if (!$this->validateToken($post['token'])) {
            show_404();
        }
        $result = $this->balen->delete($post['id']);
        if($result){
            $this->message('Berhasil menghapus data', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        echo json_encode($result);
    }

    public function tolak_pengajuan($token){
        $post = $this->input->post();
        if (!$this->validateToken($token)) {
            show_404();
        }

        $result = $this->ekspedisi->update($post['id'], array('is_done' => 2));
        if($result){
            $this->message('Berhasil mengubah data', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        echo json_encode($result);
    }

    public function get_detail_validasi($token){
        $post = $this->input->post();
        if (!$this->validateToken($token)) {
            show_404();
        }
        $result = $this->ekspedisi->get_data_validasi_biaya($post['id']);
        echo json_encode($result);
    }
}
