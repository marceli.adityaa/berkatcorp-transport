<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kendaraan extends MY_Controller_api
{
	public function __construct()
	{
        parent::__construct();
        $this->load->model('Kendaraan_model','model');
        $this->has_uuid = TRUE;
	}

	public function callback_table($data = [])
	{
		foreach ($data['rows'] as $k => $v) {
			if(isset($v->insert_time))
				$v->insert_time = time_ago($v->update_time);
			# Aksi=========================================
			$disablePar = $v->is_disabled == '1' ?true:false;
			$v->aksi  = "<div><button class='btn btn-warning btn-sm' title='Sunting' onclick='edit(" . $v->id . ")'><i class='icon-pencil'></i></button> ";
			$v->aksi .= "<button class='btn ".($disablePar?'btn-danger':'btn-success')." btn-sm bDisable' title='".($disablePar?'Aktifkan':'Nonaktifkan')."' onclick='disable(" . $v->id . ", ".$disablePar.")'><i class='fa ".($disablePar?'fa-ban':'fa-check')." mr-2'></i>".($disablePar?'nonaktif':'aktif')."</button></div>";
		}

		return $data;
	}
	
    public function get_select2_data($token = '')
	{
		if(!$this->validateToken($token)) {
            show_404();
        }

		$par = $this->input->get();
		$data = array(
			'results' => $this->model->select('id,no_pol AS text')->like(array('no_pol' => $par['q']))->get_many_by(array('is_disabled'=>0)),
			'pagination' => array('more' => false)
		);
		echo json_encode($data);
	}

	public function aktif($token){
		if (!$this->validateToken($token)) {
            show_404();
        }

        $id = $this->input->post('id');
        $result = $this->model->update($id, array('is_disabled' => 0));
        if($result){
            $this->message('Berhasil mengubah data', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
		echo json_encode($result);
	}

	public function nonaktif($token){
		if (!$this->validateToken($token)) {
            show_404();
        }

        $id = $this->input->post('id');
        $result = $this->model->update($id, array('is_disabled' => 1));
        if($result){
            $this->message('Berhasil mengubah data', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
		echo json_encode($result);
	}
}