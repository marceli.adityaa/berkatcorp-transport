<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ekspedisi extends MY_Controller_api
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Ekspedisi_model', 'model');
        $this->load->model('Pegawai_model', 'pegawai');
        $this->load->model('Kelengkapan_model',  'kelengkapan');
        $this->load->model('Biaya_model',  'biaya');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
    }

    protected function callbackTable($data = array())
    {
        $data['rows'] = $this->pegawai->join($data['rows']);

        foreach ($data['rows'] as $k => $v) {
            $v->status = $v->is_deleted ? '<span class="badge badge-danger">dibatalkan</span>' : ($v->is_done ? '<span class="badge badge-success">selesai</span>' : '<span class="badge badge-secondary">berlangsung</span>');
            if (!$v->is_deleted) {
                $v->aksi  = "<a href='".site_url('ekspedisi/sunting/'.$v->id)."' class='btn btn-warning btn-sm' title='Sunting'><i class='icon-pencil'></i></a> ";
                # Aksi jika data dihapus
                if (!$v->tanggal_cek_berangkat) {
                    $v->aksi  .= "<button class='btn btn-danger btn-sm' title='Batalkan transaksi' onclick='cancel(" . $v->id . ")'><i class='icon-ban'></i></button> ";
                }
            }
        }

        return $data;
    }

    public function cancel($token = '')
    {
        if (!$this->validateToken($token)) {
            show_404();
        }

        $id = $this->input->post('id');
        $this->model->update($id, ['is_deleted' => 1]);

        die(json_encode(['status' => 'success', 'message' => 'Ekspedisi berhasil dibatalkan']));
    }

    public function hapus($token = '')
    {
        if (!$this->validateToken($token)) {
            show_404();
        }

        $id = $this->input->post('id');
        $result = $this->model->delete($id);
        if($result){
            die(json_encode(['status' => 'success', 'message' => 'Ekspedisi berhasil dihapus']));
        }else{
            die(json_encode(['status' => 'error', 'message' => 'Gagal menghapus data']));
        }
    }

    public function get_detail_ekspedisi($token = '')
    {
        if (!$this->validateToken($token)) {
            show_404();
        }

        $id = $this->input->post('id');
        $result = $this->model->get_detail_ekspedisi($id);
        echo json_encode($result);
    }

    public function setuju_pengajuan($token = '')
    {
        # Ubah data
        $post = $this->input->post();
        $id = $post['id'];
        $url = $post['url'];
        $post['uang_saku'] = str_replace(",", "", $post['uang_saku']);
        $post['solar_pertama'] = str_replace(",", "", $post['solar_pertama']);
        if(!empty($post['id_rek_pengirim'])){
            $post['id_kas'] = $post['id_rek_pengirim'];
        }
        $post['update_by'] = get_session('auth')['id'];
        $post['status_berangkat'] = 1;
        $post['tanggal_validasi'] = setNewDateTime();
        unset($post['id_rek_pengirim']);
        unset($post['id']);
        unset($post['url']);
        $result = $this->model->update($id, $post);
        
        $detail = $this->model->get_detail_ekspedisi($id);

        
        # Integrasi ke arus kas untuk uang saku
        if($detail->uang_saku > 0){
            $arus_kas['id_perusahaan'] = $this->id_perusahaan;
            $arus_kas['id_kas'] = $detail->id_kas;
            $arus_kas['tgl_transaksi'] = $detail->tanggal_jalan;
            $arus_kas['arus'] = "out";
            $arus_kas['sumber'] = "ekspedisi-berangkat";
            $arus_kas['id_sumber'] = $id;
            $arus_kas['keterangan'] = 'Uang saku '.$detail->no_resi;
            $arus_kas['nominal'] = $detail->uang_saku;
            $arus_kas['input_by'] = get_session('auth')['id'];
            $arus_kas['input_timestamp'] = setNewDateTime();
            $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
            $response = json_decode(xcurl($param_curl)['response']);
        }

        # Integrasi ke arus kas untuk solar pertama
        if($detail->solar_pertama > 0){
            $arus_kas['id_perusahaan'] = $this->id_perusahaan;
            $arus_kas['id_kas'] = $detail->id_kas;
            $arus_kas['tgl_transaksi'] = $detail->tanggal_jalan;
            $arus_kas['arus'] = "out";
            $arus_kas['sumber'] = "ekspedisi-berangkat";
            $arus_kas['id_sumber'] = $id;
            $arus_kas['keterangan'] = 'Solar pertama '.$detail->no_resi;
            $arus_kas['nominal'] = $detail->uang_saku;
            $arus_kas['input_by'] = get_session('auth')['id'];
            $arus_kas['input_timestamp'] = setNewDateTime();
            $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
            $response = json_decode(xcurl($param_curl)['response']);
        }

        if($result){
            $this->message('Berhasil mengubah data', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }

        $this->go('ekspedisi/persetujuan?'.$url);
    }

    public function tolak_pengajuan($token = '')
    {
        if (!$this->validateToken($token)) {
            show_404();
        }

        $id = $this->input->post('id');
        $result = $this->model->update($id, ['status_berangkat' => 2]);
        if($result){
            die(json_encode(['status' => 'success', 'message' => 'Berhasil menolak pengajuan']));
        }else{
            die(json_encode(['status' => 'error', 'message' => 'Gagal menghapus data']));
        }
    }
}
