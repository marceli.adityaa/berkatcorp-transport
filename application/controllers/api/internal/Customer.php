<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Customer extends MY_Controller_api
{
	public function __construct()
	{
        parent::__construct();
        $this->load->model('Customer_model','model');
        $this->has_uuid = TRUE;
	}

	public function ctable($data = [])
	{
		foreach ($data['rows'] as $k => $v) {
			if(isset($v->insert_time))
				$v->insert_time = time_ago($v->update_time);
			# Aksi=========================================
			$disablePar = $v->is_disabled == '1' ?true:false;
			$v->aksi  = "<div><button class='btn btn-warning btn-sm' title='Sunting' onclick='edit(" . $v->id . ")'><i class='icon-pencil'></i></button> ";
			$v->aksi .= "<button class='btn ".($disablePar?'btn-danger':'btn-success')." btn-sm bDisable' title='".($disablePar?'Aktifkan':'Nonaktifkan')."' onclick='disable(" . $v->id . ", ".$disablePar.")'><i class='fa ".($disablePar?'fa-ban':'fa-check')." mr-2'></i>".($disablePar?'nonaktif':'aktif')."</button></div>";
		}

		return $data;
	}
	
	public function csearch($data = [])
	{
		foreach ($data['rows'] as $k => $v) {
			$v->nama = "<span class='sNama'>".$v->nama."</span>";
			$v->alamat = "<span class='salamat'>".$v->alamat."</span>";
			$v->telpon = "<span class='stelpon'>".$v->telpon."</span>";
			$v->aksi = "<button class='btn btn-primary btn-sm' onclick='pilih(this)'>pilih</button>";
		}

		return $data;
	}
	
	public function beforeSave($data = [])
	{
		if(!isset($data['is_sender'])) {
			$data['is_sender'] = 0;
		}

		if(!isset($data['is_reciver'])) {
			$data['is_reciver'] = 0;
		}

		return $data;
	}
}