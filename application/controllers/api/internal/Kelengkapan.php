<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kelengkapan extends MY_Controller_api
{
	public function __construct()
	{
        parent::__construct();
        $this->load->model('Kelengkapan_model','model');
        $this->load->model('Pegawai_model','pegawai');
        $this->has_uuid = TRUE;
    }

    public function cTable($data = [])
    {
        $data['rows'] = $this->pegawai->join($data['rows']);
        foreach ($data['rows'] as $d) {
            $d->aksi = "<button class='btn btn-sm btn-primary' onclick='detail(" . $d->id . ")'><i class='fa fa-tasks'></i?</button>";
        }

        return $data;
    }

    public function get_detail($token){
        if (!$this->validateToken($token)) {
            show_404();
        }

        $id = $this->input->post('id');
        $result = $this->model->get_detail($id);
        echo json_encode($result);
    }
}