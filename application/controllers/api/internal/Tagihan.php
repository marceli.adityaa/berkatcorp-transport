<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tagihan extends MY_Controller_api
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Ekspedisi_model', 'ekspedisi');
        $this->load->model('Pegawai_model', 'pegawai');
        $this->load->model('Balen_model', 'balen');
        $this->load->model('Tagihan_model', 'tagihan');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        // $this->has_uuid = TRUE;
    }

    public function tolak_pengajuan($token){
        $post = $this->input->post();
        if (!$this->validateToken($token)) {
            show_404();
        }

        $result = $this->tagihan->update($post['id'], array('is_lunas' => 2));
        if($result){
            $this->message('Berhasil mengubah data', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        echo json_encode($result);
    }

    public function get_detail_validasi($token){
        $post = $this->input->post();
        if (!$this->validateToken($token)) {
            show_404();
        }
        $result = $this->tagihan->get_detail($post['id']);
        echo json_encode($result);
    }

    public function setuju_pengajuan(){
        $post = $this->input->post();
        $id = $post['id'];
        if(!empty($id)){
            if(isset($post['jenis_pembayaran'])){
                if($post['jenis_pembayaran'] == 'cash'){
                    $kas = $post['id_kas'];
                }else{
                    $kas = $post['id_rek_penerima'];
                }
            }else{
                $post['jenis_pembayaran'] = 'tbd';
                $kas = 0;
            }
            

			$update = array(
				'pembayaran' => $post['jenis_pembayaran'],
				'id_kas' => $kas,
				'tanggal_bayar' => $post['tgl_bayar'],
                'jumlah_bayar' => str_replace(",", "", $post['jumlah_bayar']),
                'is_lunas' => 1,
				'verif_by' => get_session('auth')['id'],
				'timestamp_verif' => setNewDateTime(),
			);
            $result = $this->tagihan->update($id, $update);
			
			if($result){
				$transaksi = $this->tagihan->get_detail($id);
                $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                $arus_kas['id_kas'] = $transaksi['id_kas'];
                $arus_kas['tgl_transaksi'] = $transaksi['tanggal_bayar'];
                $arus_kas['arus'] = "in";
                $arus_kas['sumber'] = $transaksi['sumber'];
                $arus_kas['id_sumber'] = $post['id'];
                $arus_kas['keterangan'] = 'Pendapatan '.$transaksi['no_resi'];
                $arus_kas['nominal'] = str_replace(",", "", $post['jumlah_bayar']);
                $arus_kas['input_by'] = get_session('auth')['id'];
                $arus_kas['input_timestamp'] = setNewDateTime();
                $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                $result = json_decode(xcurl($param_curl)['response']);
			}
		}else{
			$result = false;
		}
		
		if ($result) {
			$this->message('Berhasil menyetujui data', 'success');
		} else {
			$this->message('Gagal', 'error');
		}

		$this->go('tagihan/index?'.$post['url']);
    }

}
