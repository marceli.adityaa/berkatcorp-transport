<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Area extends MY_Controller_api
{
    protected $auth_type = 'external';
    
	public function __construct()
	{
        parent::__construct();
        $this->load->model('Area_model','model');
    }
    
    public function get($token = '')
    {
        if(!$this->validateToken($token)) {
            show_404();
        }

        $param = $this->input->get();
        
        die(json_encode($this->model->get_data($param)));
    }
}