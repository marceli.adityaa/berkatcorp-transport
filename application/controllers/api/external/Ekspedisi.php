<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ekspedisi extends MY_Controller_api
{
    protected $auth_type = 'external';
    
    public function __construct()
	{
        parent::__construct();
        $this->load->model('Ekspedisi_model','model');
        $this->load->model('Kendaraan_model','kendaraan');
        $this->load->model('Biaya_model','biaya');
    }
    
    public function get($token = '')
    {
        if(!$this->validateToken($token)) {
            show_404();
        }

        $param = $this->input->get();
        echo json_encode($param);
        // echo json_encode($this->model->get_data($param));
    }

    public function get_active()
    {
        $param = $this->input->get();
        if(!$this->validateToken($param['token'])) {
            show_404();
        }
        echo json_encode($this->model->get_active($param));
    }

    public function get_detail_ekspedisi()
    {
        $param = $this->input->get();

        if(!$this->validateToken($param['token'])) {
            show_404();
        }
        echo json_encode($this->model->get_detail_resi2($param['resi']));
    }


    public function update_muatan()
    {
        $param = $this->input->get();

        if(!$this->validateToken($param['token'])) {
            show_404();
        }
        echo json_encode($this->model->update_muatan($param));
    }

    public function proses_kiriman()
    {
        $param = $this->input->get();

        if(!$this->validateToken($param['token'])) {
            show_404();
        }
        echo json_encode($this->model->update_status_muatan($param['resi'], 1));
    }

    public function batal_kiriman()
    {
        $param = $this->input->get();

        if(!$this->validateToken($param['token'])) {
            show_404();
        }
        echo json_encode($this->model->update_status_muatan($param['resi'], 0));
    }

    public function submit($token)
    {
        $data = $this->input->post();

        if(!isset($data['token'])){
            show_404();
        }

        if(!$this->validateToken($token)) {
            show_404();
        }
        
        unset($data['token']);

        $this->load->model('Kendaraan_model','kendaraan');

        $kendaraan = $this->kendaraan->get($data['kendaraan_id']);
        # Generate RESI
        $data['no_resi'] = datify($data['tanggal_jalan'],'ymd').strtoupper($kendaraan->no_pol);
        # Uang saku
        $uang_saku = $data['uang_saku'];
        unset($data['uang_saku']);
        # Insert
        $this->model->insert($data);
        $this->biaya->insert(['no_resi'=>$data['no_resi'], 'uang_saku'=>$uang_saku]);
        
        die(json_encode(['status'=>'success','message'=>'Data berhasil disimpan']));
    
    }
}