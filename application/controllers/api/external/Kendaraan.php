<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kendaraan extends MY_Controller_api
{
    protected $auth_type = 'external';
    
	public function __construct()
	{
        parent::__construct();
        $this->load->model('Kendaraan_model','model');
    }
    
    public function get($token = '')
    {
        $param = $this->input->get();

        if(!$this->validateToken($param['token'])) {
            show_404();
        }
        
        echo json_encode($this->model->get_active());
    }

}