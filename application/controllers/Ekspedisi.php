<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Ekspedisi extends MY_Controller_admin
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Ekspedisi_model', 'model');
        $this->load->model('Kelengkapan_model', 'kelengkapan');
        $this->load->model('Biaya_model', 'biaya');
        $this->load->model('Kendaraan_model', 'kendaraan');
        $this->load->model('Pegawai_model', 'pegawai');
        $this->load->model('Customer_model', 'customer');
        $this->load->model('Area_model', 'area');
        $this->load->model('Balen_model', 'balen');
        $this->load->model('Kas_model', 'kas');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
    }

    public function index()
    {
        if(empty($_GET['status'])){
            $_GET['status'] = 1;   
        }
        // $data['eks_aktif'] = $this->model->getEkspedisiAktif();
        $data['ekspedisi'] = $this->kas->join($this->pegawai->joins($this->model->get_data_ekspedisi(), 'supir', 'pegawai_id'),'kas', 'id_kas');
        $data['widget'] = $this->model->get_widget();
        $data['widget'] = array_merge($data['widget'], $this->biaya->getWidget());
        $data['kendaraan'] = $this->kendaraan->get_active();
        $data['supir'] = $this->pegawai->get_active_driver()['rows'];
        $data['status'] = array(1 => 'Menunggu', 2 => 'Berlangsung', 3 => 'Selesai', 4 => 'Dibatalkan');
        $data['customer'] = $this->customer->get_active();
        // dump($data['ekspedisi']);
        set_session('title', 'Ekspedisi');
        set_activemenu('sub-ekspedisi', 'menu-ekspedisi');
        $this->render('ekspedisi/index', $data);
    }

    public function tambah()
    {
        $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
        $param_rek_pengirim = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
        $data['kas']        = json_decode(xcurl($param_kas)['response']);
        $data['rek_pengirim'] = json_decode(xcurl($param_rek_pengirim)['response']);
        $data['pembayaran'] = array('cash', 'transfer', 'tbd');
        $data['sopir'] = $this->pegawai->get_many(['kelompok_id' => 2]);
        $data['kendaraan'] = $this->kendaraan->get_data();
        $data['area'] = $this->area->order_by('kota')->get_all();
        $data['customer'] = $this->customer->get_active();
        set_session('title', 'Ekspedisi');
        set_activemenu('sub-ekspedisi', 'menu-ekspedisi');
        $this->render('ekspedisi/form', $data);
    }

    public function sunting($id = '')
    {
        if(empty($id)) {
            show_404();
        }
        $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
        $param_rek_pengirim = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
        $data['kas']        = json_decode(xcurl($param_kas)['response']);
        $data['rek_pengirim'] = json_decode(xcurl($param_rek_pengirim)['response']);
        $data['pembayaran'] = array('cash', 'transfer', 'tbd');

        $data['ekspedisi'] = $this->model->get($id);
        $data['sopir'] = $this->pegawai->get_many(['kelompok_id' => 2]);
        $data['kendaraan'] = $this->kendaraan->get_data();
        $data['area'] = $this->area->order_by('kota')->get_all();
        $data['customer'] = $this->customer->get_active();
        set_session('title', 'Ekspedisi');
        set_activemenu('sub-ekspedisi', 'menu-ekspedisi');
        $this->render('ekspedisi/form', $data);
    }

    public function save($token = '')
    {
        if (!$this->validateToken($token)) {
            show_404();
        }

        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']);
        
        if($data['jenis_pembayaran'] == 'transfer'){
            $data['id_kas'] = $data['id_rek_pengirim'];
        }
        unset($data['id_rek_pengirim']);

        if (empty($id)) {
            # Insert
            $data['no_resi'] = 'RS' . date('ymd') . '-' . strtoupper(uniqchar(3));
            $data['tanggal_entri'] = date('Y-m-d H:i:s');
            $this->model->insert($data);
            $this->message('Ekspedisi berhasil ditambah');
        } else {
            # Update
            $this->model->update($id, $data);
            $this->message('Ekspedisi berhasil disunting');
        }

        $this->go('ekspedisi');
    }

    public function detail($id = '')
    {
        if(empty($id)) {
            show_404();
        }

        $data['ekspedisi'] = $this->model->select('ekspedisi.*, no_pol')->join("kendaraan", "kendaraan_id = kendaraan.id")->get_by(['ekspedisi.id'=>$id]);
        $data['ekspedisi'] = $this->pegawai->join($data['ekspedisi']);
        if(empty($data['ekspedisi'])) {
            show_404();
        }

        $data['cek_berangkat'] = $this->kelengkapan->get_by(['ekspedisi_id'=>$id,'jenis_cek'=>'berangkat']);
        $data['cek_pulang'] = $this->kelengkapan->get_by(['ekspedisi_id'=>$id,'jenis_cek'=>'pulang']);

        $data['biaya'] = $this->biaya->get_by(['ekspedisi_id'=>$id]);
        $data['balen'] = $this->balen->get_by(['ekspedisi_id'=>$id]);
        $this->render('ekspedisi/detail', $data);
    }

    public function finish($id = '')
    {
        if(empty($id)) {
            show_404();
        }

        $this->model->update($id, ['tanggal_verifikasi_akhir'=>date('Y-m-d H:i:s'), 'is_done'=>1]);
        $this->go('ekspedisi/detail/'.$id);
    }

    public function persetujuan()
    {
        if(empty($_GET['status'])){
            $_GET['status'] = 1;   
        }
        $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
        $param_rek_pengirim = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
        $data['kas']        = json_decode(xcurl($param_kas)['response']);
        $data['rek_pengirim'] = json_decode(xcurl($param_rek_pengirim)['response']);

        $data['ekspedisi'] = $this->kas->join($this->pegawai->joins($this->model->get_data_persetujuan_ekspedisi(), 'supir', 'pegawai_id'), 'kas', 'id_kas');
        $data['kendaraan'] = $this->kendaraan->get_active();
        $data['status'] = array(1 => 'Menunggu', 2 => 'Disetujui', 3 => 'Ditolak');
        $data['supir'] = $this->pegawai->get_active_driver()['rows'];
        $data['pembayaran'] = array('cash', 'transfer');
        $data['area'] = $this->area->order_by('kota')->get_all();
        // dump($data['kas']);
        set_session('title', 'Persetujuan Ekspedisi');
        set_activemenu('sub-ekspedisi', 'menu-persetujuan-ekspedisi');
        $this->render('ekspedisi/persetujuan', $data);
    }

    public function rekap()
    {
        if(empty($_GET['status'])){
            $_GET['status'] = 3;   
        }
        $data['ekspedisi'] = $this->pegawai->joins($this->model->get_rekap_ekspedisi(), 'supir', 'pegawai_id');
        $data['kendaraan'] = $this->kendaraan->get_active();
        $data['supir'] = $this->pegawai->get_active_driver()['rows'];
        $data['customer'] = $this->customer->get_active();
        // dump($data['ekspedisi']);
        set_session('title', 'Rekap Ekspedisi');
        set_activemenu('sub-ekspedisi', 'menu-rekap-ekspedisi');
        $this->render('ekspedisi/rekap', $data);
    }

    public function detail_rekap($id){
        $data['ekspedisi'] = $this->model->select("ekspedisi.*, no_pol, kota, is_sby, customer.nama as customer")->join("kendaraan", "kendaraan_id=kendaraan.id")->join("area", "area.id = id_area")->join('customer', 'customer_id = customer.id')->get_by(['ekspedisi.id' => $id]);
        $data['biaya'] = $this->biaya->get_by(['ekspedisi_id' => $id]);
        $data['balen'] = $this->kas->join($this->balen->get_data_balen($id), 'kas', 'transfer_rekening');
        $data['area'] = $this->area->order_by('kota')->get_all();
        $data['customer'] = $this->customer->get_active();
        $data['id'] = $id;
        set_session('title', 'Detail Transaksi');
        set_activemenu('sub-ekspedisi', 'menu-rekap-ekspedisi');
        $this->render('ekspedisi/detail_rekap', $data);
    }

    public function export_rekap(){
        $get                    = $this->input->get();
        $_GET['status']         = 3;
        $data['transaksi']      = $this->pegawai->joins($this->model->get_rekap_ekspedisi(), 'supir', 'pegawai_id');
        // dump($data);
        $spreadsheet = new Spreadsheet();
        $idx = 0;
        $total_ongkos = 0;
        
        $spreadsheet->getActiveSheet()->setTitle('Rekap Ekspedisi');
        $spreadsheet->setActiveSheetIndex($idx);
        $sheet = $spreadsheet->getActiveSheet($idx);
        $kolom = 1;
        $baris = 1;
        # PROSES - HEADER
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'No Resi');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tgl Jalan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tgl Kembali');
        // $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Waktu Berangkat');
        // $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Waktu Pulang');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Kendaraan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Supir');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tujuan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Isi Berangkat');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Jenis Perhitungan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Berat Muatan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'KM Awal');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'KM Akhir');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Total KM');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Uang Saku');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Solar Pertama');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Total Biaya');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Upah Supir');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Ongkos Ekspedisi');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Ongkos Balen');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Laba / Rugi');
        $baris++;
        if(!empty($data['transaksi'])){
            foreach($data['transaksi'] as $d){
                $kolom = 1;
                $laba = ($d['biaya'] + $d['total_balen'] - $d['solar_pertama'] - $d['total_biaya']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['no_resi']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['tanggal_jalan']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['tanggal_kembali']);
                // $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['tanggal_cek_berangkat']);
                // $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['tanggal_cek_pulang']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['no_pol']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($d['supir']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['kota']);   
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['jenis_barang']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['jenis_perhitungan_berat']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['berat_muatan']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['km_awal']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['km_akhir']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['km_total']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['uang_saku']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['solar_pertama']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['total_biaya']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['upah']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['biaya']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d['total_balen']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $laba);
                $baris++;
                $total_ongkos += $d['biaya'];
            }
        }


        # Set subtotal
        $baris++;
        $kolom = 1;
        // $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Total Berat Netto');
        // $sheet->setCellValueByColumnAndRow($kolom++, $baris, $total_netto);

        foreach (range('A', $spreadsheet->getActiveSheet()->getHighestDataColumn()) as $col) {
            $spreadsheet->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }
        $idx++;  

        # SAVE
        $writer = new Xlsx($spreadsheet);
        $datenow = date('Ymdhis', strtotime(setNewDateTime()));
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="rekap_ekspedisi_'.$datenow.'.xlsx"');
		$writer->save("php://output");
    }
}
