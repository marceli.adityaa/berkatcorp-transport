<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Biaya extends MY_Controller_admin
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Ekspedisi_model', 'ekspedisi');
        $this->load->model('Biaya_model', 'biaya');
        $this->load->model('Balen_model', 'balen');
        $this->load->model('Kendaraan_model', 'kendaraan');
        $this->load->model('Pegawai_model', 'pegawai');
        $this->load->model('Area_model', 'area');
        $this->load->model('Kas_model', 'kas');
        $this->load->model('Customer_model', 'customer');
        $this->load->model('Tagihan_model', 'tagihan');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
    }

    public function index()
    {
        if(empty($_GET['status'])){
            $_GET['status'] = 1;   
        }
        $data['ekspedisi'] = $this->pegawai->joins($this->ekspedisi->get_data_biaya(), 'supir', 'pegawai_id');
        $data['kendaraan'] = $this->kendaraan->get_active();
        $data['supir'] = $this->pegawai->get_active_driver()['rows'];
        $data['customer'] = $this->customer->get_active();
        $data['status'] = array(1 => 'Menunggu input', 2 => 'Menunggu Persetujuan', 3 => 'Selesai', 4 => 'Dibatalkan');
        set_session('title', 'Biaya');
        set_activemenu('sub-ekspedisi', 'menu-biaya');
        $this->render('ekspedisi/biaya/index', $data);
    }

    public function form($id = '')
    {
        $param_rek_penerima = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
        $data['rek_penerima'] = json_decode(xcurl($param_rek_penerima)['response']);
        $data['pembayaran'] = array('cash', 'transfer');
        $data['ekspedisi'] = $this->ekspedisi->select("ekspedisi.*, no_pol, kota, is_sby, customer.nama as customer")->join("kendaraan", "kendaraan_id=kendaraan.id")->join("area", "area.id = id_area")->join('customer', 'customer_id = customer.id')->get_by(['ekspedisi.id' => $id]);
        $data['biaya'] = $this->biaya->get_by(['ekspedisi_id' => $id]);
        $data['balen'] = $this->kas->join($this->balen->get_data_balen($id), 'kas', 'transfer_rekening');
        $data['area'] = $this->area->order_by('kota')->get_all();
        $data['customer'] = $this->customer->get_active();
        $data['id'] = $id;
        set_session('title', 'Form Biaya');
        set_activemenu('sub-ekspedisi', 'menu-biaya');
        $this->render('ekspedisi/biaya/form', $data);
    }

    public function save($token = '')
    {
        if (!$this->validateToken($token)) {
            show_404();
        }

        # Ambil data
        $data = $this->input->post();
        if (isset($data['id']) && !empty($data['id'])) {
            # UPDATE
            $id = $data['id'];
            unset($data['id']);
            # Cek sumber
            $isvalidasi = $this->input->get('isvalidasi');

            if (!empty($isvalidasi)) {
                # Input balen
                if (isset($data['hasbalen'])) {
                    $this->balen->delete_by(['ekspedisi_id' => $data['ekspedisi_id']]);
                    $data['balen']['ekspedisi_id'] = $data['ekspedisi_id'];
                    $this->balen->insert($data['balen']);
                    $data['balen'] = $data['balen']['biaya'];
                } else {
                    $data['balen'] = 0;
                }

                unset($data['hasbalen']);
                $this->biaya->update($id, $data);
                $this->ekspedisi->update($data['ekspedisi_id'], ['tanggal_validasi_biaya' => date('Y-m-d H:i:s')]);
                $this->go('validasi_biaya');
            } else {
                $this->biaya->update($id, $data);
                $this->ekspedisi->update($data['ekspedisi_id'], ['tanggal_input_biaya' => date('Y-m-d H:i:s')]);
                $this->go('biaya');
            }
        } else {
            # INSERT
            $this->biaya->insert($data);
            $this->ekspedisi->update($data['ekspedisi_id'], ['tanggal_input_biaya' => date('Y-m-d H:i:s')]);
            $this->go('biaya');
        }
    }

    public function submit_tambah_balen(){
        $post = $this->input->post();
        $ekspedisi = $post['ekspedisi_id'];
        $post['berat'] = str_replace(",", "", $post['berat']);
        $post['harga_satuan'] = str_replace(",", "", $post['harga_satuan']);
        $post['biaya'] = str_replace(",", "", $post['biaya']);
        $post['insert_time'] = setNewDateTime();
        $post['insert_by'] = get_session('auth')['id'];
        $result = $this->balen->insert($post);
        if($result){
            $this->message('Berhasil menambahkan data', 'success');
        }else{
            $this->message('Gagal menambahkan data', 'error');
        }
        $this->go('biaya/form/'.$ekspedisi);
    }

    
}
