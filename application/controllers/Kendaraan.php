<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Kendaraan extends MY_Controller_admin
{
	public function __construct()
	{
		parent::__construct();
        $this->load->model('Master_biaya_kendaraan_model', 'master_biaya_kendaraan');
		$this->load->model('Kendaraan_model', 'kendaraan');
        $this->load->model('Kendaraan_biaya_model', 'kendaraan_biaya');
        $this->load->model('user_model', 'user');
        $this->load->model('kas_model', 'kas');
		$this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->source = 'master-kendaraan';
        $this->role = json_decode($this->session->auth['role'])->tran;
	}

	public function index()
    {

        $data['kendaraan'] = $this->kendaraan->get_data_kendaraan();
		// dump($data);
        $data['status'] = array(1 => 'Aktif', 2 => 'Nonaktif');
        set_session('title', 'Master Kendaraan');
        set_activemenu('sub-kendaraan', 'menu-kendaraan');
		$this->render('kendaraan/index', $data);
    }

	public function biaya(){
		$hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('kdr-by', $hak->tran) || $hak->tran[0] == '*') {
            $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rekening = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
            $data['rekening']   = json_decode(xcurl($param_rekening)['response']);
            $data['pembayaran'] = array('cash', 'transfer', 'tbd');
            $data['role'] = $this->role;
            $data['kendaraan'] = $this->kendaraan->get_active();
            $data['kategori'] = $this->master_biaya_kendaraan->get_active();
            $data['biaya'] = $this->kas->join($this->user->join($this->user->join($this->kendaraan_biaya->get_data_kelola(), 'submit_by', 'input_by'), 'acc_by', 'verifikasi_by'), 'kas', 'id_kas');
            $data['status'] = array( 1 => "Menunggu Verifikasi", 2 => "Disetujui", 3 => "Ditolak");
            // dump($data);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Manajemen Beban' => 'active', 'Kelola Transaksi' => 'active'));
            set_session('title', 'Biaya Kendaraan');
            set_activemenu('sub-kendaraan', 'menu-biaya-kendaraan');
            $this->render('kendaraan/v-biaya-kendaraan', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
	}

    public function persetujuan_biaya(){
        if(empty($_GET['status'])){
            $_GET['status'] = 1;   
        }
        $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
        $param_rekening = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
        $data['kas']        = json_decode(xcurl($param_kas)['response']);
        $data['rekening']   = json_decode(xcurl($param_rekening)['response']);
        $data['kendaraan'] = $this->kendaraan->get_active();
        $data['kategori'] = $this->master_biaya_kendaraan->get_active();
        $data['biaya'] = $this->kas->join($this->user->join($this->user->join($this->kendaraan_biaya->get_data_kelola(), 'submit_by', 'input_by'), 'acc_by', 'verifikasi_by'), 'kas', 'id_kas');
        $data['status'] = array(1 => 'Menunggu', 2 => 'Disetujui', 3 => 'Ditolak');
        $data['pembayaran'] = array('cash', 'transfer');
        set_session('title', 'Persetujuan Biaya Kendaraan');
        set_activemenu('sub-kendaraan', 'menu-otorisasi-biaya-kendaraan');
        $this->render('kendaraan/v-persetujuan-biaya', $data);
    }

    public function rekap_biaya(){
        if(empty($_GET['status'])){
            $_GET['status'] = 2;   
        }
        $data['kendaraan'] = $this->kendaraan->get_active();
        $data['kategori'] = $this->master_biaya_kendaraan->get_active();
        $data['biaya'] = $this->kas->join($this->user->join($this->user->join($this->kendaraan_biaya->get_data_kelola(), 'submit_by', 'input_by'), 'acc_by', 'verifikasi_by'), 'kas', 'id_kas');
        set_session('title', 'Rekap Biaya Kendaraan');
        set_activemenu('sub-kendaraan', 'menu-rekap-biaya-kendaraan');
        $this->render('kendaraan/v-rekap-biaya', $data);
    }

    public function submit_biaya()
    {
        $post = $this->input->post();
        $url = $post['url'];
        if(empty($post['id'])){
            # Insert Statement
            $beban['id_kategori'] = $post['id_kategori'];
            $beban['id_kendaraan'] = $post['id_kendaraan'];
            $beban['tanggal'] = $post['tanggal'];
            $beban['nominal'] = str_replace(",", "", $post['nominal']);
            $beban['catatan'] = $post['catatan'];
            $beban['jenis_pembayaran'] = $post['jenis_pembayaran'];
            if($post['jenis_pembayaran'] == 'cash'){
                $beban['id_kas'] = $post['id_kas'];
            }else if($post['jenis_pembayaran'] == 'transfer'){
                $beban['id_kas'] = $post['id_rekening'];
            }else{
                $beban['id_kas'] = 0;
            }
            $beban['input_by'] = get_session('auth')['id'];
            $beban['timestamp_input'] = setNewDateTime();
            $result = $this->kendaraan_biaya->insert($beban);
            
            if ($result == true) {
                $this->message('Berhasil menyimpan data.', 'success');
            } else {
                $this->message('Gagal menyimpan data', 'error');
            }
        }else{
            # Update Statement
            $beban['id_kategori'] = $post['id_kategori'];
            $beban['id_kendaraan'] = $post['id_kendaraan'];
            $beban['tanggal'] = $post['tanggal'];
            $beban['nominal'] = str_replace(",", "", $post['nominal']);
            $beban['catatan'] = $post['catatan'];
            $beban['jenis_pembayaran'] = $post['jenis_pembayaran'];
            if($post['jenis_pembayaran'] == 'cash'){
                $beban['id_kas'] = $post['id_kas'];
            }else if($post['jenis_pembayaran'] == 'transfer'){
                $beban['id_kas'] = $post['id_rekening'];
            }else{
                $beban['id_kas'] = 0;
            }
            $beban['input_by'] = get_session('auth')['id'];
            $beban['timestamp_input'] = setNewDateTime();
            $result = $this->kendaraan_biaya->update($post['id'], $beban);
            if ($result == true) {
                $this->message('Berhasil menyimpan data.', 'success');
            } else {
                $this->message('Gagal mengubah data', 'error');
            }
        }        
        
        $this->go('kendaraan/biaya?'.$url);
    }

    public function approve_transaksi_biaya(){
        $id = $this->input->post('id');
        if(!empty($id)){
            $detail = $this->kendaraan_biaya->get_data_kelola($id);
            $update['is_verifikasi'] = 1;
            $update['verifikasi_by'] = get_session('auth')['id'];
            $update['timestamp_verifikasi'] = setNewDateTime();
            $response = $this->kendaraan_biaya->update($id, $update);
            if($response){
                # record ke arus kas
                $arus_kas['id_perusahaan'] = $this->id_perusahaan;
                $arus_kas['id_kas'] = $detail['id_kas'];
                $arus_kas['tgl_transaksi'] = $detail['tanggal'];
                $arus_kas['arus'] = "out";
                $arus_kas['sumber'] = 'master-biaya-kendaraan';
                $arus_kas['id_sumber'] = $id;
                $arus_kas['keterangan'] = $detail['kategori'];
                $arus_kas['deskripsi'] = $detail['catatan'];
                $arus_kas['nominal'] = $detail['nominal'];
                $arus_kas['input_by'] = get_session('auth')['id'];
                $arus_kas['input_timestamp'] = setNewDateTime();
                $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
                $result = json_decode(xcurl($param_curl)['response']);
                $response = $result;
                if($response){
                    $this->message('Transaksi berhasil disetujui.', 'success');
                }else{
                    $this->message('Gagal record data ke arus kas', 'warning');
                }
            }else{
                $response = false;
                $this->message('Gagal mengubah data', 'error');
            }
        }else{
            $response = false;
            $this->message('Terjadi kesalahan!', 'error');
        }
        $this->go('kendaraan/persetujuan_biaya');
    }

    public function decline_transaksi_biaya(){
        $id = $this->input->post('id');
        if (!empty($id)) {
            $update = array(
                'is_verifikasi' => 2,
                'verifikasi_by' => get_session('auth')['id'],
                'timestamp_verifikasi' => setNewDateTime(),
            );
            $response = $this->kendaraan_biaya->update($id, $update);
            if ($response == true) {
                $this->message('Transaksi ditolak.', 'success');
            } else {
                $this->message('Gagal mengubah data', 'error');
            }
        } else {
            $response = false;
            $this->message('Terjadi kesalahan!', 'error');
        }

        echo json_encode($response);
    }

    public function json_get_data_biaya()
    {
        $id = $this->input->post('id');
        $response = $this->kendaraan_biaya->get_data($id);
        echo json_encode($response);
    }

    public function delete_data_biaya()
    {
        $id = $this->input->post('id');
        $detail = $this->kendaraan_biaya->get_data($id);
        if($detail['is_verifikasi'] == 1){
            # Decline action
            $this->message('Gagal menghapus data. Data transaksi sudah disetujui.', 'warning');
        }else{
            # Delete transaksi
            $result = $this->kendaraan_biaya->delete($id);
            if($result){
                $this->message('Berhasil menghapus data transaksi.', 'success');
            }else{
                $this->message('Gagal menghapus data.', 'danger');
            }
        }
        echo json_encode($detail);
    }

    public function export_biaya()
    {
        $_GET['status'] = 2;
        $transaksi = $this->kas->join($this->user->join($this->user->join($this->kendaraan_biaya->get_data_kelola(), 'submit_by', 'input_by'), 'acc_by', 'verifikasi_by'), 'kas', 'id_kas');
        # INIT
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
        $kolom = 1;
        $baris = 1;
		# PROSES - HEADER
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tanggal');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Kendaraan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Kategori');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Catatan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Nominal');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Sumber Kas');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Input by');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Timestamp Input');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Verif by');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Timestamp Verif');
        $baris++;
        if(!empty($transaksi)){
            foreach($transaksi as $row){
                $kolom = 1;
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['tanggal']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($row['kendaraan']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($row['kategori']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($row['catatan']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['nominal']);
                
                if(isset($row['kas_is_rekening'])){
                    if($row['kas_is_rekening'] == 1){
                        $sheet->setCellValueByColumnAndRow($kolom++, $baris, ($row['kas_bank'].'-'.$row['kas_no_rek']));
                        
                    }else{
                        $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['kas_kas']);
                    }
                }else{
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, '-');
                }
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($row['submit_by']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['timestamp_input']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ucwords($row['acc_by']));
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row['timestamp_verifikasi']);
                $baris++;
            }
        }
        # SAVE
		$writer = new Xlsx($spreadsheet);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="rekap_biaya_kendaraan_'.strtotime(setNewDateTime()).'.xlsx"');
		$writer->save("php://output");
    }
}