<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Customer extends MY_Controller_admin
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Customer_model', 'customer');
    }

    public function index()
    {
    	set_session('title', 'Customer');
        set_activemenu('sub-master', 'menu-customer');
        $this->render('customer/index');
    }
}
