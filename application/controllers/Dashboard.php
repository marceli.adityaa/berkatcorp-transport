<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller_admin
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Ekspedisi_model', 'ekspedisi');
		$this->load->model('Biaya_model', 'biaya');
	}

	public function index()
	{
		$data['wEkspedisi'] = $this->ekspedisi->count_by(['YEAR(tanggal_jalan)'=>date('Y'), 'is_deleted'=>0]);
		$data['wJumlah'] = $this->biaya->getWidget();
		$data['graph'] = $this->biaya->getGraphAnnual();
		set_session('title', 'Dashboard');
        set_activemenu('', 'menu-dashboard');
		$this->render('dashboard/index', $data);
	}
}