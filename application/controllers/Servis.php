<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Servis extends MY_Controller_admin
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Servis_model', 'servis');
		$this->load->model('Pegawai_model', 'pegawai');
		$this->load->model('Kendaraan_model', 'kendaraan');
	}

	public function index()
	{
		set_session('title', 'Servis Kendaraan');
        set_activemenu('sub-kendaraan', 'menu-servis');
		$this->render('servis/index');
	}

	public function add()
	{
		# Ambil sopir
		$data['sopir'] = $this->pegawai->get_many(['kelompok_id'=>2])['rows'];
		$data['kendaraan'] = $this->kendaraan->get_all();
		# Generate UUID
		$data['uuid'] = 'SV' . date('ymd') . '-' . strtoupper(uniqchar(3));
		set_session('title', 'Tambah Servis Kendaraan');
        set_activemenu('sub-kendaraan', 'menu-servis');
		$this->render('servis/form', $data);
	}

	public function edit($id = '')
	{
		if (empty($id))
			show_404();

		# Ambil sopir
		$data['sopir'] = $this->pegawai->get_many(['kelompok_id'=>2])['rows'];
		# Ambil data
		$data['servis'] = $this->servis->get($id);
		if ($data['servis']) {
			$this->servis->_table = 'servis_rincian';
			$data['servis']->kendaraan_id = $this->kendaraan->get($data['servis']->kendaraan_id);
			$data['servis']->rincian = $this->servis->get_many_by(['servis_id' => $id]);
		}
		set_session('title', 'Edit Servis Kendaraan');
        set_activemenu('sub-kendaraan', 'menu-servis');
		$this->render('servis/form', $data);
	}

	public function save()
	{
		$data = $this->input->post();

		if (empty($data['id'])) {
			$rincian = $data['rincian'];
			unset($data['rincian']);
			# Insert servis
			$id = $this->servis->insert($data);
			# Insert rincian
			foreach ($rincian as $index => $value) {
				$rincian[$index]['servis_id'] = $id;
			}
			$this->servis->_table = 'servis_rincian';
			$this->servis->batch_insert($rincian);

			$this->go('servis');
		} else {
			$id = $data['id'];
			$rincian = $data['rincian'];
			unset($data['rincian'], $data['id']);
			# Insert servis
			$this->servis->update($id, $data);
			# Insert rincian
			foreach ($rincian as $index => $value) {
				$rincian[$index]['servis_id'] = $id;
			}
			$this->servis->_table = 'servis_rincian';
			$this->servis->delete_by(['servis_id'=>$id]);
			$this->servis->batch_insert($rincian);

			$this->go('servis');
		}
	}
}
