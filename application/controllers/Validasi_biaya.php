<?php
defined('BASEPATH') or exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Validasi_biaya extends MY_Controller_admin
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Ekspedisi_model', 'model');
        $this->load->model('Kelengkapan_model', 'kelengkapan');
        $this->load->model('Biaya_model', 'biaya');
        $this->load->model('Kendaraan_model', 'kendaraan');
        $this->load->model('Pegawai_model', 'pegawai');
        $this->load->model('Area_model', 'area');
        $this->load->model('Balen_model', 'balen');
        $this->load->model('Kas_model', 'kas');
        $this->load->model('Customer_model', 'customer');
        $this->load->model('Tagihan_model', 'tagihan');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        
	}

	public function index()
	{
		if(empty($_GET['status'])){
            $_GET['status'] = 1;   
        }
        $param_kas = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
        $param_rek_pengirim = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
        $data['kas']        = json_decode(xcurl($param_kas)['response']);
        $data['rek_pengirim'] = json_decode(xcurl($param_rek_pengirim)['response']);

        $data['ekspedisi'] = $this->kas->join($this->pegawai->joins($this->model->get_data_validasi_biaya(), 'supir', 'pegawai_id'), 'kas', 'id_kas');
        $data['kendaraan'] = $this->kendaraan->get_active();
        $data['status'] = array(1 => 'Menunggu', 2 => 'Disetujui', 3 => 'Ditolak');
        $data['supir'] = $this->pegawai->get_active_driver()['rows'];
        $data['pembayaran'] = array('cash', 'transfer');
        $data['area'] = $this->area->order_by('kota')->get_all();
        // dump($data['kas']);
        set_session('title', 'Validasi Biaya');
        set_activemenu('sub-ekspedisi', 'menu-validasi-biaya');
        $this->render('ekspedisi/validasi_biaya/index', $data);
    }

    public function detail($id){
        $param_rek_penerima = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
        $data['rek_penerima'] = json_decode(xcurl($param_rek_penerima)['response']);
        $data['pembayaran'] = array('cash', 'transfer');
        $data['ekspedisi'] = $this->model->select("ekspedisi.*, no_pol, kota, is_sby, customer.nama as customer")->join("kendaraan", "kendaraan_id=kendaraan.id")->join("area", "area.id = id_area")->join('customer', 'customer_id = customer.id')->get_by(['ekspedisi.id' => $id]);
        $data['biaya'] = $this->biaya->get_by(['ekspedisi_id' => $id]);
        $data['balen'] = $this->kas->join($this->balen->get_data_balen($id), 'kas', 'transfer_rekening');
        $data['area'] = $this->area->order_by('kota')->get_all();
        $data['customer'] = $this->customer->get_active();
        $data['id'] = $id;
        set_session('title', 'Form Biaya');
        set_activemenu('sub-ekspedisi', 'menu-validasi-biaya');
        $this->render('ekspedisi/validasi_biaya/detail', $data);
    }
    
    public function form($token = '', $id = '')
    {
        if (!$this->validateToken($token) || empty($id)) {
            show_404();
        }

        $data['ekspedisi'] = $this->ekspedisi->select("ekspedisi.*, no_pol")->join("kendaraan", "kendaraan_id=kendaraan.id")->get_by(['ekspedisi.id' => $id]);
        $data['biaya'] = $this->biaya->get_by(['ekspedisi_id' => $id]);
        $data['area'] = $this->area->order_by('kota')->get_all();
        $data['balen'] = $this->balen->get_by(['ekspedisi_id' => $id]);
        set_session('title', 'Form Validasi');
        set_activemenu('sub-ekspedisi', 'menu-validasi-biaya');
        $this->render('ekspedisi/validasi_biaya/form', $data);
    }

    public function cetak_laporan_biaya()
    {
        $tahun = $this->input->get('tahun');
        $bulan = $this->input->get('bulan');
        $this->biaya->cetak($tahun, $bulan);
    }

    public function setuju_pengajuan(){
        $id = $this->input->post('id');
        $ekspedisi = $this->model->get_data_validasi_biaya($id);
        $balen = $this->kas->join($this->balen->get_data_balen($id), 'kas', 'transfer_rekening');

        # cek sisa uang saku, jika > 0 masuk ke buku kas dengan arus in. jika < 0 masuk ke buku kas dengan arus out
        $sisa_uangsaku = $ekspedisi['uang_saku'] - $ekspedisi['total_biaya'];

        if($sisa_uangsaku > 0){
            $arus_kas['id_perusahaan'] = $this->id_perusahaan;
            $arus_kas['id_kas'] = $ekspedisi['id_kas'];
            $arus_kas['tgl_transaksi'] = $ekspedisi['tanggal_kembali'];
            $arus_kas['arus'] = "in";
            $arus_kas['sumber'] = 'ekspedisi-kembali';
            $arus_kas['id_sumber'] = $id;
            $arus_kas['keterangan'] = 'Sisa uang saku '.$ekspedisi['no_resi'];
            $arus_kas['nominal'] = $sisa_uangsaku;
            $arus_kas['input_by'] = get_session('auth')['id'];
            $arus_kas['input_timestamp'] = setNewDateTime();
            $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
            $result = json_decode(xcurl($param_curl)['response']);
        }else{
            $arus_kas['id_perusahaan'] = $this->id_perusahaan;
            $arus_kas['id_kas'] = $ekspedisi['id_kas'];
            $arus_kas['tgl_transaksi'] = $ekspedisi['tanggal_kembali'];
            $arus_kas['arus'] = "in";
            $arus_kas['sumber'] = 'ekspedisi-kembali';
            $arus_kas['id_sumber'] = $id;
            $arus_kas['keterangan'] = 'Kurang bayar '.$ekspedisi['no_resi'];
            $arus_kas['nominal'] = abs($sisa_uangsaku);
            $arus_kas['input_by'] = get_session('auth')['id'];
            $arus_kas['input_timestamp'] = setNewDateTime();
            $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($arus_kas));
            $result = json_decode(xcurl($param_curl)['response']);
        }

        # insert data tagihan ekspedisi (ongkos ekspedisi)
        $ongkos = array(
            'tanggal' => $ekspedisi['tanggal_jalan'],
            'customer_id' => $ekspedisi['customer_id'],
            'sumber' => 'ekspedisi',
            'id_ekspedisi' => $ekspedisi['id'],
            'id_balen' => 0,
            'id_area' => $ekspedisi['id_area'],
            'pembayaran' => 'tbd',
            'id_kas' => 0,
            'subtotal' => $ekspedisi['biaya'],
            'timestamp' => setNewDateTime()
        );

        $result = $this->tagihan->insert($ongkos);
        
        # insert data tagihan balen (jika ada)
        if(!empty($balen)){
            foreach($balen as $row){
                $balen = array(
                    'tanggal' => $row['tanggal_balen'],
                    'customer_id' => $row['customer_id'],
                    'sumber' => 'balen',
                    'id_ekspedisi' => $ekspedisi['id'],
                    'id_balen' => $row['id'],
                    'id_area' => $row['id_area'],
                    'pembayaran' => $row['jenis_pembayaran'],
                    'id_kas' => $row['transfer_rekening'],
                    'subtotal' => $row['biaya'],
                    'timestamp' => setNewDateTime()
                );
                $result = $this->tagihan->insert($balen);
            }
        }
        # update status (is_done = 1)
        $update = array(
            'is_done' => 1,
            'acc_by' => $this->session->auth['id'],
            'timestamp_acc' => setNewDateTime()
        );
        $result = $this->model->update($id, $update);
        if($result){
            $this->message('Berhasil mengubah data', 'success');
        }else{
            $this->message('Gagal mengubah data', 'error');
        }
        $this->go('validasi_biaya');
    }
}