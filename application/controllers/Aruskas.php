<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Aruskas extends MY_Controller_admin
{
    public $bypass_auth = true;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
        $this->url_master = $this->config->item('url_master');
        $this->id_perusahaan = $this->config->item('id_perusahaan');
        $this->source = 'master-transaksi-aruskas';
        $this->role = json_decode($this->session->auth['role']);
    }

    public function index()
    {
        show_404();
    }

    public function kelola()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('kll-arsks', $hak->tran) || $hak->tran[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 1;
            }
            $query_string = http_build_query($this->input->get());
            $param_kas          = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rekening     = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_transaksi    = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/get_data_persetujuan?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan.'&'.$query_string);
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
            $data['rekening']   = json_decode(xcurl($param_rekening)['response']);
            $data['role']       = $this->role;
            // $data['customer']   = $this->customer->get_active();
            $data['arus']       = array("in" => "Arus Masuk", "out" => "Arus Keluar");
            $data['status']     = array(1 => "Menunggu Persetujuan", 2 => "Disetujui", 3 => "Ditolak");
            $data['transaksi']  = json_decode(xcurl($param_transaksi)['response']);
            // dump($data['transaksi']);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Buku Kas' => 'active', 'Kelola Transaksi' => 'active'));
            set_session('title', 'Kelola Transaksi Buku Kas');
            set_activemenu('sub-aruskas', 'menu-aruskas-kelola');
            $this->render('aruskas/v-kelola-transaksi-aruskas', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function persetujuan()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('acc-arsks', $hak->tran) || $hak->tran[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 1;
            }
            $query_string = http_build_query($this->input->get());
            $param_kas          = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rekening     = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_transaksi    = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/get_data_persetujuan?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan.'&'.$query_string);
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
            $data['rekening']   = json_decode(xcurl($param_rekening)['response']);
            $data['role']       = $this->role;
            // $data['customer']   = $this->customer->get_active();
            $data['arus']       = array("in" => "Arus Masuk", "out" => "Arus Keluar");
            $data['status']     = array(1 => "Menunggu Persetujuan", 2 => "Disetujui", 3 => "Ditolak");
            $data['transaksi']  = json_decode(xcurl($param_transaksi)['response']);
            // dump($data);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Buku Kas' => 'active', 'Persetujuan' => 'active'));
            set_session('title', 'Persetujuan Transaksi Buku Kas');
            set_activemenu('sub-aruskas', 'menu-aruskas-persetujuan');
            $this->render('aruskas/v-persetujuan-aruskas', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function rekap()
    {
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('rkp-arsks', $hak->tran) || $hak->tran[0] == '*') {
            $param_kas          = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rekening     = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
            $data['rekening']   = json_decode(xcurl($param_rekening)['response']);
            $data['role']       = $this->role;
            // $data['customer']   = $this->customer->get_active();
            $data['arus']       = array("in" => "Arus Masuk", "out" => "Arus Keluar");
            $data['status']     = array(1 => "Menunggu Persetujuan", 2 => "Disetujui", 3 => "Ditolak");
            if(!empty($_GET)){
                $_GET['status'] = 2;
                $query_string = http_build_query($this->input->get());
                $param_transaksi    = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/get_data_rekap?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan.'&'.$query_string);
                $data['transaksi']  = json_decode(xcurl($param_transaksi)['response']);
            }else{
                $data['transaksi'] = '';
            }

            if(!isset($data['transaksi']->saldo_awal[0]->total_in)){
                $data['total_in'] = 0;
            }else{
                $data['total_in'] = $data['transaksi']->saldo_awal[0]->total_in;
            }
            if(!isset($data['transaksi']->saldo_awal[0]->total_out)){
                $data['total_out'] = 0;
            }else{
                $data['total_out'] = $data['transaksi']->saldo_awal[0]->total_out;
            }
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Buku Kas' => 'active', 'Rekap Transaksi' => 'active'));
            set_session('title', 'Rekap Transaksi Buku Kas');
            set_activemenu('sub-aruskas', 'menu-aruskas-rekap');
            $this->render('aruskas/v-rekap-transaksi-aruskas', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function mutasi(){
        $hak = json_decode($this->session->auth['hak_akses']);
        if (in_array('mts-arsks', $hak->tran) || $hak->tran[0] == '*') {
            if (empty($this->input->get())) {
                $_GET['status'] = 1;
            }
            $query_string = http_build_query($this->input->get());
            $param_kas          = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_rekening     = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
            $param_transaksi    = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/get_data_mutasi?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan.'&'.$query_string);
            $data['kas']        = json_decode(xcurl($param_kas)['response']);
            $data['rekening']   = json_decode(xcurl($param_rekening)['response']);
            $data['role']       = $this->role;
            $data['transaksi']  = json_decode(xcurl($param_transaksi)['response']);
            set_session('breadcrumb', array('Dashboard' => base_url('dashboard'), 'Buku Kas' => 'active', 'Mutasi Akun' => 'active'));
            set_session('title', 'Mutasi Buku Kas');
            set_activemenu('sub-aruskas', 'menu-aruskas-mutasi');
            $this->render('aruskas/v-mutasi-akun', $data);
        } else {
            show_error("Anda tidak memiliki hak akses untuk halaman ini.");
        }
    }

    public function submit_transaksi()
    {
        $post = $this->input->post();
        if(empty($post['id'])){
            # Insert statement
            $data['id_perusahaan'] = $this->id_perusahaan;
            $data['id_kas'] = $post['id_kas'];
            $data['tgl_transaksi'] = $post['tgl_transaksi'];
            $data['arus'] = $post['arus'];
            $data['sumber'] = $this->source;
            $data['keterangan'] = $post['keterangan'];
            $data['nominal'] = str_replace(",", "", $post['nominal']);
            $data['input_by'] = get_session('auth')['id'];
            $data['input_timestamp'] = setNewDateTime();
            $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($data));
            $response = json_decode(xcurl($param_curl)['response']);
            if ($response) {
                $this->message('Berhasil membuat transaksi.', 'success');
            } else {
                $this->message('Gagal.', 'error');
            }
        }else{
            # Update statement
            $data['id'] = $post['id'];
            $data['id_kas'] = $post['id_kas'];
            $data['tgl_transaksi'] = $post['tgl_transaksi'];
            $data['arus'] = $post['arus'];
            $data['keterangan'] = $post['keterangan'];
            $data['nominal'] = str_replace(",", "", $post['nominal']);
            $data['input_by'] = get_session('auth')['id'];
            $data['input_timestamp'] = setNewDateTime();
            $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/update_transaksi?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($data));
            $response = json_decode(xcurl($param_curl)['response']);
            if ($response) {
                $this->message('Berhasil mengubah data.', 'success');
            } else {
                $this->message('Gagal.', 'error');
            }
        }

        $this->go('aruskas/kelola?'.$post['url']);
    }

    public function approve_transaksi()
    {
        $id = $this->input->post('id');
        $remark = $this->input->post('remark');
        if (!empty($id)) {
            $update = array(
                'id' => $id,
                'remark' => $remark,
                'is_approval' => 1,
                'approval_by' => get_session('auth')['id'],
                'approval_timestamp' => setNewDateTime(),
            );
            $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/update_transaksi?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($update));
            $response = json_decode(xcurl($param_curl)['response']);
            if ($response) {
                $this->message('Transaksi disetujui.', 'success');
            } else {
                $this->message('Gagal mengubah data', 'error');
            }
        } else {
            $response = false;
            $this->message('Terjadi kesalahan!', 'error');
        }

        echo json_encode($response);
    }

    public function decline_transaksi()
    {
        $id = $this->input->post('id');
        $remark = $this->input->post('remark');
        if (!empty($id)) {
            $update = array(
                'id' => $id,
                'remark' => $remark,
                'is_approval' => 2,
                'approval_by' => get_session('auth')['id'],
                'approval_timestamp' => setNewDateTime(),
            );
            $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/update_transaksi?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($update));
            $response = json_decode(xcurl($param_curl)['response']);
            if ($response) {
                $this->message('Transaksi ditolak.', 'success');
            } else {
                $this->message('Gagal mengubah data', 'error');
            }
        } else {
            $response = false;
            $this->message('Terjadi kesalahan!', 'error');
        }

        echo json_encode($response);
    }

    public function json_get_detail()
    {
        $id = $this->input->post('id');
        if(!empty($id)){
            $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/get_detail_transaksi?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query(array('id' => $id)));
            $response = json_decode(xcurl($param_curl)['response']);
        }else{
            $response = false;
        }
        echo json_encode($response);
    }

    public function delete_transaksi()
    {
        $id = $this->input->post('id');
        if(!empty($id)){
            $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/delete_transaksi?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query(array('id' => $id)));
            $response = json_decode(xcurl($param_curl)['response']);
            if($response){
                $this->message('Transaksi berhasil dihapus.', 'success');
            }else{
                $this->message('Gagal.', 'error');
            }
        }else{
            $response = false;
            $this->message('Gagal.', 'error');
        }
        echo json_encode($response);
    }

    public function submit_mutasi_akun()
    {
        $post = $this->input->post();
        # Insert transaksi mutasi
        $data['id_perusahaan'] = $this->id_perusahaan;
        $data['kas_from'] = $post['kas_from'];
        $data['kas_to'] = $post['kas_to'];
        $data['tanggal'] = $post['tanggal'];
        $data['keterangan'] = $post['keterangan'];
        $data['nominal'] = str_replace(",", "", $post['nominal']);
        $data['input_by'] = get_session('auth')['id'];
        $data['timestamp_input'] = setNewDateTime();
        $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert_mutasi_akun?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($data));
        $response = json_decode(xcurl($param_curl)['response']);
        if ($response != false) {
            # Insert transaksi-from arus kas
            $log_1['id_perusahaan'] = $this->id_perusahaan;
            $log_1['id_kas'] = $post['kas_from'];
            $log_1['tgl_transaksi'] = $post['tanggal'];
            $log_1['arus'] = 'out';
            $log_1['sumber'] = 'mutasi-akun';
            $log_1['id_sumber'] = $response;
            $log_1['keterangan'] = $post['keterangan'];
            $log_1['nominal'] = str_replace(",", "", $post['nominal']);
            $log_1['input_by'] = get_session('auth')['id'];
            $log_1['input_timestamp'] = setNewDateTime();
            $log_1['approval_timestamp'] = setNewDateTime();
            $log_1['is_approval'] = 1;
            $log_1['approval_by'] = get_session('auth')['id'];
            $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($log_1));
            xcurl($param_curl)['response'];

            # Insert transaksi-to arus kas
            $log_2['id_perusahaan'] = $this->id_perusahaan;
            $log_2['id_kas'] = $post['kas_to'];
            $log_2['tgl_transaksi'] = $post['tanggal'];
            $log_2['arus'] = 'in';
            $log_2['sumber'] = 'mutasi-akun';
            $log_2['id_sumber'] = $response;
            $log_2['keterangan'] = $post['keterangan'];
            $log_2['nominal'] = str_replace(",", "", $post['nominal']);
            $log_2['input_by'] = get_session('auth')['id'];
            $log_2['input_timestamp'] = setNewDateTime();
            $log_2['approval_timestamp'] = setNewDateTime();
            $log_2['is_approval'] = 1;
            $log_2['approval_by'] = get_session('auth')['id'];
            $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/insert?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query($log_2));
            xcurl($param_curl)['response'];
            $this->message('Berhasil membuat transaksi.', 'success');
        } else {
            $this->message('Gagal.', 'error');
        }

        $this->go('aruskas/mutasi?'.$post['url']);
    }

    public function delete_transaksi_mutasi(){
        $post = $this->input->post();
        $param_curl = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/get_detail_mutasi?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query(array('id' => $post['id'])));
        $response = json_decode(xcurl($param_curl)['response']);
        $result = false;
        if($response != false){
            foreach($response as $row){
                $param_delete = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/delete_detail_mutasi?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query(array('id' => $row->id)));
                $result = xcurl($param_delete)['response'];
            }
            if($result){
                $param_delete2 = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/delete_mutasi?token='.$this->session->auth['token'], CURLOPT_POSTFIELDS => http_build_query(array('id' => $post['id'])));
                $result = xcurl($param_delete2)['response'];
            }
        }

        if($result){
            $this->message('Berhasil menghapus transaksi.', 'success');
        }else{
            $this->message('Gagal menghapus transaksi', 'error');
        }
        echo json_encode($result);
    }

    public function export(){
        $get                = $this->input->get();
        $param_kas          = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_active?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
        $param_rekening     = array(CURLOPT_URL => $this->url_master.'api/external/kas/get_rekening?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan);
        $data['kas']        = json_decode(xcurl($param_kas)['response']);
        $data['rekening']   = json_decode(xcurl($param_rekening)['response']);
        $query_string = http_build_query($this->input->get());
        $param_transaksi    = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/get_data_export?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan.'&'.$query_string);
        $data['transaksi']  = json_decode(xcurl($param_transaksi)['response']);
        // dump($data['transaksi']->saldo_awal);
        $spreadsheet = new Spreadsheet();
        $rekap = array();
        $idx = 0;
        # Kas
        if(!empty($data['kas'])){
            foreach($data['kas'] as $row){
                if($idx == 0){
                    $spreadsheet->getActiveSheet()->setTitle($row->label);
                }else{
                    $worksheet = new Worksheet($spreadsheet, $row->label);
                    $spreadsheet->addSheet($worksheet, $idx);
                }
                $spreadsheet->setActiveSheetIndex($idx);
                $sheet = $spreadsheet->getActiveSheet($idx);
                $saldo_akhir = 0;
                $saldo_awal = 0;
                $total_masuk = 0;
                $total_keluar = 0;
                $kolom = 1;
                $baris = 1;
                # PROSES - HEADER
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tanggal');
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Kas');
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Sumber');
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Keterangan');
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Arus Masuk');
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Arus Keluar');
                $baris++;
                

                if(!empty($data['transaksi']->saldo_awal)){
                    foreach($data['transaksi']->saldo_awal as $s){
                        if($s->id_kas == $row->id){
                            $saldo_awal = $s->total_in - $s->total_out;
                            $kolom = 1;
                            $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                            $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                            $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                            $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Saldo Awal');
                            if($saldo_awal > 0){
                                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $saldo_awal);
                                $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                            }else{
                                $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $saldo_awal);
                            }
                            $saldo_akhir += $saldo_awal;
                            $baris++;
                        }
                    }
                }

                if(!empty($data['transaksi']->data)){
                    foreach($data['transaksi']->data as $d){
                        if($d->id_kas == $row->id){
                            $kolom = 1;
                            $sheet->setCellValueByColumnAndRow($kolom++, $baris, date("Y-m-d", strtotime($d->tgl_transaksi)));
                            $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row->label);
                            $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d->sumber);
                            $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d->keterangan);
                            if($d->arus == 'in'){
                                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d->nominal);
                                $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                                $saldo_akhir += $d->nominal;
                                $total_masuk += $d->nominal;
                            }else{
                                $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ($d->nominal*-1));
                                $saldo_akhir -= $d->nominal;
                                $total_keluar += $d->nominal;
                            }
                            $baris++;
                        }
                    }
                }

                # Set subtotal
                $baris++;
                $kolom = 1;
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Subtotal');
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $total_masuk);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ($total_keluar * -1));

                # Set saldo akhir
                $baris++;
                $kolom = 1;
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Saldo Akhir');
                if($saldo_akhir > 0){
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, $saldo_akhir);
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                }else{
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, $saldo_akhir);
                }

                foreach (range('A', $spreadsheet->getActiveSheet()->getHighestDataColumn()) as $col) {
                    $spreadsheet->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
                }
                array_push($rekap, array('kas' => $row->label, 'saldo_awal' => $saldo_awal, 'total_masuk' => $total_masuk, 'total_keluar' => $total_keluar, 'saldo_akhir' => $saldo_akhir));
                $idx++;
            }
        }
         
        # Rekening
        if(!empty($data['rekening'])){
            foreach($data['rekening'] as $row){
                if($idx == 0){
                    $spreadsheet->getActiveSheet()->setTitle(($row->bank.'-'.$row->no_rekening));
                }else{
                    $worksheet = new Worksheet($spreadsheet, ($row->bank.'-'.$row->no_rekening));
                    $spreadsheet->addSheet($worksheet, $idx);
                }
                $spreadsheet->setActiveSheetIndex($idx);
                $sheet = $spreadsheet->getActiveSheet($idx);
                $saldo_akhir = 0;
                $saldo_awal = 0;
                $total_masuk = 0;
                $total_keluar = 0;
                $kolom = 1;
                $baris = 1;
                # PROSES - HEADER
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tanggal');
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Rekening');
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Sumber');
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Keterangan');
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Arus Masuk');
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Arus Keluar');
                $baris++;
                

                if(!empty($data['transaksi']->saldo_awal)){
                    foreach($data['transaksi']->saldo_awal as $s){
                        if($s->id_kas == $row->id){
                            $saldo_awal = $s->total_in - $s->total_out;
                            $kolom = 1;
                            $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                            $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                            $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                            $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Saldo Awal');
                            if($saldo_awal > 0){
                                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $saldo_awal);
                                $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                            }else{
                                $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $saldo_awal);
                            }
                            $saldo_akhir += $saldo_awal;
                            $baris++;
                        }
                    }
                }

                if(!empty($data['transaksi']->data)){
                    foreach($data['transaksi']->data as $d){
                        if($d->id_kas == $row->id){
                            $kolom = 1;
                            $sheet->setCellValueByColumnAndRow($kolom++, $baris, date("Y-m-d", strtotime($d->tgl_transaksi)));
                            $sheet->setCellValueByColumnAndRow($kolom++, $baris, ($row->bank.'-'.$row->no_rekening));
                            $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d->sumber);
                            $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d->keterangan);
                            if($d->arus == 'in'){
                                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $d->nominal);
                                $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                                $saldo_akhir += $d->nominal;
                                $total_masuk += $d->nominal;
                            }else{
                                $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ($d->nominal*-1));
                                $saldo_akhir -= $d->nominal;
                                $total_keluar += $d->nominal;
                            }
                            $baris++;
                        }
                    }
                }

                # Set subtotal
                $baris++;
                $kolom = 1;
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Subtotal');
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $total_masuk);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, ($total_keluar * -1));

                # Set saldo akhir
                $baris++;
                $kolom = 1;
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Saldo Akhir');
                if($saldo_akhir > 0){
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, $saldo_akhir);
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                }else{
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, '');
                    $sheet->setCellValueByColumnAndRow($kolom++, $baris, $saldo_akhir);
                }

                foreach (range('A', $spreadsheet->getActiveSheet()->getHighestDataColumn()) as $col) {
                    $spreadsheet->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
                }

                array_push($rekap, array('kas' => ($row->bank.'-'.$row->no_rekening), 'saldo_awal' => $saldo_awal, 'total_masuk' => $total_masuk, 'total_keluar' => $total_keluar, 'saldo_akhir' => $saldo_akhir));
                $idx++;
                
            }
        }

        # Gabungan
        if(isset($get['start'])){
            $start = $get['start'];
        }else{
            $start = "-";
        }

        if(isset($get['end'])){
            $end = $get['end'];
        }else{
            $end = "-";
        }

        if($idx == 0){
            $spreadsheet->getActiveSheet()->setTitle('Rekap Akun');
        }else{
            $worksheet = new Worksheet($spreadsheet, 'Rekap Akun');
            $spreadsheet->addSheet($worksheet, $idx);
        }
        $spreadsheet->setActiveSheetIndex($idx);
        $sheet = $spreadsheet->getActiveSheet($idx);
        $baris = 1;
        $kolom = 1;
        # PROSES - HEADER
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tanggal Mulai');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Sampai Dengan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Akun Kas');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Saldo Awal');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Total Masuk');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Total Keluar');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Saldo Akhir');
        $baris++;
        if(!empty($rekap)){
            foreach($rekap as $r){
                $kolom = 1;
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $start);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $end);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $r['kas']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $r['saldo_awal']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $r['total_masuk']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $r['total_keluar']);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $r['saldo_akhir']);
                $baris++;
            }
        }

        foreach (range('A', $spreadsheet->getActiveSheet()->getHighestDataColumn()) as $col) {
            $spreadsheet->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }

        # SAVE
		$writer = new Xlsx($spreadsheet);
        $now = date('Ymdhis', strtotime(setNewDateTime()));
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="rekap_kas_'.$now.'.xlsx"');
		$writer->save("php://output");
    }

    public function export_mutasi(){
        $get                = $this->input->get();
        $query_string = http_build_query($this->input->get());
        $param_transaksi    = array(CURLOPT_URL => $this->url_master.'api/external/arus_kas/get_data_mutasi?token='.$this->session->auth['token'].'&id_perusahaan='.$this->id_perusahaan.'&'.$query_string);
        $data['transaksi']  = json_decode(xcurl($param_transaksi)['response']);
        // dump($data['transaksi']->saldo_awal);
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $rekap = array();
        $idx = 0;
        # Kas
       
        $kolom = 1;
        $baris = 1;
        # PROSES - HEADER
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Tanggal');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Kas Sumber');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Kas Tujuan');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Nominal');
        $sheet->setCellValueByColumnAndRow($kolom++, $baris, 'Keterangan');
        $baris++;
        

        if(!empty($data['transaksi'])){
            foreach($data['transaksi'] as $row){
                
                $kolom = 1;
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row->tanggal);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row->kas_from_label);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row->kas_to_label);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row->nominal);
                $sheet->setCellValueByColumnAndRow($kolom++, $baris, $row->keterangan);
                $baris++;
                
            }
        }

        # SAVE
        $writer = new Xlsx($spreadsheet);
        $now = date('Ymdhis', strtotime(setNewDateTime()));
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="rekap_mutasi_'.$now.'.xlsx"');
        $writer->save("php://output");
    }

}
