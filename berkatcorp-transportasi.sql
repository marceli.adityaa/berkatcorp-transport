-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 14, 2019 at 02:28 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `berkatcorp-transportasi`
--

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `id` int(11) NOT NULL,
  `kota` varchar(255) NOT NULL,
  `per_rit` int(11) NOT NULL,
  `per_ton` int(11) NOT NULL,
  `insert_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `insert_by` varchar(255) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_by` varchar(255) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `is_disabled` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`id`, `kota`, `per_rit`, `per_ton`, `insert_time`, `insert_by`, `update_time`, `update_by`, `is_deleted`, `is_disabled`) VALUES
(1, 'Surabaya', 175000, 150000, '2019-09-20 09:15:04', '', '2019-09-20 09:15:04', '', 0, 0),
(2, 'Kediri', 250000, 200000, '2019-09-20 09:15:04', '', '2019-09-20 09:15:04', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `biaya`
--

CREATE TABLE `biaya` (
  `id` int(11) NOT NULL,
  `no_resi` varchar(255) NOT NULL,
  `uang_saku` int(11) NOT NULL,
  `solar_pertama` int(11) NOT NULL,
  `solar` int(11) NOT NULL,
  `timbangan` int(11) NOT NULL,
  `tol` int(11) NOT NULL,
  `polisi` int(11) NOT NULL,
  `parkir` int(11) NOT NULL,
  `masuk_ruko` int(11) NOT NULL,
  `kuli` int(11) NOT NULL,
  `makan` int(11) NOT NULL,
  `pengurus_pelabuhan` int(11) NOT NULL,
  `pak_serap` int(11) NOT NULL,
  `lain` int(11) NOT NULL,
  `ongkosan` int(11) NOT NULL,
  `balen` int(11) NOT NULL,
  `tanggal_verifikasi` datetime NOT NULL,
  `insert_by` varchar(255) NOT NULL,
  `insert_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_by` varchar(255) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `is_disabled` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ekspedisi`
--

CREATE TABLE `ekspedisi` (
  `id` int(11) NOT NULL,
  `no_resi` varchar(255) NOT NULL,
  `kendaraan_id` int(11) NOT NULL,
  `pegawai_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `tanggal_jalan` date NOT NULL,
  `tujuan` varchar(255) DEFAULT NULL,
  `jenis_barang` varchar(255) NOT NULL,
  `berat` decimal(18,2) NOT NULL,
  `jenis_perhitungan_berat` varchar(10) NOT NULL,
  `biaya` int(11) NOT NULL,
  `tanggal_entri` datetime DEFAULT NULL,
  `tanggal_cek_berangkat` datetime DEFAULT NULL,
  `tanggal_cek_pulang` datetime DEFAULT NULL,
  `tanggal_input_biaya` datetime DEFAULT NULL,
  `tanggal_validasi_biaya` datetime DEFAULT NULL,
  `tanggal_verifikasi_akhir` datetime DEFAULT NULL,
  `is_done` tinyint(1) NOT NULL DEFAULT 0,
  `insert_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `insert_by` varchar(255) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `update_by` varchar(255) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `is_disabled` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ekspedisi`
--

INSERT INTO `ekspedisi` (`id`, `no_resi`, `kendaraan_id`, `pegawai_id`, `customer_id`, `tanggal_jalan`, `tujuan`, `jenis_barang`, `berat`, `jenis_perhitungan_berat`, `biaya`, `tanggal_entri`, `tanggal_cek_berangkat`, `tanggal_cek_pulang`, `tanggal_input_biaya`, `tanggal_validasi_biaya`, `tanggal_verifikasi_akhir`, `is_done`, `insert_time`, `insert_by`, `update_time`, `update_by`, `is_deleted`, `is_disabled`) VALUES
(1, 'RS191008-EHF', 5, 2, 1, '2019-10-08', 'Kediri', '1', '10.00', 'rit', 250000, '2019-10-08 14:58:05', '2019-10-11 12:17:30', '2019-10-11 14:03:48', NULL, NULL, NULL, 0, '2019-10-08 07:58:05', '', '2019-10-12 05:33:30', '', 0, 0),
(2, 'RS191010-XFH', 4, 2, 2, '2019-10-13', 'Kediri', '2', '50.00', 'tonase', 10000000, '2019-10-10 17:06:01', '2019-10-11 14:09:44', '2019-10-11 14:10:29', NULL, NULL, NULL, 0, '2019-10-10 10:06:01', '', '2019-10-11 07:10:29', '', 0, 0),
(3, 'RS191011-VB4', 5, 1, 1, '2019-10-16', 'Kediri', '1', '12.00', 'tonase', 2400000, '2019-10-11 14:11:10', '2019-10-11 14:25:38', NULL, NULL, NULL, NULL, 0, '2019-10-11 07:11:10', '', '2019-10-11 07:25:38', '', 0, 0),
(4, 'RS191012-ZTP', 5, 1, 1, '2019-10-26', 'Kediri', '1', '100.00', 'tonase', 20000000, '2019-10-12 12:38:56', NULL, NULL, NULL, NULL, NULL, 0, '2019-10-12 05:38:56', '', '2019-10-12 05:38:56', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kelengkapan`
--

CREATE TABLE `kelengkapan` (
  `id` int(11) NOT NULL,
  `ekspedisi_id` int(11) NOT NULL,
  `pemeriksa` varchar(255) NOT NULL,
  `jenis_cek` varchar(255) NOT NULL,
  `km` int(11) NOT NULL,
  `pemadam` tinyint(1) NOT NULL DEFAULT 0,
  `dongkrak` tinyint(1) NOT NULL DEFAULT 0,
  `kunci_roda` tinyint(1) NOT NULL DEFAULT 0,
  `stang_pendek` tinyint(1) NOT NULL DEFAULT 0,
  `stang_panjang` tinyint(1) NOT NULL DEFAULT 0,
  `terpal` tinyint(1) NOT NULL DEFAULT 0,
  `tampar` tinyint(1) NOT NULL DEFAULT 0,
  `p3k` tinyint(1) NOT NULL DEFAULT 0,
  `ban_serep` tinyint(1) NOT NULL DEFAULT 0,
  `buku_kir` tinyint(1) NOT NULL DEFAULT 0,
  `stnk` tinyint(1) NOT NULL DEFAULT 0,
  `surat_jalan` tinyint(1) NOT NULL DEFAULT 0,
  `cek_fisik` tinyint(1) NOT NULL DEFAULT 0,
  `insert_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `insert_by` varchar(255) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `update_by` varchar(255) NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelengkapan`
--

INSERT INTO `kelengkapan` (`id`, `ekspedisi_id`, `pemeriksa`, `jenis_cek`, `km`, `pemadam`, `dongkrak`, `kunci_roda`, `stang_pendek`, `stang_panjang`, `terpal`, `tampar`, `p3k`, `ban_serep`, `buku_kir`, `stnk`, `surat_jalan`, `cek_fisik`, `insert_time`, `insert_by`, `update_time`, `update_by`, `is_deleted`) VALUES
(1, 1, 'Ainul Yaqin', 'berangkat', 1200, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, '2019-10-11 05:17:30', '', '2019-10-11 05:17:30', '', 0),
(3, 1, 'Ainul Yaqin', 'pulang', 12000, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2019-10-11 07:03:48', '', '2019-10-11 07:03:48', '', 0),
(4, 2, 'Ainul Yaqin', 'berangkat', 12300, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2019-10-11 07:09:44', '', '2019-10-11 07:09:44', '', 0),
(5, 2, 'Ainul Yaqin', 'pulang', 12345, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, '2019-10-11 07:10:29', '', '2019-10-11 07:10:29', '', 0),
(6, 3, 'Ainul Yaqin', 'berangkat', 1200, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2019-10-11 07:25:38', '', '2019-10-11 07:25:38', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `kendaraan`
--

CREATE TABLE `kendaraan` (
  `id` int(11) NOT NULL,
  `uuid` varchar(5) NOT NULL,
  `no_pol` varchar(255) NOT NULL,
  `insert_by` int(11) NOT NULL,
  `insert_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_by` int(11) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_disabled` tinyint(1) NOT NULL DEFAULT 0,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kendaraan`
--

INSERT INTO `kendaraan` (`id`, `uuid`, `no_pol`, `insert_by`, `insert_time`, `update_by`, `update_time`, `is_disabled`, `is_deleted`) VALUES
(4, '11pny', 'ABC', 0, '2019-09-07 06:00:38', 0, '2019-10-03 06:55:11', 0, 0),
(5, '', 'P 3098 AB', 0, '2019-10-08 06:44:10', 0, '2019-10-08 06:44:10', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `uuid` varchar(10) NOT NULL,
  `label` varchar(255) NOT NULL,
  `deskripsi` varchar(255) DEFAULT NULL,
  `icon` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `tautan` text NOT NULL,
  `child_of` int(11) NOT NULL DEFAULT 0,
  `urutan` int(11) NOT NULL,
  `is_disabled` tinyint(1) NOT NULL DEFAULT 0,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `uuid`, `label`, `deskripsi`, `icon`, `class`, `tautan`, `child_of`, `urutan`, `is_disabled`, `is_deleted`) VALUES
(1, 'dsb', 'dashboard', NULL, 'icon-home', '', 'dashboard', 0, 1, 0, 0),
(2, 'kdr', 'kendaraan', NULL, 'icon-speedometer', '', '', 0, 2, 0, 0),
(3, 'kdrdt', 'master data', NULL, 'icon-list', '', 'kendaraan', 2, 3, 0, 0),
(4, 'kdrsv', 'servis kendaraan', NULL, 'icon-wrench', '', 'servis', 2, 4, 0, 0),
(5, 'eks', 'ekspedisi', NULL, 'fa fa-truck', '', '', 0, 5, 0, 0),
(6, 'brkt', 'Data Ekspedisi', NULL, 'icon-directions', '', 'ekspedisi', 5, 6, 0, 0),
(7, 'klp', 'cek kelengkapan', NULL, 'icon-check', '', 'ekspedisi/kelengkapan', 5, 7, 0, 0),
(8, 'bya', 'entri biaya', NULL, 'icon-note', '', 'ekspedisi/biaya', 5, 8, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `servis`
--

CREATE TABLE `servis` (
  `id` int(11) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `kendaraan_id` int(11) NOT NULL,
  `pegawai_id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `lokasi` varchar(255) NOT NULL,
  `mekanik` varchar(255) NOT NULL,
  `keterangan` text DEFAULT NULL,
  `insert_by` int(11) NOT NULL,
  `insert_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_by` int(11) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `servis`
--

INSERT INTO `servis` (`id`, `uuid`, `kendaraan_id`, `pegawai_id`, `tanggal`, `lokasi`, `mekanik`, `keterangan`, `insert_by`, `insert_time`, `update_by`, `update_time`, `is_deleted`) VALUES
(1, 'SV190921-TXM', 4, 2, '2019-09-21', 'Jember', 'Supriadi', 'Maintenace rutin', 0, '2019-09-21 08:29:21', 0, '2019-09-21 08:29:21', 0);

-- --------------------------------------------------------

--
-- Table structure for table `servis_rincian`
--

CREATE TABLE `servis_rincian` (
  `id` int(11) NOT NULL,
  `servis_id` int(11) NOT NULL,
  `perbaikan` varchar(255) NOT NULL,
  `jumlah` int(11) NOT NULL DEFAULT 1,
  `biaya` int(11) NOT NULL,
  `keterangan` text DEFAULT NULL,
  `insert_by` int(11) NOT NULL,
  `insert_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_by` int(11) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `servis_rincian`
--

INSERT INTO `servis_rincian` (`id`, `servis_id`, `perbaikan`, `jumlah`, `biaya`, `keterangan`, `insert_by`, `insert_time`, `update_by`, `update_time`, `is_deleted`) VALUES
(4, 1, 'Ganti oli', 1, 50000, 'Oli berkualitas', 0, '2019-09-24 06:10:10', 0, '2019-09-24 06:10:10', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `biaya`
--
ALTER TABLE `biaya`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ekspedisi`
--
ALTER TABLE `ekspedisi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kelengkapan`
--
ALTER TABLE `kelengkapan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kendaraan`
--
ALTER TABLE `kendaraan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `servis`
--
ALTER TABLE `servis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `servis_rincian`
--
ALTER TABLE `servis_rincian`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `biaya`
--
ALTER TABLE `biaya`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ekspedisi`
--
ALTER TABLE `ekspedisi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `kelengkapan`
--
ALTER TABLE `kelengkapan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `kendaraan`
--
ALTER TABLE `kendaraan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `servis`
--
ALTER TABLE `servis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `servis_rincian`
--
ALTER TABLE `servis_rincian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
