-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 09, 2021 at 06:56 AM
-- Server version: 10.1.39-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `berkatcorp-transportasi`
--

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `id` int(11) NOT NULL,
  `kota` varchar(255) NOT NULL,
  `is_sby` tinyint(1) NOT NULL,
  `per_rit` int(11) NOT NULL,
  `per_ton` int(11) NOT NULL,
  `insert_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `insert_by` varchar(255) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(255) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `is_disabled` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`id`, `kota`, `is_sby`, `per_rit`, `per_ton`, `insert_time`, `insert_by`, `update_time`, `update_by`, `is_deleted`, `is_disabled`) VALUES
(1, 'Surabaya', 1, 175000, 150000, '2019-09-20 09:15:04', '', '2019-09-20 09:15:04', '', 0, 0),
(2, 'Kediri', 0, 250000, 200000, '2019-09-20 09:15:04', '', '2019-09-20 09:15:04', '', 0, 0),
(3, 'Jember', 0, 150000, 120000, '2021-01-22 10:47:38', '', '2021-01-22 10:47:38', '', 0, 0),
(4, 'Banyuwangi', 0, 150000, 13000, '2021-01-26 05:19:33', '', '2021-01-26 05:19:33', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `balen`
--

CREATE TABLE `balen` (
  `id` int(11) NOT NULL,
  `ekspedisi_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `tanggal_balen` date NOT NULL,
  `id_area` tinyint(4) NOT NULL,
  `jenis_pembayaran` enum('cash','transfer','tbd') NOT NULL,
  `transfer_rekening` int(11) NOT NULL,
  `jenis_barang` varchar(255) NOT NULL,
  `keterangan` text NOT NULL,
  `jenis_perhitungan_berat` varchar(255) NOT NULL,
  `berat` decimal(18,2) NOT NULL,
  `harga_satuan` int(11) NOT NULL,
  `biaya` int(11) NOT NULL,
  `insert_time` int(11) NOT NULL,
  `insert_by` varchar(255) NOT NULL,
  `update_time` int(11) NOT NULL,
  `update_by` varchar(255) NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `balen`
--

INSERT INTO `balen` (`id`, `ekspedisi_id`, `customer_id`, `tanggal_balen`, `id_area`, `jenis_pembayaran`, `transfer_rekening`, `jenis_barang`, `keterangan`, `jenis_perhitungan_berat`, `berat`, `harga_satuan`, `biaya`, `insert_time`, `insert_by`, `update_time`, `update_by`, `is_deleted`) VALUES
(7, 4, 1, '2021-02-21', 1, 'cash', 0, 'pakan ayam', '', 'rit', '2.00', 800000, 1600000, 2021, '1', 0, '', 0),
(8, 4, 1, '2021-02-22', 1, 'cash', 0, 'Besi', 'abc', 'tonase', '3000.00', 1000, 3000000, 2021, '1', 0, '', 0),
(9, 2, 2, '2021-02-24', 1, 'cash', 0, 'abc', '', 'rit', '1.00', 1000000, 1000000, 2021, '1', 0, '', 0),
(10, 2, 2, '2021-02-24', 1, 'cash', 0, 'pakan', '', 'tonase', '3000.00', 90, 270000, 2021, '1', 0, '', 0),
(11, 4, 1, '2021-03-02', 1, 'transfer', 8, 'pakan', '', 'rit', '1.00', 1000000, 1000000, 2021, '1', 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `beban`
--

CREATE TABLE `beban` (
  `id` int(11) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `id_beban` int(11) NOT NULL,
  `nominal` int(11) NOT NULL,
  `catatan` text NOT NULL,
  `jenis_pembayaran` enum('cash','transfer','tbd') NOT NULL,
  `id_kas` int(11) NOT NULL,
  `is_verifikasi` tinyint(1) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL,
  `verifikasi_by` int(11) NOT NULL,
  `timestamp_verifikasi` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `biaya`
--

CREATE TABLE `biaya` (
  `id` int(11) NOT NULL,
  `ekspedisi_id` varchar(255) NOT NULL,
  `solar` int(11) NOT NULL,
  `timbangan` int(11) NOT NULL,
  `tol` int(11) NOT NULL,
  `polisi` int(11) NOT NULL,
  `parkir` int(11) NOT NULL,
  `masuk_ruko` int(11) NOT NULL,
  `kuli` int(11) NOT NULL,
  `makan` int(11) NOT NULL,
  `pengurus_pelabuhan` int(11) NOT NULL,
  `pak_serap` int(11) NOT NULL,
  `lain` int(11) NOT NULL,
  `upah` int(11) NOT NULL,
  `insert_by` varchar(255) NOT NULL,
  `insert_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(255) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `is_disabled` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `biaya`
--

INSERT INTO `biaya` (`id`, `ekspedisi_id`, `solar`, `timbangan`, `tol`, `polisi`, `parkir`, `masuk_ruko`, `kuli`, `makan`, `pengurus_pelabuhan`, `pak_serap`, `lain`, `upah`, `insert_by`, `insert_time`, `update_by`, `update_time`, `is_deleted`, `is_disabled`) VALUES
(1, '3', 150000, 50000, 35000, 100000, 3000, 25000, 20000, 100000, 30000, 100000, 10000, 56000, '', '2019-10-21 06:12:29', '', '2019-10-25 07:21:51', 0, 0),
(2, '4', 120000, 50000, 45000, 150000, 5000, 20000, 100000, 200000, 50000, 100000, 150000, 250000, '', '2019-10-28 05:13:16', '', '2021-02-23 10:20:33', 0, 0),
(3, '8', 124000, 50000, 0, 0, 0, 0, 100000, 120000, 0, 0, 60000, 250000, '', '2019-10-30 08:35:24', '', '2019-10-30 08:45:44', 0, 0),
(4, '9', 120000, 50000, 3000, 10000, 10000, 25000, 100000, 50000, 20000, 12000, 70000, 250000, '', '2019-10-31 02:44:27', '', '2019-10-31 02:45:48', 0, 0),
(5, '2', 120000, 0, 0, 1000000, 0, 0, 0, 0, 0, 0, 0, 200000, '', '2021-02-25 07:47:46', '', '2021-03-01 09:47:31', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `alamat` text,
  `telpon` varchar(255) DEFAULT NULL,
  `insert_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `insert_by` varchar(255) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(255) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `is_disabled` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `nama`, `alamat`, `telpon`, `insert_time`, `insert_by`, `update_time`, `update_by`, `is_deleted`, `is_disabled`) VALUES
(1, 'PT.BAS', 'Jalan Palsu, Jember', '088899991234', '2019-10-30 03:54:51', '', '2021-03-03 07:01:52', '', 0, 0),
(2, 'CV.BFI', 'Jalan Raya, Jember', '123456789', '2019-10-30 04:04:56', '', '2021-03-03 03:37:49', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ekspedisi`
--

CREATE TABLE `ekspedisi` (
  `id` int(11) NOT NULL,
  `no_resi` varchar(255) NOT NULL,
  `kendaraan_id` int(11) NOT NULL,
  `pegawai_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `tanggal_jalan` date NOT NULL,
  `id_area` int(11) NOT NULL,
  `uang_saku` int(11) NOT NULL,
  `solar_pertama` int(11) NOT NULL,
  `jenis_pembayaran` enum('cash','transfer','tbd') NOT NULL DEFAULT 'tbd',
  `id_kas` int(11) NOT NULL,
  `tujuan` varchar(255) DEFAULT NULL,
  `jenis_barang` varchar(255) NOT NULL,
  `catatan` text NOT NULL,
  `berat_muatan` int(11) NOT NULL,
  `berat` double NOT NULL,
  `jenis_perhitungan_berat` varchar(10) NOT NULL,
  `harga_satuan` int(11) NOT NULL,
  `biaya` int(11) NOT NULL,
  `tanggal_entri` datetime DEFAULT NULL,
  `tanggal_validasi` datetime NOT NULL,
  `tanggal_cek_berangkat` datetime DEFAULT NULL,
  `tanggal_cek_pulang` datetime DEFAULT NULL,
  `tanggal_input_biaya` datetime DEFAULT NULL,
  `tanggal_validasi_biaya` datetime DEFAULT NULL,
  `tanggal_verifikasi_akhir` datetime DEFAULT NULL,
  `status_berangkat` tinyint(4) NOT NULL,
  `status_muatan` tinyint(1) NOT NULL,
  `status_biaya` tinyint(4) NOT NULL,
  `is_done` tinyint(1) NOT NULL DEFAULT '0',
  `has_balen` tinyint(4) NOT NULL,
  `insert_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `insert_by` varchar(255) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(255) NOT NULL,
  `acc_by` int(11) NOT NULL,
  `timestamp_acc` datetime NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `is_disabled` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ekspedisi`
--

INSERT INTO `ekspedisi` (`id`, `no_resi`, `kendaraan_id`, `pegawai_id`, `customer_id`, `tanggal_jalan`, `id_area`, `uang_saku`, `solar_pertama`, `jenis_pembayaran`, `id_kas`, `tujuan`, `jenis_barang`, `catatan`, `berat_muatan`, `berat`, `jenis_perhitungan_berat`, `harga_satuan`, `biaya`, `tanggal_entri`, `tanggal_validasi`, `tanggal_cek_berangkat`, `tanggal_cek_pulang`, `tanggal_input_biaya`, `tanggal_validasi_biaya`, `tanggal_verifikasi_akhir`, `status_berangkat`, `status_muatan`, `status_biaya`, `is_done`, `has_balen`, `insert_time`, `insert_by`, `update_time`, `update_by`, `acc_by`, `timestamp_acc`, `is_deleted`, `is_disabled`) VALUES
(2, 'RS191010-XFH', 4, 135, 1, '2019-10-13', 1, 1500000, 300000, 'cash', 7, 'Kediri', '', '', 3575, 50, 'tonase', 0, 10000000, '2019-10-10 17:06:01', '2021-02-15 13:23:27', '2019-10-11 14:09:44', '2019-10-11 14:10:29', NULL, NULL, NULL, 1, 1, 0, 0, 1, '2019-10-10 10:06:01', '', '2021-03-03 06:49:19', '1', 0, '0000-00-00 00:00:00', 0, 0),
(4, 'RS191012-ZTP', 5, 155, 1, '2019-09-26', 1, 0, 0, 'tbd', 7, 'Surabaya', 'beras', '', 1000, 10, 'tonase', 0, 1500000, '2019-10-12 12:38:56', '0000-00-00 00:00:00', '2019-10-16 16:32:08', '2019-10-21 16:03:22', '2021-03-06 16:38:09', '2019-10-28 12:18:54', NULL, 1, 0, 1, 1, 1, '2019-10-12 05:38:56', '', '2021-03-12 10:22:45', '', 1, '2021-03-12 17:22:45', 0, 0),
(8, 'RS191030-2HB', 5, 135, 2, '2019-10-30', 3, 1300000, 200000, 'transfer', 8, 'Kediri', 'jangung', '', 1925, 1, 'tonase', 0, 200000, '2019-10-30 15:04:30', '2021-02-16 14:57:31', '2019-10-30 15:06:54', '2019-10-30 15:30:01', '2021-02-23 17:53:19', '2019-10-30 15:45:44', NULL, 1, 0, 1, 0, 0, '2019-10-30 08:04:30', '', '2021-04-16 08:52:16', '1', 0, '0000-00-00 00:00:00', 0, 0),
(9, 'RS191031-LW9', 4, 1, 1, '2019-10-31', 1, 0, 0, 'tbd', 7, 'Surabaya', 'beras', '', 1, 10, 'rit', 0, 175000, '2019-10-31 09:41:56', '0000-00-00 00:00:00', '2019-10-31 09:42:26', '2019-10-31 09:42:58', '2019-10-31 09:44:27', '2019-10-31 09:45:48', '2019-10-31 15:56:53', 2, 0, 0, 0, 0, '2019-10-31 02:41:56', '', '2021-03-10 10:54:30', '', 0, '0000-00-00 00:00:00', 0, 0),
(12, 'RS210203-P7J', 4, 155, 1, '2021-02-03', 3, 1200000, 200000, 'cash', 7, NULL, 'beras', 'tes', 4000, 0, 'tonase', 120, 0, '2021-02-03 12:11:37', '2021-02-15 17:31:55', '2021-02-15 17:34:49', '2021-02-15 17:40:36', NULL, NULL, NULL, 1, 0, 0, 0, 0, '2021-02-03 05:11:37', '', '2021-03-10 10:54:28', '1', 0, '0000-00-00 00:00:00', 0, 0),
(13, 'RS210216-1LC', 4, 87, 1, '2021-02-16', 1, 1300000, 200000, 'transfer', 8, NULL, 'beras', '...', 10000, 0, 'tonase', 150, 1500000, '2021-02-16 15:02:27', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2021-02-16 08:02:27', '', '2021-03-03 06:49:51', '', 0, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `hutang`
--

CREATE TABLE `hutang` (
  `id` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `tgl_hutang` date NOT NULL,
  `nominal_hutang` bigint(20) NOT NULL,
  `keterangan` text NOT NULL,
  `is_paid_off` tinyint(1) NOT NULL,
  `is_verifikasi` tinyint(1) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL,
  `verifikasi_by` int(11) NOT NULL,
  `timestamp_verifikasi` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hutang_detail`
--

CREATE TABLE `hutang_detail` (
  `id` int(11) NOT NULL,
  `id_hutang` int(11) NOT NULL,
  `source` varchar(100) NOT NULL,
  `id_source` int(11) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `tambah_hutang` bigint(20) NOT NULL,
  `bayar_pokok` bigint(20) NOT NULL,
  `bayar_bunga` bigint(20) NOT NULL,
  `pokok_awal` bigint(20) NOT NULL,
  `sisa_pokok` bigint(20) NOT NULL,
  `remark` text NOT NULL,
  `jenis_pembayaran` enum('cash','transfer','tbd') NOT NULL,
  `id_kas` int(11) NOT NULL,
  `is_verifikasi` tinyint(1) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL,
  `verifikasi_by` int(11) NOT NULL,
  `timestamp_verifikasi` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kelengkapan`
--

CREATE TABLE `kelengkapan` (
  `id` int(11) NOT NULL,
  `ekspedisi_id` int(11) NOT NULL,
  `pemeriksa` varchar(255) NOT NULL,
  `jenis_cek` varchar(255) NOT NULL,
  `km` int(11) NOT NULL,
  `pemadam` tinyint(1) NOT NULL DEFAULT '0',
  `dongkrak` tinyint(1) NOT NULL DEFAULT '0',
  `kunci_roda` tinyint(1) NOT NULL DEFAULT '0',
  `stang_pendek` tinyint(1) NOT NULL DEFAULT '0',
  `stang_panjang` tinyint(1) NOT NULL DEFAULT '0',
  `terpal` tinyint(1) NOT NULL DEFAULT '0',
  `tampar` tinyint(1) NOT NULL DEFAULT '0',
  `p3k` tinyint(1) NOT NULL DEFAULT '0',
  `ban_serep` tinyint(1) NOT NULL DEFAULT '0',
  `buku_kir` tinyint(1) NOT NULL DEFAULT '0',
  `stnk` tinyint(1) NOT NULL DEFAULT '0',
  `surat_jalan` tinyint(1) NOT NULL DEFAULT '0',
  `cek_fisik` tinyint(1) NOT NULL DEFAULT '0',
  `insert_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `insert_by` varchar(255) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(255) NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelengkapan`
--

INSERT INTO `kelengkapan` (`id`, `ekspedisi_id`, `pemeriksa`, `jenis_cek`, `km`, `pemadam`, `dongkrak`, `kunci_roda`, `stang_pendek`, `stang_panjang`, `terpal`, `tampar`, `p3k`, `ban_serep`, `buku_kir`, `stnk`, `surat_jalan`, `cek_fisik`, `insert_time`, `insert_by`, `update_time`, `update_by`, `is_deleted`) VALUES
(1, 1, 'Ainul Yaqin', 'berangkat', 1200, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, '2019-10-11 05:17:30', '', '2019-10-11 05:17:30', '', 0),
(3, 1, 'Ainul Yaqin', 'pulang', 12000, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2019-10-11 07:03:48', '', '2019-10-11 07:03:48', '', 0),
(4, 2, 'Ainul Yaqin', 'berangkat', 12300, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2019-10-11 07:09:44', '', '2019-10-11 07:09:44', '', 0),
(5, 2, 'Ainul Yaqin', 'pulang', 12345, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, '2019-10-11 07:10:29', '', '2019-10-11 07:10:29', '', 0),
(6, 3, 'Ainul Yaqin', 'berangkat', 1200, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2019-10-11 07:25:38', '', '2019-10-11 07:25:38', '', 0),
(7, 4, 'Ainul Yaqin', 'berangkat', 120, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, '2019-10-16 09:32:08', '', '2021-03-23 11:17:44', '', 0),
(8, 3, 'Ainul Yaqin', 'pulang', 120, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2019-10-16 09:32:20', '', '2019-10-16 09:32:20', '', 0),
(9, 4, 'Ainul Yaqin', 'pulang', 1230, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2019-10-21 09:03:22', '', '2019-10-21 09:03:22', '', 0),
(10, 8, 'Ainul Yaqin', 'berangkat', 100, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2019-10-30 08:06:33', '', '2019-10-30 08:06:33', '', 0),
(11, 8, 'Ainul Yaqin', 'berangkat', 100, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2019-10-30 08:06:54', '', '2019-10-30 08:06:54', '', 0),
(12, 8, 'Ainul Yaqin', 'pulang', 120, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2019-10-30 08:30:01', '', '2019-10-30 08:30:01', '', 0),
(13, 9, 'Ainul Yaqin', 'berangkat', 200, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2019-10-31 02:42:26', '', '2019-10-31 02:42:26', '', 0),
(14, 9, 'Ainul Yaqin', 'pulang', 230, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2019-10-31 02:42:58', '', '2019-10-31 02:42:58', '', 0),
(15, 12, 'marcel', 'berangkat', 1234, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2021-02-03 06:41:23', '', '2021-02-03 06:41:23', '', 0),
(16, 12, 'marcel', 'pulang', 1432, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2021-02-03 06:41:48', '', '2021-02-03 06:41:48', '', 0),
(17, 12, 'marcel', 'berangkat', 25980, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2021-02-15 10:34:49', '', '2021-02-15 10:34:49', '', 0),
(18, 12, 'marcel', 'pulang', 31000, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2021-02-15 10:40:36', '', '2021-02-15 10:40:36', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `kendaraan`
--

CREATE TABLE `kendaraan` (
  `id` int(11) NOT NULL,
  `uuid` varchar(5) NOT NULL,
  `no_pol` varchar(255) NOT NULL,
  `no_mesin` varchar(100) NOT NULL,
  `merk` varchar(100) NOT NULL,
  `tipe` varchar(100) NOT NULL,
  `warna` varchar(100) NOT NULL,
  `tahun` int(11) NOT NULL,
  `stnk_terbayar` date NOT NULL,
  `stnk_jatuh_tempo` date NOT NULL,
  `pajak_terbayar` date NOT NULL,
  `pajak_jatuh_tempo` date NOT NULL,
  `bpkb_terbayar` date NOT NULL,
  `bpkb_jatuh_tempo` date NOT NULL,
  `kir_terbayar` date NOT NULL,
  `kir_jatuh_tempo` date NOT NULL,
  `insert_by` int(11) NOT NULL,
  `insert_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_disabled` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kendaraan`
--

INSERT INTO `kendaraan` (`id`, `uuid`, `no_pol`, `no_mesin`, `merk`, `tipe`, `warna`, `tahun`, `stnk_terbayar`, `stnk_jatuh_tempo`, `pajak_terbayar`, `pajak_jatuh_tempo`, `bpkb_terbayar`, `bpkb_jatuh_tempo`, `kir_terbayar`, `kir_jatuh_tempo`, `insert_by`, `insert_time`, `update_by`, `update_time`, `is_disabled`, `is_deleted`) VALUES
(4, '', 'P 3098 AB', 'nomor12345', 'Honda', 'CRV', 'Hitam', 2019, '2019-03-25', '2021-03-27', '2019-03-25', '2021-04-10', '2021-09-06', '2026-09-06', '2021-09-06', '2021-09-10', 0, '2019-10-08 06:44:10', 0, '2021-09-06 09:49:31', 0, 0),
(6, '', 'P999BK', '123456', 'Isuzu', 'Panther', 'Grey', 2015, '2021-03-19', '2025-04-10', '2021-03-18', '2021-04-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 0, '2021-03-26 02:29:01', 0, '2021-03-30 10:23:34', 0, 0),
(7, '', 'P8989BK', '12357890', 'Mazda', 'CX5', 'Merah', 2016, '2021-03-25', '2021-04-05', '2021-03-25', '2021-04-01', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 0, '2021-03-26 02:30:46', 0, '2021-03-30 10:41:29', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kendaraan_biaya`
--

CREATE TABLE `kendaraan_biaya` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `id_kendaraan` int(11) NOT NULL,
  `catatan` text NOT NULL,
  `nominal` bigint(20) NOT NULL,
  `jenis_pembayaran` enum('cash','transfer','tbd') NOT NULL,
  `id_kas` int(11) NOT NULL,
  `is_verifikasi` tinyint(4) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL,
  `verifikasi_by` int(11) NOT NULL,
  `timestamp_verifikasi` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kendaraan_biaya`
--

INSERT INTO `kendaraan_biaya` (`id`, `tanggal`, `id_kategori`, `id_kendaraan`, `catatan`, `nominal`, `jenis_pembayaran`, `id_kas`, `is_verifikasi`, `input_by`, `timestamp_input`, `verifikasi_by`, `timestamp_verifikasi`) VALUES
(1, '2021-08-30', 1, 7, 'ganti aki', 600000, 'cash', 7, 0, 1, '2021-09-08 17:13:45', 0, '0000-00-00 00:00:00'),
(3, '2021-09-08', 3, 6, 'perpanjang bpkb', 1000000, 'transfer', 8, 1, 1, '2021-09-08 17:43:11', 1, '2021-09-08 17:48:56');

-- --------------------------------------------------------

--
-- Table structure for table `master_akun_biaya`
--

CREATE TABLE `master_akun_biaya` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_akun_biaya`
--

INSERT INTO `master_akun_biaya` (`id`, `nama`, `status`, `is_deleted`) VALUES
(1, 'Biaya Angkutan', 1, 0),
(2, 'Biaya Servis', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `master_akun_biaya_detail`
--

CREATE TABLE `master_akun_biaya_detail` (
  `id` int(11) NOT NULL,
  `id_biaya` int(11) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_akun_biaya_detail`
--

INSERT INTO `master_akun_biaya_detail` (`id`, `id_biaya`, `nama`, `status`) VALUES
(1, 1, 'Ganti Ban', 1);

-- --------------------------------------------------------

--
-- Table structure for table `master_akun_pendapatan`
--

CREATE TABLE `master_akun_pendapatan` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_akun_pendapatan`
--

INSERT INTO `master_akun_pendapatan` (`id`, `nama`, `status`, `is_deleted`) VALUES
(1, 'Penjualan Barang Bekas', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `master_biaya_kendaraan`
--

CREATE TABLE `master_biaya_kendaraan` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_biaya_kendaraan`
--

INSERT INTO `master_biaya_kendaraan` (`id`, `nama`, `status`, `is_deleted`) VALUES
(1, 'Servis', 1, 0),
(2, 'Bahan Bakar', 1, 0),
(3, 'Administrasi', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `uuid` varchar(10) NOT NULL,
  `label` varchar(255) NOT NULL,
  `deskripsi` varchar(255) DEFAULT NULL,
  `icon` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `tautan` text NOT NULL,
  `child_of` int(11) NOT NULL DEFAULT '0',
  `urutan` int(11) NOT NULL,
  `query_badge` text NOT NULL,
  `is_curl` tinyint(1) NOT NULL,
  `is_disabled` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `uuid`, `label`, `deskripsi`, `icon`, `class`, `tautan`, `child_of`, `urutan`, `query_badge`, `is_curl`, `is_disabled`, `is_deleted`) VALUES
(1, 'dsb', 'dashboard', NULL, 'icon-home', 'menu-dashboard', 'dashboard', 0, 1, '', 0, 0, 0),
(2, 'kdr', 'kendaraan', NULL, 'icon-speedometer', 'sub-kendaraan', '', 0, 2, '', 0, 0, 0),
(3, 'kdrdt', 'data kendaraan', NULL, 'icon-list', 'menu-kendaraan', 'kendaraan', 2, 3, 'SELECT (sum(if(datediff(stnk_jatuh_tempo, now()) <= 7 , 1, 0)) + sum(if(datediff(pajak_jatuh_tempo, now()) <= 7 , 1, 0))) + sum(if(datediff(bpkb_jatuh_tempo, now()) <= 7 , 1, 0)) + sum(if(datediff(kir_jatuh_tempo, now()) <= 7 , 1, 0)) as total FROM `kendaraan`', 0, 0, 0),
(4, 'kdrsv', 'servis kendaraan', NULL, 'icon-wrench', 'menu-servis', 'servis', 2, 4, '', 0, 0, 1),
(5, 'eks', 'ekspedisi', NULL, 'fa fa-truck', 'sub-ekspedisi', '', 0, 6, '', 0, 0, 0),
(6, 'brkt', 'Data Ekspedisi', NULL, 'icon-directions', 'menu-ekspedisi', 'ekspedisi', 5, 6, '', 0, 0, 0),
(7, 'klp', 'cek kelengkapan', NULL, 'icon-list', 'menu-kelengkapan', 'kelengkapan', 5, 8, 'SELECT COUNT(*) as total FROM ekspedisi where status_berangkat = 1 and is_done = 0 and is_deleted = 0 and (tanggal_cek_berangkat is null or tanggal_cek_pulang is null)', 0, 0, 0),
(8, 'bya', 'entri biaya', NULL, 'icon-note', 'menu-biaya', 'biaya', 5, 9, 'SELECT COUNT(*) as total FROM ekspedisi where status_berangkat = 1 and status_biaya = 0 and is_done = 0 and tanggal_cek_berangkat is not null and tanggal_cek_pulang is not null ', 0, 0, 0),
(9, 'vlbya', 'validasi biaya', NULL, 'icon-check', 'menu-validasi-biaya', 'validasi_biaya', 5, 10, 'SELECT COUNT(*) as total FROM ekspedisi where status_berangkat = 1 and status_biaya = 1 and is_done = 0 and tanggal_cek_berangkat is not null and tanggal_cek_pulang is not null ', 0, 0, 0),
(10, 'csm', 'master customer', NULL, 'icon-people', 'menu-customer', 'customer', 20, 10, '', 0, 0, 0),
(11, 'area', 'master area', NULL, 'icon-settings', 'menu-master-area', 'area', 20, 11, '', 0, 0, 0),
(12, 'arsks', 'buku kas', NULL, 'fa fa-exchange', 'sub-aruskas', 'aruskas/rekap', 0, 15, '', 0, 0, 0),
(13, 'kll-arsks', 'kelola transaksi', NULL, 'icon-arrow-right', 'menu-aruskas-kelola', 'aruskas/kelola', 12, 1, '', 0, 0, 0),
(14, 'acc-arsks', 'otorisasi transaksi', NULL, 'icon-arrow-right', 'menu-aruskas-persetujuan', 'aruskas/persetujuan', 12, 3, 'api/external/arus_kas/get_jumlah_pengajuan', 1, 0, 0),
(15, 'mts-arsks', 'mutasi akun', NULL, 'icon-arrow-right', 'menu-aruskas-mutasi', 'aruskas/mutasi', 12, 2, '', 0, 0, 0),
(16, 'rkp-arsks', 'rekap arus kas', NULL, 'icon-arrow-right', 'menu-aruskas-rekap', 'aruskas/rekap', 12, 4, '', 0, 0, 0),
(17, 'acc-expd', 'persetujuan', NULL, 'icon-list', 'menu-persetujuan-ekspedisi', 'ekspedisi/persetujuan', 5, 7, 'SELECT COUNT(*) as total FROM ekspedisi where status_berangkat = 0 and is_done = 0 and is_deleted = 0', 0, 0, 0),
(18, 'rkp-expd', 'rekap ekspedisi', NULL, 'icon-list', 'menu-rekap-ekspedisi', 'ekspedisi/rekap', 5, 12, '', 0, 0, 0),
(19, 'tghn', 'tagihan', NULL, 'icon-check', 'menu-tagihan', 'tagihan', 5, 11, 'SELECT COUNT(*) as total FROM tagihan where is_lunas = 0', 0, 0, 0),
(20, 'kms', 'data master', '', 'icon-book-open', 'sub-master', '', 0, 1, '', 0, 0, 0),
(21, 'm-ak-bbn', 'akun biaya', NULL, 'icon-arrow-right', 'menu-akun-biaya', 'master/akun/biaya', 20, 1, '', 0, 0, 0),
(22, 'm-ak-pdp', 'akun pendapatan', NULL, 'icon-arrow-right', 'menu-mstr-akun-pendapatan', 'master/akun/pendapatan', 20, 2, '', 0, 0, 0),
(23, 'bbn', 'Beban / Biaya', NULL, 'icon icon-basket', 'sub-beban', '', 0, 7, '', 0, 0, 0),
(24, 'bbn-kll', 'kelola beban', NULL, '', 'menu-beban-kelola', 'manajemen/beban/kelola', 23, 1, '', 0, 0, 0),
(25, 'bbn-acc', 'otorisasi beban', NULL, 'icon-arrow-right', 'menu-beban-persetujuan', 'manajemen/beban/persetujuan', 23, 2, 'SELECT COUNT(*) as total FROM beban where is_verifikasi = 0', 0, 0, 0),
(26, 'bbn-rkp', 'rekap beban', NULL, 'icon-arrow-right', 'menu-beban-rekap', 'manajemen/beban/rekap', 23, 3, '', 0, 0, 0),
(27, 'pdt', 'pendapatan lain', NULL, 'icon icon-share-alt', 'sub-pendapatan', '', 0, 8, '', 0, 0, 0),
(28, 'pdt-kll', 'kelola pendapatan', NULL, 'icon-arrow-right', 'menu-pendapatan-kelola', 'manajemen/pendapatan/kelola', 27, 1, '', 0, 0, 0),
(29, 'pdt-acc', 'otorisasi pendapatan', NULL, 'icon-arrow-right', 'menu-pendapatan-persetujuan', 'manajemen/pendapatan/persetujuan', 27, 2, 'SELECT COUNT(*) as total FROM pendapatan where is_verifikasi = 0', 0, 0, 0),
(30, 'pdt-rkp', 'rekap pendapatan', NULL, 'icon-arrow-right', 'menu-pendapatan-rekap', 'manajemen/pendapatan/rekap', 27, 3, '', 0, 0, 0),
(31, 'htg', 'hutang', NULL, 'icon icon-fire', 'sub-hutang', '', 0, 10, '', 0, 0, 0),
(32, 'htg-kll', 'kelola transaksi', NULL, 'icon-arrow-right', 'menu-hutang-kelola', 'manajemen/hutang/kelola', 31, 1, '', 0, 0, 0),
(33, 'htg-acc', 'otorisasi hutang', NULL, 'icon-arrow-right', 'menu-hutang-persetujuan', 'manajemen/hutang/persetujuan', 31, 2, 'SELECT COUNT(*) as total FROM hutang_detail where is_verifikasi = 0', 0, 0, 0),
(34, 'htg-rkp', 'rekap hutang', NULL, 'icon-arrow-right', 'menu-hutang-rekap', 'manajemen/hutang/rekap', 31, 3, '', 0, 0, 0),
(35, 'piut', 'piutang', NULL, 'icon icon-calculator', 'sub-piutang', '', 0, 11, '', 0, 0, 0),
(36, 'piut-kll', 'kelola transaksi', NULL, 'icon-arrow-right', 'menu-piutang-kelola', 'manajemen/piutang/kelola', 35, 1, '', 0, 0, 0),
(37, 'piut-acc', 'otorisasi piutang', NULL, 'icon-arrow-right', 'menu-piutang-persetujuan', 'manajemen/piutang/persetujuan', 35, 2, 'SELECT COUNT(*) as total FROM piutang_detail where is_verifikasi = 0', 0, 0, 0),
(38, 'piut-rkp', 'rekap piutang', NULL, 'icon-arrow-right', 'menu-piutang-rekap', 'manajemen/piutang/rekap', 35, 3, '', 0, 0, 0),
(39, 'm-by-kdr', 'biaya kendaraan', NULL, 'icon-arrow-right', 'menu-master-biaya-kendaraan', 'master/biaya_kendaraan', 20, 3, '', 0, 0, 0),
(40, 'kdr-by', 'biaya kendaraan', NULL, 'icon-wrench', 'menu-biaya-kendaraan', 'kendaraan/biaya', 2, 5, '', 0, 0, 0),
(41, 'kdr-by-acc', 'otorisasi biaya', NULL, 'icon-wrench', 'menu-otorisasi-biaya-kendaraan', 'kendaraan/persetujuan_biaya', 2, 6, 'SELECT COUNT(*) as total FROM kendaraan_biaya where is_verifikasi = 0', 0, 0, 0),
(42, 'kdr-by-rkp', 'rekap biaya', NULL, 'icon-wrench', 'menu-rekap-biaya-kendaraan', 'kendaraan/rekap_biaya', 2, 7, '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pendapatan`
--

CREATE TABLE `pendapatan` (
  `id` int(11) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `nominal` int(11) NOT NULL,
  `catatan` text NOT NULL,
  `jenis_pembayaran` enum('cash','transfer','tbd') NOT NULL,
  `id_kas` int(11) NOT NULL,
  `is_verifikasi` tinyint(1) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL,
  `verifikasi_by` int(11) NOT NULL,
  `timestamp_verifikasi` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `piutang`
--

CREATE TABLE `piutang` (
  `id` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `tgl_piutang` date NOT NULL,
  `jenis_piutang` enum('panjang','pendek','') NOT NULL,
  `nominal_piutang` bigint(20) NOT NULL,
  `keterangan` text NOT NULL,
  `is_paid_off` tinyint(1) NOT NULL,
  `is_verifikasi` tinyint(1) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL,
  `verifikasi_by` int(11) NOT NULL,
  `timestamp_verifikasi` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `piutang_detail`
--

CREATE TABLE `piutang_detail` (
  `id` int(11) NOT NULL,
  `id_piutang` int(11) NOT NULL,
  `source` varchar(100) NOT NULL,
  `id_source` int(11) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `tambah_piutang` bigint(20) NOT NULL,
  `bayar_pokok` bigint(20) NOT NULL,
  `bayar_bunga` bigint(20) NOT NULL,
  `pokok_awal` bigint(20) NOT NULL,
  `sisa_pokok` bigint(20) NOT NULL,
  `remark` text NOT NULL,
  `jenis_pembayaran` enum('cash','transfer','tbd') NOT NULL,
  `id_kas` int(11) NOT NULL,
  `is_verifikasi` tinyint(1) NOT NULL,
  `input_by` int(11) NOT NULL,
  `timestamp_input` datetime NOT NULL,
  `verifikasi_by` int(11) NOT NULL,
  `timestamp_verifikasi` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `servis`
--

CREATE TABLE `servis` (
  `id` int(11) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `kendaraan_id` int(11) NOT NULL,
  `pegawai_id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `lokasi` varchar(255) NOT NULL,
  `mekanik` varchar(255) NOT NULL,
  `keterangan` text,
  `insert_by` int(11) NOT NULL,
  `insert_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `servis`
--

INSERT INTO `servis` (`id`, `uuid`, `kendaraan_id`, `pegawai_id`, `tanggal`, `lokasi`, `mekanik`, `keterangan`, `insert_by`, `insert_time`, `update_by`, `update_time`, `is_deleted`) VALUES
(1, 'SV190921-TXM', 4, 2, '2019-09-21', 'Jember', 'Supriadi', 'Maintenace rutin', 0, '2019-09-21 08:29:21', 0, '2019-09-21 08:29:21', 0);

-- --------------------------------------------------------

--
-- Table structure for table `servis_rincian`
--

CREATE TABLE `servis_rincian` (
  `id` int(11) NOT NULL,
  `servis_id` int(11) NOT NULL,
  `perbaikan` varchar(255) NOT NULL,
  `jumlah` int(11) NOT NULL DEFAULT '1',
  `biaya` int(11) NOT NULL,
  `keterangan` text,
  `insert_by` int(11) NOT NULL,
  `insert_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `servis_rincian`
--

INSERT INTO `servis_rincian` (`id`, `servis_id`, `perbaikan`, `jumlah`, `biaya`, `keterangan`, `insert_by`, `insert_time`, `update_by`, `update_time`, `is_deleted`) VALUES
(4, 1, 'Ganti oli', 1, 50000, 'Oli berkualitas', 0, '2019-09-24 06:10:10', 0, '2019-09-24 06:10:10', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tagihan`
--

CREATE TABLE `tagihan` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `customer_id` int(11) NOT NULL,
  `sumber` enum('ekspedisi','balen') NOT NULL,
  `id_ekspedisi` int(11) NOT NULL,
  `id_balen` int(11) NOT NULL,
  `id_area` int(11) NOT NULL,
  `pembayaran` enum('cash','transfer','tbd') NOT NULL,
  `id_kas` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `tanggal_bayar` date NOT NULL,
  `jumlah_bayar` int(11) NOT NULL,
  `is_lunas` tinyint(4) NOT NULL,
  `verif_by` int(11) NOT NULL,
  `timestamp_verif` datetime NOT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tagihan`
--

INSERT INTO `tagihan` (`id`, `tanggal`, `customer_id`, `sumber`, `id_ekspedisi`, `id_balen`, `id_area`, `pembayaran`, `id_kas`, `subtotal`, `tanggal_bayar`, `jumlah_bayar`, `is_lunas`, `verif_by`, `timestamp_verif`, `timestamp`) VALUES
(1, '2019-09-26', 1, 'ekspedisi', 4, 0, 1, 'transfer', 8, 1500000, '2021-03-18', 1500000, 1, 1, '2021-03-19 17:47:35', '2021-03-12 17:22:45'),
(2, '2021-02-21', 1, 'balen', 4, 7, 1, 'cash', 0, 1600000, '0000-00-00', 0, 2, 0, '0000-00-00 00:00:00', '2021-03-12 17:22:45'),
(3, '2021-02-22', 1, 'balen', 4, 8, 1, 'cash', 0, 3000000, '0000-00-00', 0, 0, 0, '0000-00-00 00:00:00', '2021-03-12 17:22:45'),
(4, '2021-03-02', 1, 'balen', 4, 11, 1, 'transfer', 8, 1000000, '0000-00-00', 0, 0, 0, '0000-00-00 00:00:00', '2021-03-12 17:22:45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `balen`
--
ALTER TABLE `balen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `beban`
--
ALTER TABLE `beban`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_beban` (`id_beban`);

--
-- Indexes for table `biaya`
--
ALTER TABLE `biaya`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ekspedisi`
--
ALTER TABLE `ekspedisi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_area` (`id_area`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `hutang`
--
ALTER TABLE `hutang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `input_by` (`input_by`),
  ADD KEY `verifikasi_by` (`verifikasi_by`);

--
-- Indexes for table `hutang_detail`
--
ALTER TABLE `hutang_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_piutang` (`id_hutang`),
  ADD KEY `input_by` (`input_by`),
  ADD KEY `verifikasi_by` (`verifikasi_by`);

--
-- Indexes for table `kelengkapan`
--
ALTER TABLE `kelengkapan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kendaraan`
--
ALTER TABLE `kendaraan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kendaraan_biaya`
--
ALTER TABLE `kendaraan_biaya`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kategori` (`id_kategori`),
  ADD KEY `id_kendaraan` (`id_kendaraan`),
  ADD KEY `id_kas` (`id_kas`);

--
-- Indexes for table `master_akun_biaya`
--
ALTER TABLE `master_akun_biaya`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_akun_biaya_detail`
--
ALTER TABLE `master_akun_biaya_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_biaya` (`id_biaya`);

--
-- Indexes for table `master_akun_pendapatan`
--
ALTER TABLE `master_akun_pendapatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_biaya_kendaraan`
--
ALTER TABLE `master_biaya_kendaraan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pendapatan`
--
ALTER TABLE `pendapatan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_beban` (`id_kategori`);

--
-- Indexes for table `piutang`
--
ALTER TABLE `piutang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `input_by` (`input_by`),
  ADD KEY `verifikasi_by` (`verifikasi_by`);

--
-- Indexes for table `piutang_detail`
--
ALTER TABLE `piutang_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_piutang` (`id_piutang`),
  ADD KEY `input_by` (`input_by`),
  ADD KEY `verifikasi_by` (`verifikasi_by`);

--
-- Indexes for table `servis`
--
ALTER TABLE `servis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `servis_rincian`
--
ALTER TABLE `servis_rincian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tagihan`
--
ALTER TABLE `tagihan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `balen`
--
ALTER TABLE `balen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `beban`
--
ALTER TABLE `beban`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `biaya`
--
ALTER TABLE `biaya`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ekspedisi`
--
ALTER TABLE `ekspedisi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `hutang`
--
ALTER TABLE `hutang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hutang_detail`
--
ALTER TABLE `hutang_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kelengkapan`
--
ALTER TABLE `kelengkapan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `kendaraan`
--
ALTER TABLE `kendaraan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `kendaraan_biaya`
--
ALTER TABLE `kendaraan_biaya`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `master_akun_biaya`
--
ALTER TABLE `master_akun_biaya`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `master_akun_biaya_detail`
--
ALTER TABLE `master_akun_biaya_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `master_akun_pendapatan`
--
ALTER TABLE `master_akun_pendapatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `master_biaya_kendaraan`
--
ALTER TABLE `master_biaya_kendaraan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `pendapatan`
--
ALTER TABLE `pendapatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `piutang`
--
ALTER TABLE `piutang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `piutang_detail`
--
ALTER TABLE `piutang_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `servis`
--
ALTER TABLE `servis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `servis_rincian`
--
ALTER TABLE `servis_rincian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tagihan`
--
ALTER TABLE `tagihan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
